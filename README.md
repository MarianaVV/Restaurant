# Restaurant
#Steps to run the app

Steps to be able to run the "SomethingGoodToEat" web application:

1. Prerequisites:
To be able to run the application, I used the following versions of tools:
-IntelliJ IDEA 2022.3.1 (Ultimate Edition) - is free for students/teachers
-Gradle 7.6 (use the command: gradle -v to see if you have it installed or not, if it doesn't need to be installed)
-Openjdk version "1.8.0_302" (use the command: java -version to see if you have the java version installed, if not it must be installed)
—————————————————————————————————————-------------------------------------------------------------------------------------------------------
-Operating system used: macOS Ventura

2. Go to the gitlab link and download the code:
 https://github.com/MarianaDodenciu/Restaurant.git
Click on "Clone" -> Clone with HTTPS ->  https://github.com/MarianaDodenciu/Restaurant.git
3. I made a new folder called "RestaurantApp", I opened a terminal with the location in that folder and gave the command in the terminal:
   https://github.com/MarianaDodenciu/Restaurant.git
5. It is possible to ask for my credentials for downloading:
username: mariana.draganescu
password: FinalYear2023!
5.Open IntelliJ IDEA -> File ->Open ->RestaurantApp -> and click on build.gradle to open the application
->Open as a Project ->New Window
6. It is expected that all the imports will be brought, and for the connection with the database, click on the "Database" side
7. Press "+" -> Data Source -> My SQL ->
User: root
Password: 1234567890
URL: jdbc:mysql://localhost:3306
Press: Test Connection
8. Enter the class: RestaurantManagementSystemApplication and run the application by pressing the green arrow from code line 9 or as you wish.
9. At the end of the logs appears: "Tomcat started on port(s): 8080 (http) with context path ''" and the application can be accessed at
http://localhost:8080

10. Login users:
    
1.username: manager
password: testManager01234
2. username: waiter
password: testWaiter01234
3. username:chef
password: testChef01234
