#EXAMPLE OF queries
1.Categories
INSERT INTO categories (name) VALUES ("Coffee");
INSERT INTO categories (name) VALUES ("Dinner");
INSERT INTO categories (name) VALUES ("Lunch");
INSERT INTO categories (name) VALUES ("Snack");
INSERT INTO categories (name) VALUES ("Tea");
INSERT INTO categories (name) VALUES ("Summer Menu");
INSERT INTO categories (name) VALUES ("Alcoholic beverages");

2.Users
INSERT INTO userEntities (email_address, first_name, name, rol) VALUES ("chef@someEmail.com", "Marius",
                                                                        "Lazarescu", "CHEF");
INSERT INTO userEntities (email_address, first_name, name, rol) VALUES ("manager@someEmail.com", "Alex",
                                                                        "Anghelescu", "MANAGER");
INSERT INTO userEntities (email_address, first_name, name, rol) VALUES ("waiter@someEmail.com", "Maria",
                                                                        "Marinescu", "WAITER");

3.Tables
INSERT INTO tables (name, table_status, number_of_seats) VALUES ("Table 001", "FREE",  4);
INSERT INTO tables (name, table_status, number_of_seats) VALUES ("Table 002", "BOOKED",  8);
INSERT INTO tables (name, table_status, number_of_seats) VALUES ("Table 003", "BUSY",  12);
INSERT INTO orderEntities (name, order_status, payment_type, total_price_of_order, received_amount, tip) VALUES ("Order 001", "PENDING",  "CASH", "80", "100","20");
INSERT INTO menuEntities (description, image, name, price) VALUES ("Hamburger with fries,onion,pickles,mustard", "image1",  "Meniu Hamburger", "20");
INSERT INTO categories ( name ) VALUES ("Fast-Food");

4.Manually create users table
CREATE TABLE users (
                       id BIGINT AUTO_INCREMENT PRIMARY KEY,
                       name VARCHAR(255) NOT NULL,
                       first_name VARCHAR(255) NOT NULL,
                       rol VARCHAR(255) NOT NULL,
                       email_address VARCHAR(255) NOT NULL,
                       username VARCHAR(255) NOT NULL,
                       password VARCHAR(255) NOT NULL,
                       account_not_expired BOOLEAN DEFAULT true,
                       account_not_locked BOOLEAN DEFAULT true,
                       credentials_not_expired BOOLEAN DEFAULT true,
                       enabled BOOLEAN DEFAULT true
);
