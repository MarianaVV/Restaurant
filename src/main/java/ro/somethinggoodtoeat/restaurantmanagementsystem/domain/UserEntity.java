package ro.somethinggoodtoeat.restaurantmanagementsystem.domain;

import lombok.*;
import ro.somethinggoodtoeat.restaurantmanagementsystem.enums.Role;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "Users")
public class UserEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", columnDefinition = "BIGINT DEFAULT 1")
    private Long id;

    @Column(name = "NAME", nullable = false)
    private String lastName;

    @Column(name = "FIRST_NAME", nullable = false)
    private String firstName;

    @Enumerated(value = EnumType.STRING)
    @Column(name = "ROL")
    private Role role;

    @Column(name = "EMAIL_ADDRESS", nullable = false)
    private String emailAddress;

    @Column(name = "USERNAME")
    private String username;
    private String password;
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    private Set<UserPermissionEntity> permissions;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    private List<OrderEntity> orderEntities = new ArrayList<>();
    @Builder.Default
    private Boolean accountNotExpired = true;
    @Builder.Default
    private Boolean accountNotLocked = true;
    @Builder.Default
    private Boolean credentialsNotExpired = true;
    @Builder.Default
    private Boolean enabled = true;
}
