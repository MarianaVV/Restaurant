package ro.somethinggoodtoeat.restaurantmanagementsystem.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "Menus")
public class MenuEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "category_id")
    private CategoryEntity category;

    @Column(name = "NAME", nullable = false)
    private String name;

    @Column(name = "PRICE", nullable = false)
    private Double price;

    @Lob
    @Column(name = "IMAGE", columnDefinition = "BLOB")
    private byte[] image;

    @Column(name = "DESCRIPTION")
    private String description;

    @Column(name = "POPULAR_FLAG")
    private String isPopular;
    @OneToMany(mappedBy = "menu", cascade = CascadeType.ALL)
    private List<OrderDetailsEntity> orderDetails = new ArrayList<>();
}
