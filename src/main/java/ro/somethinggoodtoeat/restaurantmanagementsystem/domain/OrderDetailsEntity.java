package ro.somethinggoodtoeat.restaurantmanagementsystem.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ro.somethinggoodtoeat.restaurantmanagementsystem.enums.OrderDetailsStatus;
import ro.somethinggoodtoeat.restaurantmanagementsystem.enums.UnitOfMeasure;

import javax.persistence.*;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "Order_Details")
public class OrderDetailsEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Column(name = "AMOUNT", nullable = false)
    private Double amount;

    @Enumerated(value = EnumType.STRING)
    @Column(name = "ORDER_DETAILS_STATUS")
    private OrderDetailsStatus orderDetailsStatus;

    @Enumerated(value = EnumType.STRING)
    @Column(name = "UNIT_OF_MEASURE")
    private UnitOfMeasure unitOfMeasure;

    @ManyToOne
    @JoinColumn(name = "menu_id")
    private MenuEntity menu;

    @ManyToOne
    @JoinColumn(name = "order_id")
    private OrderEntity order;
}
