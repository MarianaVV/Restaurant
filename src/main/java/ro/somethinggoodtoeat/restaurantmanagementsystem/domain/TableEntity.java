package ro.somethinggoodtoeat.restaurantmanagementsystem.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ro.somethinggoodtoeat.restaurantmanagementsystem.enums.TableStatus;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "Tables")
public class TableEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Column(name = "NAME", nullable = false)
    private String name;

    @Enumerated(value = EnumType.STRING)
    @Column(name = "TABLE_STATUS", nullable = false)
    private TableStatus tableStatus;

    @Column(name = "NUMBER_OF_SEATS", nullable = false)
    private Integer numberOfSeats;
}
