package ro.somethinggoodtoeat.restaurantmanagementsystem.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ro.somethinggoodtoeat.restaurantmanagementsystem.enums.OrderStatus;
import ro.somethinggoodtoeat.restaurantmanagementsystem.enums.PaymentType;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "Orders")
public class OrderEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Column(name = "NAME")
    private String name;
    @Column(name = "TOTAL_PRICE_OF_ORDER", nullable = false)
    private Double priceOfOrder;

    @Column(name = "RECEIVED_AMOUNT")
    private Double receivedAmount;

    @Column(name = "TIP")
    private Double tip;

    @Column(name = "AMOUNT_DUE")
    private Double amountDue;

    @Enumerated(value = EnumType.STRING)
    @Column(name = "PAYMENT_TYPE", nullable = false)
    private PaymentType paymentType;

    @Enumerated(value = EnumType.STRING)
    @Column(name = "ORDER_STATUS")
    private OrderStatus orderStatus;

    @ManyToOne
    @JoinColumn(name = "table_id")
    private TableEntity eatingTable;

    @OneToMany(mappedBy = "order")
    private List<OrderDetailsEntity> orderDetailEntities = new ArrayList<>();

    @ManyToOne
    @JoinColumn(name = "user_id")
    private UserEntity user;

    @Column(name = "START_TIME")
    private LocalDateTime startTime;
}
