package ro.somethinggoodtoeat.restaurantmanagementsystem.domain;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "Permissions")
public class PermissionEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Column(name = "ROLE")
    private String role;

    @OneToMany(mappedBy = "permission", cascade = CascadeType.ALL)
    private List<UserPermissionEntity> permissions;
}
