package ro.somethinggoodtoeat.restaurantmanagementsystem.config;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import ro.somethinggoodtoeat.restaurantmanagementsystem.exception.SomethingGoodToEatException;
import ro.somethinggoodtoeat.restaurantmanagementsystem.security.LoginUserService;

@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private final LoginUserService loginUserService;

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http.authorizeRequests(authorize ->
                {
                    try {
                        configureUrlPattern(authorize);
                        configureExceptionHandling(authorize);
                    } catch (Exception e) {
                        throw new SomethingGoodToEatException("An error occurred while configuring security.", e, "CONFIG_ERROR");
                    }
                })
                .formLogin(loginConfigurer -> {
                    loginConfigurer
                            .loginProcessingUrl("/api/v1/login/singIn")
                            .loginPage("/").permitAll()
                            .successForwardUrl("/")
                            .defaultSuccessUrl("/")
                            .failureUrl("/?error");

                })
                .logout(logoutConfigurer -> {
                    logoutConfigurer
                            .logoutRequestMatcher(new
                                    AntPathRequestMatcher("/api/v1/login/logout", "POST"))
                            .logoutSuccessUrl("/api/v1/login/singIn")
                            .permitAll();

                })
                .csrf()
                .disable()
                .httpBasic();
    }

    private void configureUrlPattern(ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry authorize) {

        authorize
                .antMatchers("/", "/webjars/**", "/login", "/resources/**").permitAll()
                .antMatchers("/api/v1/login/**").permitAll()
                .antMatchers("/api/v1/order/**").permitAll()
                .antMatchers("/api/v1/order/order").permitAll()
                .antMatchers("/api/v1/order/orderDetails/**").permitAll()
                .antMatchers("/api/v1/report/reports").permitAll()
                .antMatchers("/api/v1/order/viewOrderDetails/**").permitAll()
                .antMatchers("/api/v1/category/**").hasRole("MANAGER")
                .antMatchers("/api/v1/report/**").hasRole("MANAGER")
                .antMatchers("/api/v1/user/**").hasRole("MANAGER")
                .antMatchers("/api/v1/menu/**").hasAnyRole("MANAGER", "WAITER", "CHEF")
                .antMatchers("/api/v1/table/**").hasAnyRole("MANAGER", "WAITER")
                .antMatchers("/api/v1/table/table").hasAnyRole("MANAGER", "WAITER")
                .antMatchers("/api/v1/table/table/{id}/update").hasAnyRole("MANAGER", "WAITER")
                .antMatchers("/api/v1/report/popularItems").hasAnyRole("MANAGER")
                .anyRequest().authenticated();
    }

    private void configureExceptionHandling(ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry authorize) throws Exception {

        authorize
                .and()
                .exceptionHandling()
                .accessDeniedPage("/api/v1/login/access-denied");
    }

    @Bean
    PasswordEncoder passwordEncoder() {

        return PasswordEncoderFactories.createDelegatingPasswordEncoder();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {

        auth.userDetailsService(this.loginUserService).passwordEncoder(passwordEncoder());
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }
}
