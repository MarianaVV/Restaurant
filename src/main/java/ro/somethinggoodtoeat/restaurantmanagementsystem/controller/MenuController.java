package ro.somethinggoodtoeat.restaurantmanagementsystem.controller;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import ro.somethinggoodtoeat.restaurantmanagementsystem.dto.CategoryDto;
import ro.somethinggoodtoeat.restaurantmanagementsystem.dto.MenuDto;
import ro.somethinggoodtoeat.restaurantmanagementsystem.enums.Crud;
import ro.somethinggoodtoeat.restaurantmanagementsystem.service.CategoryServiceImpl;
import ro.somethinggoodtoeat.restaurantmanagementsystem.service.MenuServiceImpl;

import javax.validation.Valid;
import java.util.List;

@RequestMapping("/api/v1/menu")
@Controller
public class MenuController {

    private final MenuServiceImpl menuServiceImpl;
    private final CategoryServiceImpl categoryServiceImpl;

    public MenuController(MenuServiceImpl menuServiceImpl, CategoryServiceImpl categoryServiceImpl) {
        this.menuServiceImpl = menuServiceImpl;
        this.categoryServiceImpl = categoryServiceImpl;
    }

    @GetMapping({"/menus"})
    public String getAllMenus(@RequestParam(defaultValue = "0") int page,
                              @RequestParam(defaultValue = "10") int size,
                              Model model) {

        Page<MenuDto> menuPage = menuServiceImpl.getAllMenus(page, size);
        model.addAttribute("menus", menuPage.getContent());
        model.addAttribute("page", menuPage);

        return "/menu/menu.html";
    }

    @RequestMapping("/menu/new")
    public String newMenu(Model model) {

        MenuDto menuDto = new MenuDto();
        model.addAttribute("menu", menuDto);

        menuDto.setCategory(new CategoryDto());
        model.addAttribute("categories", categoryServiceImpl.getAllCategories());

        return "menu/create.html";
    }

    @RequestMapping("/menu/{id}/edit")
    public String editMenu(Model model,
                           @PathVariable("id") Long id) {

        MenuDto menuDto = menuServiceImpl.edit(id);

        if (menuDto != null) {
            menuDto.setIdString(menuDto.getId().toString());
            model.addAttribute("menu", menuDto);
            model.addAttribute("id", menuDto.getId());
        }

        model.addAttribute("categories",
                categoryServiceImpl.getAllCategories());

        return "menu/edit.html";
    }

    @PostMapping("/menu")
    public String createMenu(@Valid @ModelAttribute("menu") MenuDto menu,
                             BindingResult bindingResult,
                             RedirectAttributes redirectAttributes,
                             Model model) {

        if (bindingResult.hasErrors()) {
            model.addAttribute("categories", categoryServiceImpl.getAllCategories());
            return "menu/create.html";
        }

        List<MenuDto> popularMenus = menuServiceImpl.getAllMenusByIsPopular();
        int countPopularMenus = (int) popularMenus.stream()
                .filter(popularMenu -> "Y".equals(popularMenu.getIsPopular()))
                .count();
        if ((countPopularMenus >= 3) && (("Y").equals(menu.getIsPopular()))) {
            model.addAttribute("errorMessage",
                    "You reached the limit of adding popular menus for this month." +
                            "Unselect the checkbox in order to proceed.");
            model.addAttribute("categories", categoryServiceImpl.getAllCategories());
            return "menu/create.html";
        }

        MenuDto savedMenu = menuServiceImpl.createMenu(menu);
        redirectAttributes.addFlashAttribute("successMessage",
                "Menu with ID " + savedMenu.getId() + " was created successfully.");

        return "redirect:/api/v1/menu/menus";
    }

    @RequestMapping("/menu/{id}/update")
    public String updateMenu(@PathVariable("id") Long id,
                             @Valid @ModelAttribute("menu") MenuDto menuDto,
                             BindingResult bindingResult,
                             Model model,
                             RedirectAttributes redirectAttributes) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("categories", categoryServiceImpl.getAllCategories());
            return "menu/create.html";
        }

        MenuDto updatedMenu = menuServiceImpl.updateMenu(id, menuDto);
        model.addAttribute("menu", updatedMenu);
        addSuccessFlashAttribute(redirectAttributes, Crud.UPDATE.toString(), updatedMenu.getId());

        return "redirect:/api/v1/menu/menus";
    }

    @GetMapping
    @RequestMapping("/menu/{id}/delete")
    public String deleteMenu(@PathVariable("id") Long id,
                             RedirectAttributes redirectAttributes) {

        menuServiceImpl.deleteMenu(id);
        addSuccessFlashAttribute(redirectAttributes, Crud.DELETE.toString(), id);

        return "redirect:/api/v1/menu/menus";
    }

    private void addSuccessFlashAttribute(RedirectAttributes redirectAttributes, String action, Long entityId) {

        String successMessage = String.format("%s with ID %d was %sd successfully.", "Menu", entityId, action.toLowerCase());
        redirectAttributes.addFlashAttribute("successMessage", successMessage);
    }
}
