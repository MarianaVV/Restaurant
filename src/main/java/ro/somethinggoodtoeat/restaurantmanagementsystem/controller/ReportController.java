package ro.somethinggoodtoeat.restaurantmanagementsystem.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ro.somethinggoodtoeat.restaurantmanagementsystem.dto.MenuDto;
import ro.somethinggoodtoeat.restaurantmanagementsystem.dto.OrderDetailsDto;
import ro.somethinggoodtoeat.restaurantmanagementsystem.dto.OrderDto;
import ro.somethinggoodtoeat.restaurantmanagementsystem.dto.Report;
import ro.somethinggoodtoeat.restaurantmanagementsystem.service.MenuServiceImpl;
import ro.somethinggoodtoeat.restaurantmanagementsystem.service.OrderServiceImpl;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

@RequestMapping("/api/v1/report")
@Controller
public class ReportController {
    private final MenuServiceImpl menuService;
    private final OrderServiceImpl orderService;

    public ReportController(
            MenuServiceImpl menuService,
            OrderServiceImpl orderService) {

        this.menuService = menuService;
        this.orderService = orderService;
    }

    @GetMapping("/popularItems")
    public String getAllPopularMenus(Model model) {

        List<MenuDto> menus = menuService.getAllMenusByIsPopular();

        model.addAttribute("menus", menus);
        model.addAttribute("report", new Report());

        return "report/report.html";
    }

    @PostMapping("/reports")
    public String generateReport(@ModelAttribute("report") Report report, Model model) {

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime startDateTime = LocalDateTime.parse(report.getStartDate() + " 00:00:00", formatter);
        LocalDateTime endDateTime = LocalDateTime.parse(report.getEndDate() + " 23:59:59", formatter);

        List<MenuDto> popularMenus = getMenuDtos();
        List<Long> popularMenuIds = getPopularMenuIds(popularMenus);
        List<OrderDto> allPaidOrders = orderService.getOrdersBetweenDates();
        List<OrderDto> orders = getOrderDtos(popularMenus, allPaidOrders);
        setMostPopularMenus(orders);

        double totalSalesAmount = getTotalSalesAmount(orders);
        int totalOrders = orders.size();

        double totalSalesAmountMenuOne = orders.stream()
                .filter(order -> orderContainsMenu(order, popularMenuIds.get(0)))
                .mapToDouble(order -> order.getOrderDetails().stream()
                        .filter(orderDetail -> orderDetail.getMenu().getId().equals(popularMenuIds.get(0)))
                        .mapToDouble(orderDetail -> orderDetail.getMenu().getPrice() * orderDetail.getAmount())
                        .sum())
                .sum();
        double totalSalesAmountMenuTwo = orders.stream()
                .filter(order -> orderContainsMenu(order, popularMenuIds.get(1)))
                .mapToDouble(order -> order.getOrderDetails().stream()
                        .filter(orderDetail -> orderDetail.getMenu().getId().equals(popularMenuIds.get(1)))
                        .mapToDouble(orderDetail -> orderDetail.getMenu().getPrice() * orderDetail.getAmount())
                        .sum())
                .sum();
        double totalSalesAmountMenuThree = orders.stream()
                .filter(order -> orderContainsMenu(order, popularMenuIds.get(2)))
                .mapToDouble(order -> order.getOrderDetails().stream()
                        .filter(orderDetail -> orderDetail.getMenu().getId().equals(popularMenuIds.get(2)))
                        .mapToDouble(orderDetail -> orderDetail.getMenu().getPrice() * orderDetail.getAmount())
                        .sum())
                .sum();

        int totalOrdersWithMenu1 = (int) orders.stream()
                .filter(order -> orderContainsMenu(order, popularMenuIds.get(0)))
                .count();
        int totalOrdersWithMenu2 = (int) orders.stream()
                .filter(order -> orderContainsMenu(order, popularMenuIds.get(0)))
                .count();
        int totalOrdersWithMenu3 = (int) orders.stream()
                .filter(order -> orderContainsMenu(order, popularMenuIds.get(0)))
                .count();

        model.addAttribute("menus", popularMenus);
        model.addAttribute("orders", orders);
        model.addAttribute("totalOrders", totalOrders);
        model.addAttribute("totalSalesAmount", totalSalesAmount);
        model.addAttribute("totalOrdersWithMenu1", totalOrdersWithMenu1);
        model.addAttribute("totalSalesAmountMenuOne", totalSalesAmountMenuOne);
        model.addAttribute("menuOneName", popularMenus.get(0).getName());
        model.addAttribute("totalOrdersWithMenu2", totalOrdersWithMenu2);
        model.addAttribute("totalSalesAmountMenuTwo", totalSalesAmountMenuTwo);
        model.addAttribute("menuTwoName", popularMenus.get(1).getName());
        model.addAttribute("totalOrdersWithMenu3", totalOrdersWithMenu3);
        model.addAttribute("menuThreeName", popularMenus.get(2).getName());
        model.addAttribute("totalSalesAmountMenuThree", totalSalesAmountMenuThree);

        return "report/report.html";
    }

    private List<OrderDto> getOrderDtos(List<MenuDto> popularMenus, List<OrderDto> allPaidOrders) {
        List<OrderDto> orders = allPaidOrders.stream()
                .filter(order -> orderContainsPopularMenus(order, popularMenus))
                .collect(Collectors.toList());
        return orders;
    }

    private List<MenuDto> getMenuDtos() {
        List<MenuDto> popularMenus = menuService.getAllMenusByIsPopular().stream()
                .sorted(Comparator.comparing(MenuDto::getId))
                .collect(Collectors.toList());
        return popularMenus;
    }

    private static double getTotalSalesAmount(List<OrderDto> orders) {
        double totalSalesAmount = 0;
        for (OrderDto order : orders) {
            double totalPriceForOrder = order.getOrderDetails().stream()
                    .mapToDouble(orderDetail -> orderDetail.getMenu().getPrice() * orderDetail.getAmount())
                    .sum();
            totalSalesAmount += totalPriceForOrder;
        }
        return totalSalesAmount;
    }

    private boolean orderContainsPopularMenus(OrderDto order, List<MenuDto> popularMenus) {
        List<Long> popularMenuIds = getPopularMenuIds(popularMenus);

        return order.getOrderDetails().stream()
                .anyMatch(orderDetails -> {

                    MenuDto menu = orderDetails.getMenu();
                    return (popularMenuIds.contains(menu.getId()) && menu.getIsPopular().equalsIgnoreCase("Y"));
                });
    }

    public boolean orderContainsMenu(OrderDto order, Long menuId) {

        return order.getOrderDetails().stream()
                .anyMatch(orderDetail -> orderDetail.getMenu().getId().equals(menuId));
    }

    public static void setMostPopularMenus(List<OrderDto> orders) {

        for (OrderDto order : orders) {
            List<OrderDetailsDto> orderDetailsWithPopularMenus = new ArrayList<>();

            for (OrderDetailsDto orderDetail : order.getOrderDetails()) {

                if (orderDetail.getMenu() != null && "Y".equalsIgnoreCase(orderDetail.getMenu().getIsPopular())) {
                    orderDetailsWithPopularMenus.add(orderDetail);
                }
            }
            order.setOrderDetails(orderDetailsWithPopularMenus);
        }
    }

    private static List<Long> getPopularMenuIds(List<MenuDto> popularMenus) {
        List<Long> popularMenuIds = popularMenus.stream()
                .map(MenuDto::getId)
                .collect(Collectors.toList());
        return popularMenuIds;
    }
}
