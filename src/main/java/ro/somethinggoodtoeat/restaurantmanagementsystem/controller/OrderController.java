package ro.somethinggoodtoeat.restaurantmanagementsystem.controller;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import ro.somethinggoodtoeat.restaurantmanagementsystem.dto.MenuDto;
import ro.somethinggoodtoeat.restaurantmanagementsystem.dto.OrderDetailsDto;
import ro.somethinggoodtoeat.restaurantmanagementsystem.dto.OrderDto;
import ro.somethinggoodtoeat.restaurantmanagementsystem.dto.TableDto;
import ro.somethinggoodtoeat.restaurantmanagementsystem.enums.*;
import ro.somethinggoodtoeat.restaurantmanagementsystem.service.OrderDetailsServiceImpl;
import ro.somethinggoodtoeat.restaurantmanagementsystem.service.OrderServiceImpl;
import ro.somethinggoodtoeat.restaurantmanagementsystem.service.TableServiceImpl;
import ro.somethinggoodtoeat.restaurantmanagementsystem.service.MenuServiceImpl;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RequestMapping("/api/v1/order")
@Controller
public class OrderController {

    private final OrderServiceImpl orderServiceImpl;
    private final MenuServiceImpl menuServiceImpl;
    private final TableServiceImpl tableServiceImpl;
    private final OrderDetailsServiceImpl orderDetailsServiceImpl;

    public OrderController(OrderServiceImpl orderServiceImpl,
                           MenuServiceImpl menuServiceImpl,
                           TableServiceImpl tableServiceImpl,
                           OrderDetailsServiceImpl orderDetailsServiceImpl) {
        this.orderServiceImpl = orderServiceImpl;
        this.menuServiceImpl = menuServiceImpl;
        this.tableServiceImpl = tableServiceImpl;
        this.orderDetailsServiceImpl = orderDetailsServiceImpl;
    }

    @GetMapping("/orders")
    public String getAllOrders(@RequestParam(defaultValue = "0") int page,
                               @RequestParam(defaultValue = "10") int size,
                               Model model) {

        Page<OrderDto> orderPage = orderServiceImpl.getAllOrders(page, size);

        orderServiceImpl.changeOrderStatus(model, orderPage);
        model.addAttribute("orders", orderPage.getContent());
        model.addAttribute("page", orderPage);

        return "/order/order.html";
    }

    @GetMapping("/viewOrderDetails/{id}")
    public String getAllPendingOrders(@PathVariable("id") Long id,
                                      Model model) {

        OrderDto order = orderServiceImpl.getOrderById(id);
        List<OrderDetailsDto> orderDetails =
                orderDetailsServiceImpl.getAllOrderDetailsById(order.getId());

        model.addAttribute("order", order);
        model.addAttribute("orderDetails", orderDetails);

        return "order/viewOrderDetails.html";
    }

    @PostMapping("/orderDetails/{id}/updateStatus")
    public String updateStatus(@PathVariable("id") Long id) {

        OrderDetailsDto orderDetails = orderDetailsServiceImpl.getOrderDetailsById(id);
        return "redirect:/api/v1/order/order?orderId=" + orderDetails.getOrderId();
    }

    @GetMapping
    @RequestMapping("/order/{id}/delete")
    public String deleteOrder(@PathVariable("id") Long id,
                              RedirectAttributes redirectAttributes) {

        orderServiceImpl.deleteOrder(id);
        addSuccessFlashAttributeForOrder(redirectAttributes, Crud.DELETE.toString(), id);

        return "redirect:/api/v1/order/orders";
    }

    @RequestMapping("/order/new")
    public String newOrder(Model model) {

        OrderDto orderDto = new OrderDto();
        model.addAttribute("order", orderDto);

        List<TableDto> allAvailableTables = tableServiceImpl.getAllTablesWithStatusAvailable(TableStatus.AVAILABLE);
        model.addAttribute("tables", allAvailableTables);
        return "order/create.html";
    }

    @PostMapping("/order")
    public String newOrder(@Valid @ModelAttribute("order") OrderDto orderDto,
                           @RequestParam(name = "orderId", required = false) Long orderId,
                           BindingResult bindingResult,
                           Model model,
                           RedirectAttributes redirectAttributes) {

        if (bindingResult.hasErrors()) {
            return "order/addOrderItem.html";
        }

        if (orderId != null) {

            List<PaymentType> paymentType = Arrays.asList(PaymentType.CASH, PaymentType.CREDIT_CARD);
            List<OrderStatus> orderStatus = Arrays.asList(OrderStatus.PENDING,
                    OrderStatus.PAID, OrderStatus.READY, OrderStatus.DELIVERED);
            orderDto.setId(orderId);

            OrderDto order = orderDetailsServiceImpl.createOrder(orderDto);
            List<OrderDetailsDto> orderDetails = orderDetailsServiceImpl.getOrderDetailsByOrderId(order.getId());
            order.setOrderDetails(orderDetails);
            Double totalAmount = orderServiceImpl.calculateTotalAmount(order);
            order.setTotalAmount(totalAmount);

            model.addAttribute("order", order);
            model.addAttribute("tables", tableServiceImpl.getAllTables());
            model.addAttribute("paymentType", paymentType);
            model.addAttribute("orderStatus", orderStatus);
            model.addAttribute("orderDetails", orderDetails);

            addSuccessFlashAttributeForOrder(redirectAttributes,
                    Crud.CREATE.toString(), orderDto.getId());

            return "redirect:/api/v1/order/orders";

        } else {

            List<PaymentType> paymentType = Arrays.asList(PaymentType.CASH, PaymentType.CREDIT_CARD);
            List<OrderStatus> orderStatus = Arrays.asList(OrderStatus.PENDING,
                    OrderStatus.PAID, OrderStatus.READY, OrderStatus.DELIVERED);
            orderDto.setOrderStatus(OrderStatus.PENDING);

            OrderDto order = orderDetailsServiceImpl.createOrder(orderDto);
            List<OrderDetailsDto> orderDetails = orderDetailsServiceImpl.getOrderDetailsByOrderId(order.getId());
            order.setOrderDetails(orderDetails);

            model.addAttribute("order", order);
            model.addAttribute("tables", tableServiceImpl.getAllTables());
            model.addAttribute("paymentType", paymentType);
            model.addAttribute("orderStatus", orderStatus);
            model.addAttribute("orderDetails", orderDetails);

            addSuccessFlashAttributeForOrder(redirectAttributes,
                    Crud.CREATE.toString(), orderDto.getId());

            return "order/addOrderItem.html";
        }
    }

    @RequestMapping("/order/{id}/edit")
    public String editOrder(Model model,
                            @PathVariable("id") Long id) {

        List<PaymentType> paymentType = Arrays.asList(PaymentType.CASH, PaymentType.CREDIT_CARD);
        List<OrderStatus> orderStatus = Arrays.asList(
                OrderStatus.PENDING,
                OrderStatus.PAID,
                OrderStatus.READY,
                OrderStatus.DELIVERED);

        OrderDto order = orderServiceImpl.edit(id);
        List<OrderDetailsDto> orderDetails = orderDetailsServiceImpl.getAllOrderDetails();

        List<OrderDetailsDto> orderDetailsOnOrder = new ArrayList<>();
        Long orderId = order.getId();

        orderServiceImpl.setBackGroundColor(model,
                orderDetails, orderDetailsOnOrder, orderId);

        order.setOrderDetails(orderDetailsOnOrder);
        orderServiceImpl.calculateTotalAmount(order);
        orderServiceImpl.setTotalAmountToOrderRepository(order);

        model.addAttribute("order", order);
        model.addAttribute("tables", tableServiceImpl.getAllTables());
        model.addAttribute("paymentType", paymentType);
        model.addAttribute("orderStatus", orderStatus);
        model.addAttribute("orderDetails", orderDetailsOnOrder);

        return "order/editOrder.html";
    }

    @RequestMapping("order/{id}/update")
    public String updateOrder(@PathVariable("id") Long id,
                              @Valid @ModelAttribute("order") OrderDto orderDto,
                              Model model,
                              BindingResult bindingResult,
                              RedirectAttributes redirectAttributes) {
        if (bindingResult.hasErrors()) {
            return "order/create.html";
        }

        OrderDto updatedOrderDto = orderServiceImpl.updateOrder(id, orderDto);

        if (updatedOrderDto.getOrderStatus().equals(OrderStatus.PAID)) {

            Double calcualtedChange = orderServiceImpl.calculateAmountDue(updatedOrderDto.getTotalAmount(),
                    updatedOrderDto.getReceivedAmount());
            Double expectedTip = orderServiceImpl.calculateExpectedTip(updatedOrderDto.getTotalAmount());
            Double vatAmount = orderServiceImpl.calculateAmountWithVat(updatedOrderDto.getTotalAmount());
            Double totalAmountWithVat = orderServiceImpl.calculateTotalAmountWithVat(updatedOrderDto.getTotalAmount(),
                    vatAmount);
            updatedOrderDto.setAmountDue(calcualtedChange);
            updatedOrderDto.setTip(expectedTip);
            updatedOrderDto.setVatAmount(vatAmount);
            updatedOrderDto.setTotalAmountWithVat(totalAmountWithVat);

            model.addAttribute("order", updatedOrderDto);
            return "order/payOrder.html";
        }
        model.addAttribute("orderDto", updatedOrderDto);
        addSuccessFlashAttributeForOrder(redirectAttributes, Crud.UPDATE.toString(), id);

        return "redirect:/api/v1/order/orders";
    }

    @RequestMapping("/orderDetails/{id}/new")
    public String newOrderItem(@PathVariable("id") Long orderId,
                               Model model) {

        OrderDto order = orderServiceImpl.getOrderById(orderId);
        OrderDetailsDto orderDetails = new OrderDetailsDto();

        List<MenuDto> menuList = menuServiceImpl.getAllMenus();
        List<UnitOfMeasure> unitOfMeasuresList = Arrays.asList(
                UnitOfMeasure.PLATES,
                UnitOfMeasure.CUPS,
                UnitOfMeasure.PIECES);
        List<OrderDetailsStatus> orderDetailsStatuses = Arrays.asList(
                OrderDetailsStatus.COOKING,
                OrderDetailsStatus.READY);

        orderDetails.setOrderId(orderId);
        orderDetails.setMenu(menuList.get(0));

        model.addAttribute("orderDetails", orderDetails);
        model.addAttribute("menuList", menuList);
        model.addAttribute("unitOfMeasures", unitOfMeasuresList);
        model.addAttribute("orderDetailsStatuses", orderDetailsStatuses);
        model.addAttribute("order", order);

        return "order/orderDetails.html";
    }

    @RequestMapping(value = "/orderDetails", method = RequestMethod.POST)
    public String newOrderDetails(@Valid @ModelAttribute("orderDetails") OrderDetailsDto orderDetailsDto,
                                  BindingResult bindingResult,
                                  RedirectAttributes redirectAttributes) {


        if (bindingResult.hasErrors()) {
            return "order/addOrderItem.html";
        }

        OrderDto order = new OrderDto();
        List<OrderDetailsDto> orderDetailsDtoList = new ArrayList<>();

        OrderDetailsDto savedOrderDetailsDto = orderDetailsServiceImpl.createOrderDetails(orderDetailsDto);
        orderDetailsDtoList.add(savedOrderDetailsDto);

        order.setOrderDetails(orderDetailsDtoList);
        addSuccessFlashAttributeForOrderDetails(redirectAttributes, Crud.ADDE.toString(), savedOrderDetailsDto.getId());

        return "redirect:/api/v1/order/order?orderId=" + savedOrderDetailsDto.getOrderId();

    }

    @RequestMapping(value = "/order", method = RequestMethod.GET)
    public String showOrderForm(@RequestParam(name = "orderId") Long orderId,
                                Model model) {

        List<PaymentType> paymentType = Arrays.asList(
                PaymentType.CASH,
                PaymentType.CREDIT_CARD);
        List<OrderStatus> orderStatus = Arrays.asList(
                OrderStatus.PENDING,
                OrderStatus.PAID,
                OrderStatus.READY,
                OrderStatus.DELIVERED);

        OrderDto order = orderDetailsServiceImpl.getOrderById(orderId);
        List<OrderDetailsDto> orderDetails = orderServiceImpl.getOrderDetailsDtos(orderId, order);
        order.setOrderDetails(orderDetails);
        Double totalAmount = orderServiceImpl.calculateTotalAmount(order);
        order.setTotalAmount(totalAmount);
        orderServiceImpl.setTotalAmountToOrderRepository(order);

        model.addAttribute("order", order);
        model.addAttribute("tables", tableServiceImpl.getAllTables());
        model.addAttribute("orderDetails", orderDetails);
        model.addAttribute("paymentType", paymentType);
        model.addAttribute("orderStatus", orderStatus);

        return "order/addOrderItem.html";
    }

    @RequestMapping("/orderDetails/{id}/edit")
    public String editOrderDetails(Model model,
                                   @PathVariable("id") Long id) {

        List<MenuDto> menuList = menuServiceImpl.getAllMenus();
        List<UnitOfMeasure> unitOfMeasuresList = Arrays.asList(
                UnitOfMeasure.PLATES,
                UnitOfMeasure.CUPS,
                UnitOfMeasure.PIECES);
        List<OrderDetailsStatus> orderDetailsStatuses = Arrays.asList(
                OrderDetailsStatus.COOKING,
                OrderDetailsStatus.READY);

        OrderDetailsDto retrievedOrderDetails = orderDetailsServiceImpl.edit(id);
        model.addAttribute("unitOfMeasures", unitOfMeasuresList);
        model.addAttribute("orderDetails", retrievedOrderDetails);
        model.addAttribute("menuList", menuList);
        model.addAttribute("orderDetailsStatuses", orderDetailsStatuses);

        return "order/editOrderDetails.html";
    }

    @RequestMapping("orderDetails/{id}/update")
    public String updateOrderDetails(@PathVariable("id") Long id,
                                     @Valid @ModelAttribute("orderDetails") OrderDetailsDto orderDetailsDto,
                                     Model model,
                                     BindingResult bindingResult,
                                     RedirectAttributes redirectAttributes) {
        if (bindingResult.hasErrors()) {
            return "order/addOrderItem.html";
        }

        OrderDetailsDto updatedOrderDetails = orderDetailsServiceImpl.updateOrdersDetails(id, orderDetailsDto);

        OrderDto order = new OrderDto();
        List<OrderDetailsDto> orderDetailsDtoList = new ArrayList<>();

        orderDetailsDtoList.add(updatedOrderDetails);
        order.setOrderDetails(orderDetailsDtoList);
        addSuccessFlashAttributeForOrderDetails(redirectAttributes, Crud.ADDE.toString(), updatedOrderDetails.getId());

        model.addAttribute("orderDetails", updatedOrderDetails);
        addSuccessFlashAttributeForOrderDetails(redirectAttributes, Crud.UPDATE.toString(), updatedOrderDetails.getId());

        return "redirect:/api/v1/order/order?orderId=" + updatedOrderDetails.getOrderId();
    }

    @GetMapping
    @RequestMapping("orderDetails/{id}/delete")
    public String deleteOrderDetails(@PathVariable("id") Long orderDetailId,
                                     @RequestParam("id") Long orderId,
                                     RedirectAttributes redirectAttributes) {

        System.out.println("Controller method is executed.");

        orderDetailsServiceImpl.deleteOrderDetails(orderDetailId);
        addSuccessFlashAttributeForOrderDetails(redirectAttributes, Crud.DELETE.toString(), orderDetailId);

        return "redirect:/api/v1/order/order?orderId=" + orderId;
    }

    private void addSuccessFlashAttributeForOrderDetails(RedirectAttributes redirectAttributes, String action, Long entityId) {

        String successMessage = String.format("%s with ID %d was %sd successfully.", "OrderDetails", entityId, action.toLowerCase());
        redirectAttributes.addFlashAttribute("successMessage", successMessage);
    }

    private void addSuccessFlashAttributeForOrder(RedirectAttributes redirectAttributes, String action, Long orderId) {

        String successMessage = String.format("%s with ID %d was %sd successfully.", "Order", orderId, action.toLowerCase());
        redirectAttributes.addFlashAttribute("successMessage", successMessage);
    }
}
