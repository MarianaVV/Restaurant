package ro.somethinggoodtoeat.restaurantmanagementsystem.controller;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import ro.somethinggoodtoeat.restaurantmanagementsystem.dto.CategoryDto;
import ro.somethinggoodtoeat.restaurantmanagementsystem.enums.Crud;
import ro.somethinggoodtoeat.restaurantmanagementsystem.service.CategoryServiceImpl;

import javax.validation.Valid;

@RequestMapping("/api/v1/category")
@Controller
public class CategoryController {

    private final CategoryServiceImpl categoryServiceImpl;

    public CategoryController(CategoryServiceImpl categoryServiceImpl) {
        this.categoryServiceImpl = categoryServiceImpl;
    }

    @GetMapping("/categories")
    public String getAllCategories(@RequestParam(defaultValue = "0") int page,
                                   @RequestParam(defaultValue = "10") int size,
                                   Model model) {
        Page<CategoryDto> categoryPage = categoryServiceImpl.getAllCategories(page, size);
        model.addAttribute("categories", categoryPage.getContent());
        model.addAttribute("page", categoryPage);

        return "/category/category.html";
    }

    @RequestMapping("/category/new")
    public String newCategory(Model model) {

        model.addAttribute("category", new CategoryDto());

        return "category/create.html";
    }

    @RequestMapping("/category/{id}/edit")
    public String editCategory(Model model,
                               @PathVariable("id") Long id) {

        CategoryDto retrievedCategory = categoryServiceImpl.edit(id);
        model.addAttribute("category", retrievedCategory);

        return "category/edit.html";
    }

    @PostMapping("/category")
    public String createCategory(@Valid @ModelAttribute("category") CategoryDto category,
                                 BindingResult bindingResult,
                                 RedirectAttributes redirectAttributes) {

        if (bindingResult.hasErrors()) {
            return "category/create.html";
        }

        CategoryDto savedCategory = categoryServiceImpl.createCategory(category);
        addSuccessFlashAttribute(redirectAttributes, Crud.CREATE.toString(), savedCategory.getId());

        return "redirect:/api/v1/category/categories";
    }

    @RequestMapping("category/{id}/update")
    public String updateCategory(@PathVariable("id") Long id,
                                 @Valid @ModelAttribute("category") CategoryDto categoryDto,
                                 BindingResult bindingResult,
                                 Model model,
                                 RedirectAttributes redirectAttributes) {
        if (bindingResult.hasErrors()) {
            return "category/create.html";
        }

        CategoryDto updatedCategory = categoryServiceImpl.updateCategory(id, categoryDto);
        model.addAttribute("category", updatedCategory);
        addSuccessFlashAttribute(redirectAttributes, Crud.UPDATE.toString(), updatedCategory.getId());

        return "redirect:/api/v1/category/categories";
    }

    @GetMapping
    @RequestMapping("/category/{id}/delete")
    public String deleteCategory(@PathVariable("id") Long id,
                                 RedirectAttributes redirectAttributes) {

        categoryServiceImpl.deleteCategory(id);
        addSuccessFlashAttribute(redirectAttributes, Crud.DELETE.toString(), id);

        return "redirect:/api/v1/category/categories";
    }

    private void addSuccessFlashAttribute(RedirectAttributes redirectAttributes, String action, Long entityId) {

        String successMessage = String.format("%s with ID %d was %sd successfully.", "Category", entityId, action.toLowerCase());
        redirectAttributes.addFlashAttribute("successMessage", successMessage);
    }
}
