package ro.somethinggoodtoeat.restaurantmanagementsystem.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import ro.somethinggoodtoeat.restaurantmanagementsystem.dto.User;

@Controller
public class IndexController {

    @GetMapping({"", "/"})
    public String index(Model model) {

        model.addAttribute("user", new User());

        return "index.html";
    }

    @GetMapping({"/api/v1/home"})
    public String home() {

        return "home.html";
    }
}
