package ro.somethinggoodtoeat.restaurantmanagementsystem.controller;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import ro.somethinggoodtoeat.restaurantmanagementsystem.dto.TableDto;
import ro.somethinggoodtoeat.restaurantmanagementsystem.enums.Crud;
import ro.somethinggoodtoeat.restaurantmanagementsystem.enums.TableStatus;
import ro.somethinggoodtoeat.restaurantmanagementsystem.exception.SomethingGoodToEatException;
import ro.somethinggoodtoeat.restaurantmanagementsystem.service.TableServiceImpl;

import javax.validation.Valid;
import java.util.Arrays;
import java.util.List;

@RequestMapping("/api/v1/table")
@Controller
public class TableController {

    private final TableServiceImpl tableServiceImpl;

    public TableController(TableServiceImpl tableServiceImpl) {
        this.tableServiceImpl = tableServiceImpl;
    }

    @GetMapping("/tables")
    public String getAllTables(@RequestParam(defaultValue = "0") int page,
                               @RequestParam(defaultValue = "10") int size,
                               Model model) {

        Page<TableDto> tablePage = tableServiceImpl.getAllTables(page, size);
        List<TableDto> tables = tableServiceImpl.setBackgroundColor(model, tableServiceImpl.getAllTables());
        model.addAttribute("tables", tables);
        model.addAttribute("page", tablePage);

        return "/table/table.html";
    }

    @RequestMapping("/table/new")
    public String newTable(Model model) {

        TableDto tableDto = new TableDto();
        List<TableStatus> statuses = Arrays.asList(TableStatus.AVAILABLE, TableStatus.BOOKED, TableStatus.BUSY);
        model.addAttribute("table", tableDto);
        model.addAttribute("statuses", statuses);

        return "table/create.html";
    }

    @RequestMapping("/table/{id}/edit")
    public String editTable(Model model,
                            @PathVariable("id") Long id) {

        TableDto retrievedTable = tableServiceImpl.edit(id);
        List<TableStatus> statuses = Arrays.asList(TableStatus.AVAILABLE, TableStatus.BUSY, TableStatus.BOOKED);
        model.addAttribute("table", retrievedTable);
        model.addAttribute("statuses", statuses);

        return "table/edit.html";
    }

    @PostMapping("/table")
    public String createTable(@Valid @ModelAttribute("table") TableDto tableDto,
                              BindingResult bindingResult,
                              Model model,
                              RedirectAttributes redirectAttributes) {

        if (bindingResult.hasErrors()) {

            List<TableStatus> statuses = Arrays.asList(TableStatus.AVAILABLE, TableStatus.BUSY, TableStatus.BOOKED);
            model.addAttribute("statuses", statuses);

            return "table/create.html";
        }
        try {
            TableDto savedTable = tableServiceImpl.createTable(tableDto);
            redirectAttributes.addFlashAttribute("successMessage",
                    "Table with ID " + savedTable.getId() + " was created successfully.");

            return "redirect:/api/v1/table/tables";
        } catch (SomethingGoodToEatException e) {
            return "error.html";
        }
    }

    @RequestMapping("/table/{id}/update")
    public String updateTable(@PathVariable("id") Long id,
                              @Valid @ModelAttribute("table") TableDto tableDto,
                              BindingResult bindingResult,
                              Model model,
                              RedirectAttributes redirectAttributes) {
        if (bindingResult.hasErrors()) {

            List<TableStatus> statuses = Arrays.asList(TableStatus.AVAILABLE, TableStatus.BUSY, TableStatus.BOOKED);
            model.addAttribute("statuses", statuses);

            return "table/create.html";
        }

        TableDto updatedTable = tableServiceImpl.updateTable(id, tableDto);
        model.addAttribute("table", updatedTable);
        addSuccessFlashAttribute(redirectAttributes, Crud.UPDATE.toString(), updatedTable.getId());

        return "redirect:/api/v1/table/tables";
    }

    @GetMapping
    @RequestMapping("/table/{id}/delete")
    public String deleteTable(@PathVariable("id") Long id,
                              RedirectAttributes redirectAttributes) {

        tableServiceImpl.deleteTable(id);
        addSuccessFlashAttribute(redirectAttributes, Crud.DELETE.toString(), id);

        return "redirect:/api/v1/table/tables";
    }

    private void addSuccessFlashAttribute(RedirectAttributes redirectAttributes, String action, Long entityId) {

        String successMessage = String.format("%s with ID %d was %sd successfully.", "Table", entityId, action.toLowerCase());
        redirectAttributes.addFlashAttribute("successMessage", successMessage);
    }
}
