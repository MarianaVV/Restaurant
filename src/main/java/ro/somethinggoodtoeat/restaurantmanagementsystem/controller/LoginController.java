package ro.somethinggoodtoeat.restaurantmanagementsystem.controller;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import ro.somethinggoodtoeat.restaurantmanagementsystem.domain.UserEntity;
import ro.somethinggoodtoeat.restaurantmanagementsystem.dto.User;
import ro.somethinggoodtoeat.restaurantmanagementsystem.dto.securityDto.UserDetails;
import ro.somethinggoodtoeat.restaurantmanagementsystem.enums.Role;
import ro.somethinggoodtoeat.restaurantmanagementsystem.repository.security.UserRepository;
import ro.somethinggoodtoeat.restaurantmanagementsystem.service.LoginServiceImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;
import javax.validation.Valid;
import java.util.*;

@RequestMapping("/api/v1/login")
@Controller
public class LoginController {

    private final LoginServiceImpl loginService;
    private final AuthenticationManager authenticationManager;
    private final UserRepository userRepository;

    public LoginController(LoginServiceImpl loginService, AuthenticationManager authenticationManager,
                           UserRepository userRepository) {
        this.loginService = loginService;
        this.authenticationManager = authenticationManager;
        this.userRepository = userRepository;
    }

    @GetMapping("/signUp")
    public String signUpForm(Model model) {

        UserDetails userDetails = new UserDetails();
        List<Role> roles = Arrays.asList(Role.WAITER, Role.MANAGER, Role.CHEF);
        model.addAttribute("user", userDetails);
        model.addAttribute("roles", roles);

        return "login/signUp.html";
    }

    @PostMapping("/newUser")
    public String createUser(@Valid @ModelAttribute("user") UserDetails userDetails,
                             BindingResult bindingResult,
                             Model model,
                             @RequestParam("role") String selectedRole,
                             RedirectAttributes redirectAttributes) {
        if (bindingResult.hasErrors()) {

            List<Role> roles = Arrays.asList(Role.WAITER, Role.MANAGER, Role.CHEF);
            model.addAttribute("roles", roles);

            return "login/signUp.html";
        }
        userDetails.setRole(selectedRole);
        loginService.signUpUser(userDetails);
        addSuccessMessage(redirectAttributes);

        return "redirect:/";
    }

    @PostMapping("/signIn")
    public String signInUser(@Valid @ModelAttribute("user") User userDetails,
                             BindingResult bindingResult,
                             Model model,
                             RedirectAttributes redirectAttributes,
                             HttpServletRequest request,
                             HttpServletResponse response) {
        if (bindingResult.hasErrors()) {
            return "index.html";
        }
        try {
            Authentication authentication = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(userDetails.getUsername(), userDetails.getPassword())
            );

            SecurityContextHolder.getContext().setAuthentication(authentication);
        } catch (AuthenticationException e) {

            addAuthenticationFailureMessage(redirectAttributes);
            return "redirect:/";
        }

        extracted(userDetails, request, response);

        Collection<? extends GrantedAuthority> authorities = SecurityContextHolder.getContext().getAuthentication().getAuthorities();
        boolean isManager = authorities.stream()
                .anyMatch(auth -> auth.getAuthority().equals("ROLE_MANAGER"));
        boolean isWaiter = authorities.stream()
                .anyMatch(auth -> auth.getAuthority().equals("ROLE_WAITER"));
        boolean isCook = authorities.stream()
                .anyMatch(auth -> auth.getAuthority().equals("ROLE_CHEF"));

        if (isManager) {
            model.addAttribute("user", userDetails);
            return "login/managerLayout.html";
        }
        if (isWaiter) {
            model.addAttribute("user", userDetails);
            return "login/waiterLayout.html";
        }
        if (isCook) {
            model.addAttribute("user", userDetails);
            return "login/chefLayout.html";
        }
        return "login/error.html";
    }

    @Transactional
    void extracted(User userDetails, HttpServletRequest request, HttpServletResponse response) {
        UserEntity userEntity = userRepository.findByUsername(userDetails.getUsername()).orElseThrow(() ->
        {
            return new UsernameNotFoundException("User name: " + userDetails.getUsername() + "not found");
        });
    }

    @RequestMapping("/logout")
    public String logoutUser(RedirectAttributes redirectAttributes) {

        addSuccessLogoutMessage(redirectAttributes);
        return "redirect:/";
    }

    @RequestMapping("/access-denied")
    public String accessDeniedUser() {


        return "accessDenied.html";
    }

    private void addSuccessLogoutMessage(RedirectAttributes redirectAttributes) {

        String successLogoutMessage = String.format("User Logged Out succesfully!");
        redirectAttributes.addFlashAttribute("successLogoutMessage", successLogoutMessage);
    }

    private void addSuccessMessage(RedirectAttributes redirectAttributes) {

        String successMessage = "User successfully created!";
        redirectAttributes.addFlashAttribute("successMessage", successMessage);
    }

    private void addAuthenticationFailureMessage(RedirectAttributes redirectAttributes) {

        String failureMessage = "Authentication failed!Please try again.";
        redirectAttributes.addFlashAttribute("failureMessage", failureMessage);
    }
}
