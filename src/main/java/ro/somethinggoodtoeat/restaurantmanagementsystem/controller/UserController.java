package ro.somethinggoodtoeat.restaurantmanagementsystem.controller;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import ro.somethinggoodtoeat.restaurantmanagementsystem.dto.securityDto.UserDetails;
import ro.somethinggoodtoeat.restaurantmanagementsystem.enums.Crud;
import ro.somethinggoodtoeat.restaurantmanagementsystem.enums.Role;
import ro.somethinggoodtoeat.restaurantmanagementsystem.service.UserServiceImpl;

import javax.validation.Valid;
import java.util.Arrays;
import java.util.List;

@RequestMapping("/api/v1/user")
@Controller
public class UserController {

    private final UserServiceImpl userService;

    public UserController(UserServiceImpl userService) {

        this.userService = userService;
    }

    @GetMapping("/users")
    public String getAllUsers(@RequestParam(defaultValue = "0") int page,
                              @RequestParam(defaultValue = "10") int size,
                              Model model) {
        Page<UserDetails> userPage = userService.getAllUsers(page, size);
        model.addAttribute("users", userPage.getContent());
        model.addAttribute("page", userPage);

        return "/user/user.html";
    }

    @RequestMapping("/user/new")
    public String newUser(Model model) {

        UserDetails userDetails = new UserDetails();
        List<Role> roles = Arrays.asList(Role.WAITER, Role.MANAGER, Role.CHEF);
        model.addAttribute("user", userDetails);
        model.addAttribute("roles", roles);

        return "user/create.html";
    }

    @RequestMapping("/user/{id}/edit")
    public String editUser(Model model,
                           @PathVariable("id") Long id) {

        UserDetails retrievedUser = userService.edit(id);
        Role mappedUserRole = mapRole(retrievedUser.getRole());
        retrievedUser.setRole(mappedUserRole.name());
        List<Role> roles = Arrays.asList(Role.WAITER, Role.MANAGER, Role.CHEF);

        model.addAttribute("user", retrievedUser);
        model.addAttribute("roles", roles);

        return "user/edit.html";
    }

    @PostMapping("/user")
    public String createUser(@Valid @ModelAttribute("user") UserDetails userDetails,
                             BindingResult bindingResult,
                             Model model,
                             @RequestParam("role") String selectedRole,
                             RedirectAttributes redirectAttributes) {
        if (bindingResult.hasErrors()) {

            List<Role> roles = Arrays.asList(Role.WAITER, Role.MANAGER, Role.CHEF);
            model.addAttribute("roles", roles);

            return "user/create.html";
        }
        userDetails.setRole(selectedRole);
        UserDetails savedUser = userService.createUser(userDetails);
        addSuccessFlashAttribute(redirectAttributes, Crud.CREATE.toString(), savedUser.getId());

        return "redirect:/api/v1/user/users";
    }

    @RequestMapping("/user/{id}/update")
    public String updateUser(@PathVariable("id") Long id,
                             @Valid @ModelAttribute("user") UserDetails userDetails,
                             BindingResult bindingResult,
                             Model model,
                             RedirectAttributes redirectAttributes) {
        if (bindingResult.hasErrors()) {

            List<Role> roles = Arrays.asList(Role.WAITER, Role.MANAGER, Role.CHEF);
            model.addAttribute("roles", roles);

            return "user/create.html";
        }
        UserDetails updatedUser = userService.updateUser(id, userDetails);
        model.addAttribute("user", updatedUser);
        addSuccessFlashAttribute(redirectAttributes, Crud.UPDATE.toString(), updatedUser.getId());

        return "redirect:/api/v1/user/users";
    }

    @GetMapping
    @RequestMapping("/user/{id}/delete")
    public String deleteUser(@PathVariable("id") Long id,
                             RedirectAttributes redirectAttributes) {

        userService.deleteUser(id);
        addSuccessFlashAttribute(redirectAttributes, Crud.DELETE.toString(), id);

        return "redirect:/api/v1/user/users";
    }

    private void addSuccessFlashAttribute(RedirectAttributes redirectAttributes, String action, Long entityId) {

        String successMessage = String.format("%s with ID %d was %sd successfully.", "User", entityId, action.toLowerCase());
        redirectAttributes.addFlashAttribute("successMessage", successMessage);
    }

    public Role mapRole(String roleName) {

        Role mappedRole = Role.ROLE_WAITER;

        switch (roleName) {
            case "ROLE_WAITER":
                mappedRole = Role.WAITER;
                break;
            case "ROLE_MANAGER":
                mappedRole = Role.MANAGER;
                break;
            case "ROLE_CHEF":
                mappedRole = Role.CHEF;
                break;
            default:
                break;
        }
        return mappedRole;
    }
}
