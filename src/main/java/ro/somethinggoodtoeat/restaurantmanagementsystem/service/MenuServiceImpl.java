package ro.somethinggoodtoeat.restaurantmanagementsystem.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ro.somethinggoodtoeat.restaurantmanagementsystem.converter.CategoryDtoToCategoryEntityConverter;
import ro.somethinggoodtoeat.restaurantmanagementsystem.converter.CategoryEntityToCategoryDtoConverter;
import ro.somethinggoodtoeat.restaurantmanagementsystem.converter.MenuDtoToMenuEntityConverter;
import ro.somethinggoodtoeat.restaurantmanagementsystem.converter.MenuEntityToMenuDtoConverter;
import ro.somethinggoodtoeat.restaurantmanagementsystem.domain.CategoryEntity;
import ro.somethinggoodtoeat.restaurantmanagementsystem.domain.MenuEntity;
import ro.somethinggoodtoeat.restaurantmanagementsystem.dto.CategoryDto;
import ro.somethinggoodtoeat.restaurantmanagementsystem.dto.MenuDto;
import ro.somethinggoodtoeat.restaurantmanagementsystem.exception.SomethingGoodToEatException;
import ro.somethinggoodtoeat.restaurantmanagementsystem.repository.CategoryRepository;
import ro.somethinggoodtoeat.restaurantmanagementsystem.repository.MenuRepository;
import ro.somethinggoodtoeat.restaurantmanagementsystem.service.interfaces.MenuService;

import javax.transaction.Transactional;
import javax.validation.ConstraintViolation;
import javax.validation.Valid;
import javax.validation.ValidationException;
import javax.validation.Validator;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Set;

@Slf4j
@Service
public class MenuServiceImpl implements MenuService {

    private final MenuRepository menuRepository;
    private final CategoryRepository categoryRepository;
    private final CategoryDtoToCategoryEntityConverter categoryDtoToCategoryEntityConverter;
    private final CategoryEntityToCategoryDtoConverter categoryEntityToCategoryDtoConverter;
    private final MenuEntityToMenuDtoConverter menuEntityToMenuDtoConverter;
    private final MenuDtoToMenuEntityConverter menuDtoToMenuEntityConverter;

    private final Validator validator;


    public MenuServiceImpl(MenuRepository menuRepository,
                           CategoryRepository categoryRepository,
                           CategoryEntityToCategoryDtoConverter categoryEntityToCategoryDtoConverter,
                           CategoryDtoToCategoryEntityConverter categoryDtoToCategoryEntityConverter,
                           MenuEntityToMenuDtoConverter menuEntityToMenuDtoConverter,
                           MenuDtoToMenuEntityConverter menuDtoToMenuEntityConverter, Validator validator) {
        this.menuRepository = menuRepository;
        this.categoryRepository = categoryRepository;
        this.categoryDtoToCategoryEntityConverter = categoryDtoToCategoryEntityConverter;
        this.categoryEntityToCategoryDtoConverter = categoryEntityToCategoryDtoConverter;
        this.menuEntityToMenuDtoConverter = menuEntityToMenuDtoConverter;
        this.menuDtoToMenuEntityConverter = menuDtoToMenuEntityConverter;
        this.validator = validator;
    }

    public Page<MenuDto> getAllMenus(int page, int size) {

        try {
            Pageable pageable = PageRequest.of(page, size);
            Page<MenuEntity> menuPage = menuRepository.findAll(pageable);
            List<MenuDto> menuDtoList = menuEntityToMenuDtoConverter.toDTOList(menuPage.getContent());

            return new PageImpl<>(menuDtoList, pageable, menuPage.getTotalElements());

        } catch (Exception e) {
            log.error("An error occurred while retrieving menus.", e);
            throw new SomethingGoodToEatException("Error while retrieving menus", e, "SERVER_ERROR");
        }
    }


    @Override
    public List<MenuDto> getAllMenus() {

        try {
            List<MenuEntity> returnedList = menuRepository.findAll();
            return menuEntityToMenuDtoConverter.toDTOList(returnedList);
        } catch (Exception e) {
            log.error("An error occurred while retrieving menus.", e);
            throw new SomethingGoodToEatException("Error while retrieving menus", e, "SERVER_ERROR");
        }
    }

    @Override
    @Transactional
    public MenuDto createMenu(@Valid MenuDto menuDto) {

        try {
            CategoryEntity convertedCategoryEntity;
            if (menuDto.getCategory() != null) {
                convertedCategoryEntity = categoryDtoToCategoryEntityConverter.convert(menuDto.getCategory());
            }

            if (menuDto.getCategory() == null) {
                String pizzaCategory = "Pizza";
                convertedCategoryEntity = categoryRepository.findByName(pizzaCategory).get();
                CategoryDto categoryDto = categoryEntityToCategoryDtoConverter.convert(convertedCategoryEntity);
                menuDto.setCategory(categoryDto);
            }

            CategoryEntity categoryEntity = categoryRepository.findById(menuDto.getCategory().getId()).orElseThrow(
                    () -> new SomethingGoodToEatException("Not found category with id:" + menuDto.getCategory().getId(), "CLIENT_ERROR"));
            MenuEntity createdMenuEntity = menuDtoToMenuEntityConverter.convert(menuDto);
            createdMenuEntity.setCategory(categoryEntity);
            MenuEntity savedMenuEntity = menuRepository.save(createdMenuEntity);
            log.debug("Saved Category with Id:" + savedMenuEntity.getId());

            return menuEntityToMenuDtoConverter.convert(savedMenuEntity);
        } catch (Exception e) {
            log.error("An error occurred while creating a menu.", e);
            throw new SomethingGoodToEatException("Error while creating a menu", e, "SERVER_ERROR");
        }
    }

    @Transactional
    public MenuDto updateMenu(Long id, @Valid MenuDto menuDto) {

        try {
            validate(menuDto);
            MenuEntity updatedMenuEntity = menuRepository.findById(id).orElseThrow(()
                    -> new SomethingGoodToEatException("Not found menu with id:" + id, "CLIENT_ERROR"));

            MenuDto convertedMenu = menuEntityToMenuDtoConverter.convert(updatedMenuEntity);
            if (convertedMenu != null) {
                convertedMenu.setId(menuDto.getId());
                convertedMenu.setPrice(menuDto.getPrice());
                convertedMenu.setName(menuDto.getName());
                convertedMenu.setImage(menuDto.getImage());
                convertedMenu.setDescription(menuDto.getDescription());
                convertedMenu.setCategory(menuDto.getCategory());
            }
            MenuEntity updatedNewMenuEntity = menuDtoToMenuEntityConverter.convert(convertedMenu);
            menuRepository.save(updatedNewMenuEntity);

            return convertedMenu;
        } catch (Exception e) {
            log.error("An error occurred while updating a menu.", e);
            throw new SomethingGoodToEatException("Error while updating a menu", e, "SERVER_ERROR");
        }
    }

    @Override
    @Transactional
    public void deleteMenu(Long id) {

        try {
            MenuEntity deletedMenuEntity = menuRepository.findById(id).orElseThrow(() ->
                    new SomethingGoodToEatException("Not found menu with id:" + id, "CLIENT_ERROR"));
            menuRepository.delete(deletedMenuEntity);
            log.debug("Deleted Category with Id:" + id);
        } catch (Exception e) {
            log.error("An error occurred while deleting a menu.", e);
            throw new SomethingGoodToEatException("Error while deleting a menu", e, "SERVER_ERROR");
        }
    }

    @Override
    public MenuDto edit(Long id) {

        try {
            MenuEntity menuEntity = menuRepository.findById(id).orElseThrow(()
                    -> new SomethingGoodToEatException("Not found menu with id:" + id, "CLIENT_ERROR"));
            MenuDto convertedMenu = menuEntityToMenuDtoConverter.convert(menuEntity);

            return convertedMenu;
        } catch (Exception e) {
            log.error("An error occurred while editing a menu.", e);
            throw new SomethingGoodToEatException("Error while editing a menu", e, "SERVER_ERROR");
        }
    }

    void validate(MenuDto menuDto) {
        Set<ConstraintViolation<MenuDto>> violations = validator.validate(menuDto);
        if (!violations.isEmpty()) {
            log.debug("Validation for CategoryDto failed");
            throw new ValidationException("DTO validation failed", (Throwable) violations);
        }
    }

    public MenuDto getMenuById(Long menuItemId) {

        MenuEntity menuEntity = menuRepository.findById(menuItemId).orElseThrow(()
                -> new SomethingGoodToEatException("Not found menu with id:" + menuItemId, "CLIENT_ERROR"));
        MenuDto convertedMenu = menuEntityToMenuDtoConverter.convert(menuEntity);

        return convertedMenu;
    }

    public List<MenuDto> getAllMenusByIsPopular() {

        List<MenuEntity> menuEntityList = menuRepository.findAllByIsPopular("Y");
        List<MenuDto> menuDtoList = menuEntityToMenuDtoConverter.toDTOList(menuEntityList);

        return menuDtoList;
    }
}
