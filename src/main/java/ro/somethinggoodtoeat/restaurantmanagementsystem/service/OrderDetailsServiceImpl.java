package ro.somethinggoodtoeat.restaurantmanagementsystem.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ro.somethinggoodtoeat.restaurantmanagementsystem.converter.OrderDetailsDtoToOrderDetailsEntityConverter;
import ro.somethinggoodtoeat.restaurantmanagementsystem.converter.OrderDetailsEntityToOrderDetailsDtoConverter;
import ro.somethinggoodtoeat.restaurantmanagementsystem.converter.OrderDtoToOrderEntityConverter;
import ro.somethinggoodtoeat.restaurantmanagementsystem.converter.OrderEntityToOrderDtoConverter;
import ro.somethinggoodtoeat.restaurantmanagementsystem.domain.OrderDetailsEntity;
import ro.somethinggoodtoeat.restaurantmanagementsystem.domain.OrderEntity;
import ro.somethinggoodtoeat.restaurantmanagementsystem.dto.OrderDetailsDto;
import ro.somethinggoodtoeat.restaurantmanagementsystem.dto.OrderDto;
import ro.somethinggoodtoeat.restaurantmanagementsystem.enums.OrderDetailsStatus;
import ro.somethinggoodtoeat.restaurantmanagementsystem.exception.SomethingGoodToEatException;
import ro.somethinggoodtoeat.restaurantmanagementsystem.repository.OrderDetailsRepository;
import ro.somethinggoodtoeat.restaurantmanagementsystem.repository.OrderRepository;
import ro.somethinggoodtoeat.restaurantmanagementsystem.service.interfaces.OrderDetailsService;

import javax.transaction.Transactional;
import javax.validation.ConstraintViolation;
import javax.validation.ValidationException;
import javax.validation.Validator;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Slf4j
@Service
public class OrderDetailsServiceImpl implements OrderDetailsService {

    private final OrderDetailsRepository orderDetailsRepository;
    private final OrderDetailsEntityToOrderDetailsDtoConverter orderDetailsEntityToOrderDetailsDtoConverter;
    private final OrderDetailsDtoToOrderDetailsEntityConverter orderDetailsDtoToOrderDetailsEntityConverter;
    private final OrderDtoToOrderEntityConverter orderDtoToOrderEntityConverter;
    private final OrderEntityToOrderDtoConverter orderEntityToOrderDtoConverter;
    private final OrderRepository orderRepository;
    private final Validator validator;


    public OrderDetailsServiceImpl(OrderDetailsRepository orderDetailsRepository,
                                   OrderDetailsEntityToOrderDetailsDtoConverter orderDetailsEntityToOrderDetailsDtoConverter,
                                   OrderDetailsDtoToOrderDetailsEntityConverter orderDetailsDtoToOrderDetailsEntityConverter,
                                   OrderDtoToOrderEntityConverter orderDtoToOrderEntityConverter,
                                   OrderEntityToOrderDtoConverter orderEntityToOrderDtoConverter,
                                   OrderRepository orderRepository, Validator validator) {
        this.orderDetailsRepository = orderDetailsRepository;
        this.orderDetailsEntityToOrderDetailsDtoConverter = orderDetailsEntityToOrderDetailsDtoConverter;
        this.orderDetailsDtoToOrderDetailsEntityConverter = orderDetailsDtoToOrderDetailsEntityConverter;
        this.orderDtoToOrderEntityConverter = orderDtoToOrderEntityConverter;
        this.orderEntityToOrderDtoConverter = orderEntityToOrderDtoConverter;
        this.orderRepository = orderRepository;
        this.validator = validator;
    }

    @Override
    public List<OrderDetailsDto> getAllOrderDetails() {

        List<OrderDetailsEntity> returnedList = orderDetailsRepository.findAll();
        return orderDetailsEntityToOrderDetailsDtoConverter.toDTOList(returnedList);

    }

    public List<OrderDetailsDto> getAllOrderDetailsById(Long id) {

        List<OrderDetailsEntity> returnedList = orderDetailsRepository.getAllByOrderId(id);
        return orderDetailsEntityToOrderDetailsDtoConverter.toDTOList(returnedList);

    }

    @Transactional
    public OrderDetailsDto createOrderDetails(OrderDetailsDto orderDetailsDto) {

        validate(orderDetailsDto);
        OrderDetailsEntity convertedOrderDetailsEntity = orderDetailsDtoToOrderDetailsEntityConverter.convert(orderDetailsDto);
        OrderDetailsEntity savedOrderDetailsEntity = orderDetailsRepository.save(convertedOrderDetailsEntity);
        log.debug("Saved OrderDetails with Id:" + savedOrderDetailsEntity.getId());

        return orderDetailsEntityToOrderDetailsDtoConverter.convert(savedOrderDetailsEntity);
    }

    @Transactional
    public OrderDto createOrder(OrderDto orderDto) {

        validate(orderDto);
        OrderEntity convertedOrderEntity = orderDtoToOrderEntityConverter.convert(orderDto);
        OrderEntity savedOrderEntity = orderRepository.save(convertedOrderEntity);

        log.debug("Saved Order with Id:" + savedOrderEntity.getId());
        return orderEntityToOrderDtoConverter.convert(savedOrderEntity);
    }

    void validate(OrderDto orderDto) {
        Set<ConstraintViolation<OrderDto>> violations = validator.validate(orderDto);
        if (!violations.isEmpty()) {
            log.debug("Validation for OrderDto failed");
            throw new ValidationException("DTO validation failed", (Throwable) violations);
        }
    }

    void validate(OrderDetailsDto orderDetailsDto) {
        Set<ConstraintViolation<OrderDetailsDto>> violations = validator.validate(orderDetailsDto);
        if (!violations.isEmpty()) {
            log.debug("Validation for OrderDetails failed");
            throw new ValidationException("DTO validation failed", (Throwable) violations);
        }
    }

    public OrderDetailsDto edit(Long id) {

        OrderDetailsEntity orderDetailsEntity = orderDetailsRepository.findById(id).orElseThrow(()
                -> new SomethingGoodToEatException("Not found order details with id:" + id, "CLIENT_ERROR"));
        OrderDetailsDto convertedOrderDetailsDto = orderDetailsEntityToOrderDetailsDtoConverter.convert(orderDetailsEntity);

        return convertedOrderDetailsDto;
    }

    @Transactional
    public OrderDetailsDto updateOrdersDetails(Long id, OrderDetailsDto orderDetailsDto) {

        validate(orderDetailsDto);
        OrderDetailsEntity orderDetailsEntity = orderDetailsRepository.findById(id).orElseThrow(()
                -> new SomethingGoodToEatException("Not found order details with id:" + id, "CLIENT_ERROR"));

        OrderDetailsDto convertedOrderDetailsDto = orderDetailsEntityToOrderDetailsDtoConverter.convert(orderDetailsEntity);
        if (convertedOrderDetailsDto != null) {
            convertedOrderDetailsDto.setId(orderDetailsDto.getId());
            convertedOrderDetailsDto.setUnitOfMeasure(orderDetailsDto.getUnitOfMeasure());
            convertedOrderDetailsDto.setAmount(orderDetailsDto.getAmount());
            convertedOrderDetailsDto.setOrderDetailsStatus(orderDetailsDto.getOrderDetailsStatus());
        }

        OrderDetailsEntity updatedOrderDetailsEntity = orderDetailsDtoToOrderDetailsEntityConverter.convert(convertedOrderDetailsDto);
        if (updatedOrderDetailsEntity != null) {
            orderDetailsRepository.save(updatedOrderDetailsEntity);
        }
        return convertedOrderDetailsDto;
    }

    public void deleteOrderDetails(Long id) {

        OrderDetailsEntity orderDetailsEntity = orderDetailsRepository.findById(id).orElseThrow(()
                -> new SomethingGoodToEatException("Not found order details with id:" + id, "CLIENT_ERROR"));

        orderDetailsRepository.delete(orderDetailsEntity);
        log.debug("Deleted order details with Id:" + id);
    }

    public OrderDto getOrderById(Long orderId) {

        return orderEntityToOrderDtoConverter.convert(orderRepository.getById(orderId));
    }

    public List<OrderDetailsDto> getOrderDetailsByOrderId(Long orderId) {

        List<OrderDetailsEntity> orderDetailsList = orderDetailsRepository.findAll();
        List<OrderDetailsDto> orderDetailsDtoList =
                orderDetailsEntityToOrderDetailsDtoConverter.toDTOList(orderDetailsList);
        List<OrderDetailsDto> orderDetailsListByOrderId = new ArrayList<>();

        for (OrderDetailsDto orderDetail : orderDetailsDtoList) {
            if (orderDetail.getOrderId().equals(orderId)) {
                orderDetailsListByOrderId.add(orderDetail);
            }
        }

        return orderDetailsListByOrderId;
    }

    public OrderDetailsDto getOrderDetailsById(Long id) {

        OrderDetailsEntity orderDetailsEntity = orderDetailsRepository.findById(id).orElseThrow(()
                -> new SomethingGoodToEatException("Not found orderDetails with id:" + id, "SERVER_ERROR"));
        if (orderDetailsEntity.getOrderDetailsStatus().equals(OrderDetailsStatus.COOKING)) {
            orderDetailsEntity.setOrderDetailsStatus(OrderDetailsStatus.READY);
        }
        orderDetailsRepository.save(orderDetailsEntity);

        return orderDetailsEntityToOrderDetailsDtoConverter.convert(orderDetailsEntity);
    }
}
