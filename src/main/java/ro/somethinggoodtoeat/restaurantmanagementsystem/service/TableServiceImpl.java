package ro.somethinggoodtoeat.restaurantmanagementsystem.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import ro.somethinggoodtoeat.restaurantmanagementsystem.converter.TableDtoToTableEntityConverter;
import ro.somethinggoodtoeat.restaurantmanagementsystem.converter.TableEntityToTableDtoConverter;
import ro.somethinggoodtoeat.restaurantmanagementsystem.domain.TableEntity;
import ro.somethinggoodtoeat.restaurantmanagementsystem.dto.TableDto;
import ro.somethinggoodtoeat.restaurantmanagementsystem.enums.TableStatus;
import ro.somethinggoodtoeat.restaurantmanagementsystem.exception.SomethingGoodToEatException;
import ro.somethinggoodtoeat.restaurantmanagementsystem.repository.TableRepository;
import ro.somethinggoodtoeat.restaurantmanagementsystem.service.interfaces.TableService;

import javax.transaction.Transactional;
import javax.validation.ConstraintViolation;
import javax.validation.Valid;
import javax.validation.ValidationException;
import javax.validation.Validator;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Slf4j
@Service
public class TableServiceImpl implements TableService {

    private final TableRepository tableRepository;
    private final TableDtoToTableEntityConverter tableDtoToTableEntityConverter;
    private final TableEntityToTableDtoConverter tableEntityToTableDtoConverter;
    private final OrderServiceImpl orderService;
    private final Validator validator;

    public TableServiceImpl(TableRepository tableRepository, TableDtoToTableEntityConverter tableDtoToTableEntityConverter,
                            TableEntityToTableDtoConverter tableEntityToTableDtoConverter,
                            OrderServiceImpl orderService,
                            Validator validator) {
        this.tableRepository = tableRepository;
        this.tableDtoToTableEntityConverter = tableDtoToTableEntityConverter;
        this.tableEntityToTableDtoConverter = tableEntityToTableDtoConverter;
        this.orderService = orderService;
        this.validator = validator;
    }

    public Page<TableDto> getAllTables(int pageNumber, int pageSize) {

        try {
            Pageable pageable = PageRequest.of(pageNumber, pageSize);
            Page<TableEntity> tablePage = tableRepository.findAll(pageable);
            List<TableDto> tableDtoList = tableEntityToTableDtoConverter.toDTOList(tablePage.getContent());

            return new PageImpl<>(tableDtoList, pageable, tablePage.getTotalElements());
        } catch (Exception e) {
            log.error("An error occurred while retrieving tables.", e);
            throw new SomethingGoodToEatException("Error while retrieving tables", e, "SERVER_ERROR");
        }
    }

    @Override
    @Transactional
    public TableDto createTable(@Valid TableDto tableDto) {

        try {
            validate(tableDto);
            TableEntity convertedTableEntity = tableDtoToTableEntityConverter.convert(tableDto);
            TableEntity savedTableEntity = tableRepository.save(convertedTableEntity);
            log.debug("Saved Table with Id:" + savedTableEntity.getId());

            return tableEntityToTableDtoConverter.convert(savedTableEntity);
        } catch (Exception e) {
            log.error("An error occurred while creating a table.", e);
            throw new SomethingGoodToEatException("Error while creating a table", e, "SERVER_ERROR");
        }
    }

    @Override
    @Transactional
    public TableDto edit(Long id) {

        try {
            TableEntity tableEntity = tableRepository.findById(id).orElseThrow(()
                    -> new SomethingGoodToEatException("Not found table with id:" + id, "CLIENT_ERROR"));
            TableDto convertedTable = tableEntityToTableDtoConverter.convert(tableEntity);

            return convertedTable;
        } catch (Exception e) {
            log.error("An error occurred while editing a table.", e);
            throw new SomethingGoodToEatException("Error while editing a table", e, "SERVER_ERROR");
        }
    }

    @Override
    @Transactional
    public TableDto updateTable(Long id, @Valid TableDto tableDto) {

        try {
            TableEntity tableEntity = tableRepository.findById(id).orElseThrow(()
                    -> new SomethingGoodToEatException("Not found table with id:" + id, "CLIENT_ERROR"));

            TableDto convertedTable = tableEntityToTableDtoConverter.convert(tableEntity);
            if (convertedTable != null) {
                convertedTable.setId(tableDto.getId());
                convertedTable.setName(tableDto.getName());
                convertedTable.setStatus(tableDto.getStatus());
                convertedTable.setNumberOfSeats(tableDto.getNumberOfSeats());
            }
            TableEntity updatedNewTableEntity = tableDtoToTableEntityConverter.convert(convertedTable);
            if (updatedNewTableEntity != null) {
                tableRepository.save(updatedNewTableEntity);
            }
            return convertedTable;
        } catch (Exception e) {
            log.error("An error occurred while updating a table.", e);
            throw new SomethingGoodToEatException("Error while updating a table", e, "SERVER_ERROR");
        }
    }

    @Override
    @Transactional
    public void deleteTable(Long id) {

        try {

            TableEntity tableEntity = tableRepository.findById(id).orElseThrow(()
                    -> new SomethingGoodToEatException("Not found category with id:" + id, "CLIENT_ERROR"));

            tableRepository.delete(tableEntity);
            log.debug("Deleted Table with Id:" + id);
        } catch (Exception e) {
            log.error("An error occurred while deleting a table.", e);
            throw new SomethingGoodToEatException("Error while deleting a table", e, "SERVER_ERROR");
        }
    }

    @Override
    @Transactional
    public List<TableDto> getAllTablesWithStatusAvailable(TableStatus status) {

        try {
            List<TableEntity> returnedList = tableRepository.findByTableStatus(status);
            return tableEntityToTableDtoConverter.toDTOList(returnedList);
        } catch (Exception e) {
            log.error("An error occurred while getting all tables.", e);
            throw new SomethingGoodToEatException("Error while getting all tables", e, "SERVER_ERROR");
        }
    }

    @Override
    public List<TableDto> getAllTables() {

        try {
            List<TableEntity> returnedList = tableRepository.findAll();
            return tableEntityToTableDtoConverter.toDTOList(returnedList);
        } catch (Exception e) {
            log.error("An error occurred while getting all tables.", e);
            throw new SomethingGoodToEatException("Error while getting all tables", e, "SERVER_ERROR");
        }
    }

    void validate(@Valid TableDto category) {
        Set<ConstraintViolation<TableDto>> violations = validator.validate(category);
        if (!violations.isEmpty()) {
            log.debug("Validation for TableDto failed");
            throw new ValidationException("DTO validation failed", (Throwable) violations);
        }
    }

    public List<TableDto> setBackgroundColor(Model model, List<TableDto> allTables) {

        List<TableDto> newList = new ArrayList<>();
        for (TableDto table : allTables) {

            String statusColor = orderService.getStatusColor(String.valueOf(table.getStatus()));
            table.setStatusColor(statusColor);
            table.setBackGroundColor(statusColor);
            model.addAttribute("backgroundColor", table.getStatusColor());
            newList.add(table);
        }
        return newList;
    }
}
