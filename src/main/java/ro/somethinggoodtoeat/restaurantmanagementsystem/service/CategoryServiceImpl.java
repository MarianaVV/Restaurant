package ro.somethinggoodtoeat.restaurantmanagementsystem.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import ro.somethinggoodtoeat.restaurantmanagementsystem.converter.CategoryDtoToCategoryEntityConverter;
import ro.somethinggoodtoeat.restaurantmanagementsystem.converter.CategoryEntityToCategoryDtoConverter;
import ro.somethinggoodtoeat.restaurantmanagementsystem.domain.CategoryEntity;
import ro.somethinggoodtoeat.restaurantmanagementsystem.dto.CategoryDto;
import ro.somethinggoodtoeat.restaurantmanagementsystem.exception.SomethingGoodToEatException;
import ro.somethinggoodtoeat.restaurantmanagementsystem.repository.CategoryRepository;
import ro.somethinggoodtoeat.restaurantmanagementsystem.service.interfaces.CategoryService;

import javax.transaction.Transactional;
import javax.validation.ConstraintViolation;
import javax.validation.Valid;
import javax.validation.ValidationException;
import javax.validation.Validator;
import java.util.List;
import java.util.Set;

@Slf4j
@Service
public class CategoryServiceImpl implements CategoryService {
    private final CategoryRepository categoryRepository;
    private final CategoryDtoToCategoryEntityConverter categoryDtoToCategoryEntityConverter;
    private final CategoryEntityToCategoryDtoConverter categoryEntityToCategoryDtoConverter;
    private final Validator validator;

    public CategoryServiceImpl(CategoryRepository categoryRepository,
                               CategoryDtoToCategoryEntityConverter categoryDtoToCategoryEntityConverter,
                               CategoryEntityToCategoryDtoConverter categoryEntityToCategoryDtoConverter,
                               Validator validator) {

        this.categoryRepository = categoryRepository;
        this.categoryDtoToCategoryEntityConverter = categoryDtoToCategoryEntityConverter;
        this.categoryEntityToCategoryDtoConverter = categoryEntityToCategoryDtoConverter;
        this.validator = validator;
    }

    public Page<CategoryDto> getAllCategories(int pageNumber, int pageSize) {

        try {
            Pageable pageable = PageRequest.of(pageNumber, pageSize);
            Page<CategoryEntity> categoryPage = categoryRepository.findAll(pageable);
            List<CategoryDto> categoryDtoList = categoryEntityToCategoryDtoConverter.toDTOList(categoryPage.getContent());

            return new PageImpl<>(categoryDtoList, pageable, categoryPage.getTotalElements());

        } catch (Exception e) {
            log.error("An error occurred while retrieving categories.", e);
            throw new SomethingGoodToEatException("Error while retrieving categories", e, "SERVER_ERROR");
        }
    }

    @Override
    public List<CategoryDto> getAllCategories() {

        try {
            List<CategoryEntity> returnedList = categoryRepository.findAll();
            return categoryEntityToCategoryDtoConverter.toDTOList(returnedList);
        } catch (Exception e) {
            log.error("An error occurred while retrieving categories.", e);
            throw new SomethingGoodToEatException("Error while retrieving categories", e, "SERVER_ERROR");
        }
    }

    @Override
    @Transactional
    public CategoryDto createCategory(@Valid CategoryDto category) {

        try {
            validate(category);
            CategoryEntity convertedCategoryEntity = categoryDtoToCategoryEntityConverter.convert(category);
            CategoryEntity savedCategoryEntity = categoryRepository.save(convertedCategoryEntity);
            log.debug("Saved Category with Id:" + savedCategoryEntity.getId());

            return categoryEntityToCategoryDtoConverter.convert(savedCategoryEntity);

        } catch (Exception e) {
            log.error("An error occurred while creating a category.", e);
            throw new SomethingGoodToEatException("Error while creating a category", e, "SERVER_ERROR");
        }
    }

    @Override
    @Transactional
    public CategoryDto updateCategory(Long id, @Valid CategoryDto updatedCategory) {

        try {
            CategoryEntity categoryEntity = getCategoryById(id);

            CategoryDto convertedCategory = categoryEntityToCategoryDtoConverter.convert(categoryEntity);
            if (convertedCategory != null) {
                convertedCategory.setId(updatedCategory.getId());
                convertedCategory.setName(updatedCategory.getName());
            }
            CategoryEntity updatedNewCategoryEntity = categoryDtoToCategoryEntityConverter.convert(convertedCategory);
            if (updatedNewCategoryEntity != null) {
                categoryRepository.save(updatedNewCategoryEntity);
            }
            return convertedCategory;

        } catch (Exception e) {
            log.error("An error occurred while updating a category.", e);
            throw new SomethingGoodToEatException("Error while updating a category", e, "SERVER_ERROR");
        }
    }

    @Override
    @Transactional
    public void deleteCategory(Long id) throws SomethingGoodToEatException {

        CategoryEntity categoryEntity = getCategoryById(id);

        categoryRepository.delete(categoryEntity);
        log.debug("Deleted Category with Id:" + id);
    }

    @Override
    public CategoryDto edit(Long id) {

        try {
            CategoryEntity categoryEntity = getCategoryById(id);
            return categoryEntityToCategoryDtoConverter.convert(categoryEntity);
        } catch (Exception e) {
            log.error("An error occurred while editing a category.", e);
            throw new SomethingGoodToEatException("Error while editing a category", e, "SERVER_ERROR");
        }
    }

    private CategoryEntity getCategoryById(Long id) {
        return categoryRepository.findById(id)
                .orElseThrow(() -> new SomethingGoodToEatException("Not found category with id: " + id, "SERVER_ERROR"));
    }

    void validate(CategoryDto category) {
        Set<ConstraintViolation<CategoryDto>> violations = validator.validate(category);
        if (!violations.isEmpty()) {
            log.debug("Validation for CategoryDto failed");
            throw new ValidationException("DTO validation failed", (Throwable) violations);
        }
    }
}
