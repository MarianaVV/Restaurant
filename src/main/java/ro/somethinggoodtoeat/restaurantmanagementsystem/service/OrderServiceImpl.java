package ro.somethinggoodtoeat.restaurantmanagementsystem.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import ro.somethinggoodtoeat.restaurantmanagementsystem.converter.*;
import ro.somethinggoodtoeat.restaurantmanagementsystem.domain.OrderDetailsEntity;
import ro.somethinggoodtoeat.restaurantmanagementsystem.domain.OrderEntity;
import ro.somethinggoodtoeat.restaurantmanagementsystem.domain.TableEntity;
import ro.somethinggoodtoeat.restaurantmanagementsystem.dto.OrderDetailsDto;
import ro.somethinggoodtoeat.restaurantmanagementsystem.dto.OrderDto;
import ro.somethinggoodtoeat.restaurantmanagementsystem.enums.OrderDetailsStatus;
import ro.somethinggoodtoeat.restaurantmanagementsystem.enums.OrderStatus;
import ro.somethinggoodtoeat.restaurantmanagementsystem.exception.SomethingGoodToEatException;
import ro.somethinggoodtoeat.restaurantmanagementsystem.repository.OrderDetailsRepository;
import ro.somethinggoodtoeat.restaurantmanagementsystem.repository.OrderRepository;
import ro.somethinggoodtoeat.restaurantmanagementsystem.repository.TableRepository;
import ro.somethinggoodtoeat.restaurantmanagementsystem.service.interfaces.OrderService;
import javax.transaction.Transactional;
import javax.validation.ConstraintViolation;
import javax.validation.ValidationException;
import javax.validation.Validator;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Slf4j
@Service
public class OrderServiceImpl implements OrderService {

    private final OrderRepository orderRepository;

    private final OrderDetailsRepository orderDetailsRepository;
    private final OrderDtoToOrderEntityConverter orderDtoToOrderEntityConverter;
    private final OrderEntityToOrderDtoConverter orderEntityToOrderDtoConverter;
    private final MenuDtoToMenuEntityConverter menuDtoToMenuEntityConverter;
    private final OrderDetailsServiceImpl orderDetailsServiceImpl;
    private final Validator validator;
    private final TableRepository tableRepository;

    public OrderServiceImpl(OrderRepository orderRepository, OrderDetailsRepository orderDetailsRepository,
                            OrderDtoToOrderEntityConverter orderDtoToOrderEntityConverter,
                            OrderEntityToOrderDtoConverter orderEntityToOrderDtoConverter,
                            MenuDtoToMenuEntityConverter menuDtoToMenuEntityConverter,
                            OrderDetailsServiceImpl orderDetailsServiceImpl,
                            Validator validator,
                            TableRepository tableRepository) {
        this.orderRepository = orderRepository;
        this.orderDetailsRepository = orderDetailsRepository;
        this.orderDtoToOrderEntityConverter = orderDtoToOrderEntityConverter;
        this.orderEntityToOrderDtoConverter = orderEntityToOrderDtoConverter;
        this.menuDtoToMenuEntityConverter = menuDtoToMenuEntityConverter;
        this.orderDetailsServiceImpl = orderDetailsServiceImpl;
        this.validator = validator;
        this.tableRepository = tableRepository;
    }

    public Page<OrderDto> getAllOrders(int pageNumber, int pageSize) {

        try {
            Pageable pageable = PageRequest.of(pageNumber, pageSize);
            Page<OrderEntity> orderPage = orderRepository.findAll(pageable);
            List<OrderDto> orderDtoList = orderEntityToOrderDtoConverter.toDTOList(orderPage.getContent());

            return new PageImpl<>(orderDtoList, pageable, orderPage.getTotalElements());
        } catch (Exception e) {
            log.error("An error occurred while retrieving orders.", e);
            throw new SomethingGoodToEatException("Error while retrieving orders", e, "SERVER_ERROR");
        }
    }

    @Override
    public List<OrderDto> getAllOrders() {

        try {
            List<OrderEntity> returnedList = orderRepository.findAll();
            return orderEntityToOrderDtoConverter.toDTOList(returnedList);
        } catch (Exception e) {
            log.error("An error occurred while retrieving orders.", e);
            throw new SomethingGoodToEatException("Error while retrieving orders", e, "SERVER_ERROR");
        }
    }

    @Override
    public OrderDto edit(Long id) {

        try {
            OrderEntity orderEntity = getOrderEntityById(id);
            return orderEntityToOrderDtoConverter.convert(orderEntity);
        } catch (Exception e) {
            log.error("An error occurred while editing an order.", e);
            throw new SomethingGoodToEatException("Error while editing an order", e, "SERVER_ERROR");
        }
    }

    @Override
    @Transactional
    public void deleteOrder(Long id) {

        try {
            OrderEntity orderEntity = getOrderEntityById(id);
            List<OrderDetailsEntity> orderDetailsEntities = orderEntity.getOrderDetailEntities();
            for (OrderDetailsEntity orderDetailsEntity : orderDetailsEntities) {
                orderDetailsRepository.delete(orderDetailsEntity);
                log.debug("Deleted OrderDetail with Id:" + orderDetailsEntity.getId());
            }

            orderRepository.delete(orderEntity);
            log.debug("Deleted Order with Id:" + id);
        } catch (Exception e) {
            log.error("An error occurred while deleting an order.", e);
            throw new SomethingGoodToEatException("Error while deleting an order.", e, "SERVER_ERROR");
        }
    }

    public OrderDto getOrderById(Long id) {

        OrderEntity orderEntity = getOrderEntityById(id);

        return orderEntityToOrderDtoConverter.convert(orderEntity);
    }

    public OrderDto updateOrder(Long id, OrderDto orderDto) {

        try {
            validate(orderDto);
            OrderEntity orderEntity = orderRepository.findById(id).orElseThrow(()
                    -> new SomethingGoodToEatException("Not found order details with id:" + id, "SERVER_ERROR"));

            OrderDto convertedOrderDto = orderEntityToOrderDtoConverter.convert(orderEntity);
            if (convertedOrderDto != null) {
                convertedOrderDto.setId(orderDto.getId());
                convertedOrderDto.setName(orderDto.getName());
                convertedOrderDto.setPaymentType(orderDto.getPaymentType());
                convertedOrderDto.setOrderStatus(orderDto.getOrderStatus());
                convertedOrderDto.setTotalAmount(orderDto.getTotalAmount());
                convertedOrderDto.setReceivedAmount(orderDto.getReceivedAmount());

                if (orderDto.getTable() != null) {
                    TableEntity tableEntity = tableRepository.findById(orderDto.getTableId())
                            .orElseThrow(() -> new SomethingGoodToEatException("Not found table with id: " + orderDto.getTableId(), "CLIENT_ERROR"));
                    orderEntity.setEatingTable(tableEntity);
                }
            }

            if (orderDto.getOrderDetails() != null && !orderDto.getOrderDetails().isEmpty()) {

                List<OrderDetailsEntity> updatedOrderDetailsEntities = new ArrayList<>();
                for (OrderDetailsDto orderDetailsDto : orderDto.getOrderDetails()) {
                    OrderDetailsEntity orderDetailsEntity = orderDetailsRepository.findById(orderDetailsDto.getId())
                            .orElseThrow(() -> new SomethingGoodToEatException("Not found order details with id: " + orderDetailsDto.getId(), "CLIENT_ERROR"));

                    orderDetailsEntity.setId(orderDetailsDto.getId());
                    orderDetailsEntity.setUnitOfMeasure(orderDetailsDto.getUnitOfMeasure());
                    orderDetailsEntity.setAmount(orderDetailsDto.getAmount());
                    orderDetailsEntity.setOrderDetailsStatus(orderDetailsDto.getOrderDetailsStatus());
                    orderDetailsEntity.setMenu(menuDtoToMenuEntityConverter.convert(orderDetailsDto.getMenu()));

                    updatedOrderDetailsEntities.add(orderDetailsEntity);
                }
                orderEntity.setOrderDetailEntities(updatedOrderDetailsEntities);
            }
            orderEntity.setOrderStatus(convertedOrderDto.getOrderStatus());
            orderEntity.setReceivedAmount(convertedOrderDto.getReceivedAmount());

            if ((orderDto.getReceivedAmount() != null) && (orderDto.getOrderStatus().equals(OrderStatus.DELIVERED))) {
                orderEntity.setOrderStatus(OrderStatus.PAID);
            }
            OrderEntity savedOrderEntity = orderRepository.save(orderEntity);
            OrderDto convertedOrder = orderEntityToOrderDtoConverter.convert(savedOrderEntity);

            return convertedOrder;
        } catch (Exception e) {
            log.error("An error occurred while updating an order.", e);
            throw new SomethingGoodToEatException("Error while updating an order.", e, "SERVER_ERROR");
        }
    }

    private OrderEntity getOrderEntityById(Long id) {
        return orderRepository.findById(id).orElseThrow(()
                -> new SomethingGoodToEatException("Not found order with id:" + id, "SERVER_ERROR"));
    }

    private void validate(OrderDto orderDto) {
        Set<ConstraintViolation<OrderDto>> violations = validator.validate(orderDto);
        if (!violations.isEmpty()) {
            log.debug("Validation for OrderDto failed");
            throw new ValidationException("DTO validation failed", (Throwable) violations);
        }
    }

    public List<OrderDto> getOrdersBetweenDates() {

        List<OrderEntity> retrievedPaidOrders = orderRepository.findByOrderStatus(OrderStatus.PAID);

        return orderDtoToOrderEntityConverter.toDTOList(retrievedPaidOrders);
    }

    public void setTotalAmountToOrderRepository(OrderDto order) {

        OrderEntity orderEntity = orderRepository.findById(order.getId()).orElseThrow(()
                -> new SomethingGoodToEatException("Not found order with id:" + order.getId(), "SERVER_ERROR"));
        orderEntity.setPriceOfOrder(order.getTotalAmount());

        orderRepository.save(orderEntity);
    }

    public void setBackGroundColor(Model model, List<OrderDetailsDto> orderDetails,
                                   List<OrderDetailsDto> orderDetailsOnOrder, Long orderId) {
        for (OrderDetailsDto orderDetail : orderDetails) {
            if (orderDetail.getOrderId().equals(orderId)) {
                String statusColor = getStatusColor(String.valueOf(orderDetail.getOrderDetailsStatus()));
                orderDetail.setColorStatus(statusColor);
                model.addAttribute("backgroundColor", orderDetail.getColorStatus());
                orderDetailsOnOrder.add(orderDetail);
            }
        }
    }

    public String getStatusColor(String orderStatus) {
        switch (orderStatus) {
            case "PENDING":
            case "COOKING":
                return "yellow";
            case "READY":
            case "AVAILABLE":
                return "green";
            case "PAID":
            case "BOOKED":
                return "blue";
            case "DELIVERED":
            case "BUSY":
                return "red";
            default:
                return "inherit";
        }
    }

    public Double calculateTotalAmount(OrderDto orderDto) {
        List<OrderDetailsDto> orderDetailsList = orderDto.getOrderDetails();
        Double totalAmount = 0.0;

        if (orderDetailsList != null) {
            for (OrderDetailsDto orderDetails : orderDetailsList) {
                Double amount = orderDetails.getAmount();
                Double price = orderDetails.getMenu().getPrice();
                totalAmount += (amount * price);
            }
        }
        return totalAmount;
    }

    public Double calculateTotalAmountWithVat(Double totalAmount, Double vatAmount) {

        return totalAmount + vatAmount;
    }

    public Double calculateAmountWithVat(Double totalAmount) {

        return (0.19 * totalAmount);
    }

    public Double calculateAmountDue(Double totalAmount, Double receivedAmount) {

        return receivedAmount - totalAmount;
    }

    public Double calculateExpectedTip(Double totalAmount) {

        Double tip = (0.1 * totalAmount);
        return tip;
    }

    public void changeOrderStatus(Model model, Page<OrderDto> orderPage) {

        List<OrderDto> orders = orderPage.getContent();
        for (OrderDto order : orders) {
            boolean allReady = true;
            boolean anyCooking = false;

            for (OrderDetailsDto orderDetails : order.getOrderDetails()) {
                if (orderDetails.getOrderDetailsStatus() != OrderDetailsStatus.READY) {
                    allReady = false;
                    if (orderDetails.getOrderDetailsStatus() == OrderDetailsStatus.COOKING) {
                        anyCooking = true;
                        break;
                    }
                }
            }

            if ((order.getOrderStatus().equals(OrderStatus.DELIVERED) && (allReady))) {
                order.setOrderStatus(OrderStatus.DELIVERED);
                OrderEntity orderEntity = orderRepository.findById(order.getId()).orElseThrow(()
                        -> new SomethingGoodToEatException("Not found order with id:" + order.getId(), "SERVER_ERROR"));
                orderEntity.setOrderStatus(OrderStatus.DELIVERED);
                orderRepository.updateOrderStatusById(order.getId(), OrderStatus.DELIVERED);
            } else if ((order.getOrderStatus().equals(OrderStatus.READY) && (allReady))) {
                order.setOrderStatus(OrderStatus.READY);
                OrderEntity orderEntity = orderRepository.findById(order.getId()).orElseThrow(()
                        -> new SomethingGoodToEatException("Not found order with id:" + order.getId(), "SERVER_ERROR"));
                orderEntity.setOrderStatus(OrderStatus.READY);
                orderRepository.updateOrderStatusById(order.getId(), OrderStatus.READY);
            } else if (anyCooking) {
                order.setOrderStatus(OrderStatus.PENDING);
                OrderEntity orderEntity = orderRepository.findById(order.getId()).orElseThrow(()
                        -> new SomethingGoodToEatException("Not found order with id:" + order.getId(), "SERVER_ERROR"));
                orderEntity.setOrderStatus(OrderStatus.PENDING);
                orderRepository.updateOrderStatusById(order.getId(), OrderStatus.PENDING);
            }

            String statusColor = getStatusColor(String.valueOf
                    (order.getOrderStatus()));
            order.setColorStatus(statusColor);

            model.addAttribute("backgroundColor", order.getColorStatus());
        }
    }

    public List<OrderDetailsDto> getOrderDetailsDtos(Long orderId, OrderDto order) {
        List<OrderDetailsDto> orderDetails = orderDetailsServiceImpl.getOrderDetailsByOrderId(orderId);
        for (OrderDetailsDto orderDetail : orderDetails) {
            if (OrderDetailsStatus.COOKING.equals(orderDetail.getOrderDetailsStatus())) {
                orderDetail.setBackgroundColor("yellow");
            } else {
                orderDetail.setBackgroundColor("green");
                order.setOrderStatus(OrderStatus.READY);
            }
        }
        return orderDetails;
    }
}
