package ro.somethinggoodtoeat.restaurantmanagementsystem.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ro.somethinggoodtoeat.restaurantmanagementsystem.converter.UserDtoToUserEntityConverter;
import ro.somethinggoodtoeat.restaurantmanagementsystem.converter.UserEntityToUserDtoConverter;
import ro.somethinggoodtoeat.restaurantmanagementsystem.domain.PermissionEntity;
import ro.somethinggoodtoeat.restaurantmanagementsystem.domain.UserEntity;
import ro.somethinggoodtoeat.restaurantmanagementsystem.domain.UserPermissionEntity;
import ro.somethinggoodtoeat.restaurantmanagementsystem.dto.securityDto.UserDetails;
import ro.somethinggoodtoeat.restaurantmanagementsystem.exception.SomethingGoodToEatException;
import ro.somethinggoodtoeat.restaurantmanagementsystem.repository.security.PermissionRepository;
import ro.somethinggoodtoeat.restaurantmanagementsystem.repository.security.PermissionUserRepository;
import ro.somethinggoodtoeat.restaurantmanagementsystem.repository.security.UserRepository;
import ro.somethinggoodtoeat.restaurantmanagementsystem.service.interfaces.LoginService;
import javax.transaction.Transactional;
import javax.validation.ConstraintViolation;
import javax.validation.ValidationException;
import javax.validation.Validator;
import java.util.HashSet;
import java.util.Set;

@Slf4j
@Service
public class LoginServiceImpl implements LoginService {

    private final UserRepository userRepository;

    private final UserDtoToUserEntityConverter userDtoToUserEntityConverter;
    private final UserEntityToUserDtoConverter userEntityToUserDtoConverter;

    private final PermissionRepository permissionRepository;

    private final PermissionUserRepository permissionUserRepository;

    private final Validator validator;

    public LoginServiceImpl(UserRepository userRepository, UserDtoToUserEntityConverter userDtoToUserEntityConverter, UserEntityToUserDtoConverter userEntityToUserDtoConverter, Validator validator, PermissionRepository permissionRepository, PermissionUserRepository permissionUserRepository) {
        this.userRepository = userRepository;
        this.userDtoToUserEntityConverter = userDtoToUserEntityConverter;
        this.userEntityToUserDtoConverter = userEntityToUserDtoConverter;
        this.validator = validator;
        this.permissionRepository = permissionRepository;
        this.permissionUserRepository = permissionUserRepository;
    }

    @Override
    @Transactional
    public void signUpUser(UserDetails userDetails) {

        try {
            validate(userDetails);
            UserEntity createdUserEntity = userDtoToUserEntityConverter.convert(userDetails);
            assert createdUserEntity != null;
            addUserPermissions(createdUserEntity);
            UserEntity savedUserEntity = userRepository.save(createdUserEntity);
            log.debug("Saved User with Id:" + savedUserEntity.getId());
        } catch (Exception e) {
            log.error("An error occurred while creating an user.", e);
            throw new SomethingGoodToEatException("Error while creating an user", e, "SERVER_ERROR");
        }
    }

    public void addUserPermissions(UserEntity savedUserEntity) {
        Set<UserPermissionEntity> convertedAuthorities = new HashSet<>();
        Set<UserPermissionEntity> originalAuthorities = new HashSet<>();

        if (savedUserEntity.getPermissions() != null) {
            originalAuthorities = savedUserEntity.getPermissions();
        } else {

            PermissionEntity permission = new PermissionEntity();
            permission.setRole(String.valueOf(savedUserEntity.getRole()));
            permissionRepository.save(permission);

            UserPermissionEntity userPermissionEntity = new UserPermissionEntity();
            userPermissionEntity.setPermission(permission);
            userPermissionEntity.setUser(savedUserEntity);
            originalAuthorities.add(userPermissionEntity);
        }
        for (UserPermissionEntity authority : originalAuthorities) {
            if (!convertedAuthorities.contains(authority)) {
                convertedAuthorities.add(authority);
                permissionUserRepository.save(authority);
                log.debug("Saved UserPermission with Id:" + authority.getId());

            }
        }
        savedUserEntity.setPermissions(convertedAuthorities);
    }

    private void validate(UserDetails user) {
        Set<ConstraintViolation<UserDetails>> violations = validator.validate(user);
        if (!violations.isEmpty()) {
            log.debug("Validation for UserDto failed");
            throw new ValidationException("DTO validation failed", (Throwable) violations);
        }
    }
}
