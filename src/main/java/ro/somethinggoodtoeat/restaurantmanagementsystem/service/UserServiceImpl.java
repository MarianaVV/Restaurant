package ro.somethinggoodtoeat.restaurantmanagementsystem.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import ro.somethinggoodtoeat.restaurantmanagementsystem.converter.UserDtoToUserEntityConverter;
import ro.somethinggoodtoeat.restaurantmanagementsystem.converter.UserEntityToUserDtoConverter;
import ro.somethinggoodtoeat.restaurantmanagementsystem.domain.PermissionEntity;
import ro.somethinggoodtoeat.restaurantmanagementsystem.domain.UserEntity;
import ro.somethinggoodtoeat.restaurantmanagementsystem.domain.UserPermissionEntity;
import ro.somethinggoodtoeat.restaurantmanagementsystem.dto.securityDto.UserDetails;
import ro.somethinggoodtoeat.restaurantmanagementsystem.exception.SomethingGoodToEatException;
import ro.somethinggoodtoeat.restaurantmanagementsystem.repository.security.PermissionRepository;
import ro.somethinggoodtoeat.restaurantmanagementsystem.repository.security.PermissionUserRepository;
import ro.somethinggoodtoeat.restaurantmanagementsystem.repository.security.UserRepository;
import ro.somethinggoodtoeat.restaurantmanagementsystem.service.interfaces.UserService;

import javax.transaction.Transactional;
import javax.validation.ConstraintViolation;
import javax.validation.Valid;
import javax.validation.ValidationException;
import javax.validation.Validator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Slf4j
@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final UserDtoToUserEntityConverter userDtoToUserEntityConverter;
    private final UserEntityToUserDtoConverter userEntityToUserDtoConverter;
    private final PermissionRepository permissionRepository;
    private final PermissionUserRepository permissionUserRepository;
    private final Validator validator;

    public UserServiceImpl(UserRepository userRepository,
                           UserDtoToUserEntityConverter userDtoToUserEntityConverter,
                           UserEntityToUserDtoConverter userEntityToUserDtoConverter,
                           Validator validator,
                           PermissionRepository permissionRepository,
                           PermissionUserRepository permissionUserRepository) {
        this.userRepository = userRepository;
        this.userDtoToUserEntityConverter = userDtoToUserEntityConverter;
        this.userEntityToUserDtoConverter = userEntityToUserDtoConverter;
        this.validator = validator;
        this.permissionRepository = permissionRepository;
        this.permissionUserRepository = permissionUserRepository;
    }

    public Page<UserDetails> getAllUsers(int page, int size) {
        try {
            Pageable pageable = PageRequest.of(page, size);
            Page<UserEntity> userPage = userRepository.findAll(pageable);
            List<UserDetails> userDetailsList = userEntityToUserDtoConverter.toDTOList(userPage.getContent());

            return new PageImpl<>(userDetailsList, pageable, userPage.getTotalElements());
        } catch (Exception e) {
            log.error("An error occurred while retrieving users.", e);
            throw new SomethingGoodToEatException("Error while retrieving users", e, "SERVER_ERROR");
        }
    }

    @Override
    public List<UserDetails> getAllUsers() {
        return null;
    }

    @Override
    @Transactional
    public UserDetails createUser(UserDetails userDetails) {

        try {
            validate(userDetails);
            UserEntity createdUserEntity = userDtoToUserEntityConverter.convert(userDetails);
            assert createdUserEntity != null;
            UserEntity savedUserEntity = userRepository.save(createdUserEntity);
            addUserPermissions(savedUserEntity);
            log.debug("Saved User with Id:" + savedUserEntity.getId());

            return userEntityToUserDtoConverter.convert(savedUserEntity);
        } catch (Exception e) {
            log.error("An error occurred while creating an user.", e);
            throw new SomethingGoodToEatException("Error while creating an user", e, "SERVER_ERROR");
        }
    }

    @Override
    @Transactional
    public UserDetails updateUser(Long id, @Valid UserDetails updatedUser) {

        try {
            validate(updatedUser);
            UserEntity userEntity = getUserEntityById(id);

            UserDetails convertedUser = userEntityToUserDtoConverter.convert(userEntity);
            if (convertedUser != null) {
                convertedUser.setId(updatedUser.getId());
                convertedUser.setFirstName(updatedUser.getFirstName());
                convertedUser.setLastName(updatedUser.getLastName());
                convertedUser.setEmailAddress(updatedUser.getEmailAddress());
                convertedUser.setRole(updatedUser.getRole());
                convertedUser.setUsername(updatedUser.getUsername());
            }

            UserEntity updatedNewUserEntity = userDtoToUserEntityConverter.convert(convertedUser);
            if (updatedNewUserEntity != null) {
                userRepository.save(updatedNewUserEntity);
            }
            return convertedUser;
        } catch (Exception e) {
            log.error("An error occurred while updating an user.", e);
            throw new SomethingGoodToEatException("Error while updating an user", e, "SERVER_ERROR");
        }
    }

    @Override
    @Transactional
    public void deleteUser(Long id) {
        try {
            UserEntity deletedMenu = getUserEntityById(id);
            userRepository.delete(deletedMenu);
            log.debug("Deleted Category with Id:" + id);
        } catch (Exception e) {
            log.error("An error occurred while deleting an user.", e);
            throw new SomethingGoodToEatException("Error while deleting an user", e, "SERVER_ERROR");
        }
    }

    public UserDetails edit(Long id) {

        try {
            UserEntity userEntity = getUserEntityById(id);
            return userEntityToUserDtoConverter.convert(userEntity);
        } catch (Exception e) {
            log.error("An error occurred while editing an user.", e);
            throw new SomethingGoodToEatException("Error while editing an user", e, "SERVER_ERROR");
        }
    }

    private UserEntity getUserEntityById(Long id) {
        return userRepository.findById(id).orElseThrow(()
                -> new SomethingGoodToEatException("Not found user with id:" + id, "CLIENT_ERROR"));
    }

    public void addUserPermissions(UserEntity savedUserEntity) {
        Set<UserPermissionEntity> convertedAuthorities = new HashSet<>();
        Set<UserPermissionEntity> originalAuthorities = new HashSet<>();

        if (savedUserEntity.getPermissions() != null) {
            originalAuthorities = savedUserEntity.getPermissions();
        } else {

            PermissionEntity permission = new PermissionEntity();
            permission.setRole(String.valueOf(savedUserEntity.getRole()));
            permissionRepository.save(permission);

            UserPermissionEntity userPermissionEntity = new UserPermissionEntity();
            userPermissionEntity.setPermission(permission);
            userPermissionEntity.setUser(savedUserEntity);
            originalAuthorities.add(userPermissionEntity);
        }
        for (UserPermissionEntity authority : originalAuthorities) {
            if (!convertedAuthorities.contains(authority)) {
                convertedAuthorities.add(authority);
                permissionUserRepository.save(authority);
                log.debug("Saved UserPermission with Id:" + authority.getId());

            }
        }
        savedUserEntity.setPermissions(convertedAuthorities);
    }

    private void validate(UserDetails user) {
        Set<ConstraintViolation<UserDetails>> violations = validator.validate(user);
        if (!violations.isEmpty()) {
            log.debug("Validation for UserDto failed");
            throw new ValidationException("DTO validation failed", (Throwable) violations);
        }
    }
}
