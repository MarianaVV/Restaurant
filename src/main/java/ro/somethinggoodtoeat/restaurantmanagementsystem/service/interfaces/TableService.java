package ro.somethinggoodtoeat.restaurantmanagementsystem.service.interfaces;

import ro.somethinggoodtoeat.restaurantmanagementsystem.dto.TableDto;
import ro.somethinggoodtoeat.restaurantmanagementsystem.enums.TableStatus;

import java.util.List;

public interface TableService {

    TableDto createTable(TableDto tableDto);

    TableDto edit(Long id);

    TableDto updateTable(Long id, TableDto tableDto);

    void deleteTable(Long id);

    List<TableDto> getAllTables();

    public List<TableDto> getAllTablesWithStatusAvailable(TableStatus status);
}
