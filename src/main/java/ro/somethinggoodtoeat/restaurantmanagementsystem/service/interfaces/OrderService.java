package ro.somethinggoodtoeat.restaurantmanagementsystem.service.interfaces;

import ro.somethinggoodtoeat.restaurantmanagementsystem.dto.OrderDto;
import java.util.List;

public interface OrderService {

    List<OrderDto> getAllOrders();

    public OrderDto edit(Long id);

    public OrderDto updateOrder(Long id, OrderDto orderDto);

    public void deleteOrder(Long id);
}
