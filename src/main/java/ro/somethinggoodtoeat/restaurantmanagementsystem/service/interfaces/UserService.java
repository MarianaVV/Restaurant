package ro.somethinggoodtoeat.restaurantmanagementsystem.service.interfaces;

import ro.somethinggoodtoeat.restaurantmanagementsystem.dto.securityDto.UserDetails;
import java.util.List;

public interface UserService {

    List<UserDetails> getAllUsers();

    UserDetails createUser(UserDetails userDetails);

    UserDetails updateUser(Long id, UserDetails updatedUser);

    void deleteUser(Long id);

    public UserDetails edit(Long id);
}
