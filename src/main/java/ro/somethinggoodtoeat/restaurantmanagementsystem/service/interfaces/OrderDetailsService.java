package ro.somethinggoodtoeat.restaurantmanagementsystem.service.interfaces;

import ro.somethinggoodtoeat.restaurantmanagementsystem.dto.OrderDetailsDto;

import java.util.List;

public interface OrderDetailsService {

    List<OrderDetailsDto> getAllOrderDetails();

    OrderDetailsDto createOrderDetails(OrderDetailsDto orderDetailsDto);
}
