package ro.somethinggoodtoeat.restaurantmanagementsystem.service.interfaces;

import ro.somethinggoodtoeat.restaurantmanagementsystem.dto.securityDto.UserDetails;

public interface LoginService {

    void signUpUser(UserDetails userDetails);
}
