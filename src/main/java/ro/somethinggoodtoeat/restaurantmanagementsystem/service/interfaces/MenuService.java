package ro.somethinggoodtoeat.restaurantmanagementsystem.service.interfaces;

import org.springframework.data.domain.Page;
import ro.somethinggoodtoeat.restaurantmanagementsystem.dto.MenuDto;

import java.util.List;

public interface MenuService {

    List<MenuDto> getAllMenus();

    MenuDto createMenu(MenuDto menuDto);

    MenuDto edit(Long id);

    MenuDto updateMenu(Long id, MenuDto menuDto);

    void deleteMenu(Long id);

    MenuDto getMenuById(Long menuItemId);

    List<MenuDto> getAllMenusByIsPopular();

    Page<MenuDto> getAllMenus(int page, int size);
}
