package ro.somethinggoodtoeat.restaurantmanagementsystem.service.interfaces;

import org.springframework.data.domain.Page;
import ro.somethinggoodtoeat.restaurantmanagementsystem.dto.CategoryDto;

import java.util.List;

public interface CategoryService {

    List<CategoryDto> getAllCategories();

    public Page<CategoryDto> getAllCategories(int pageNumber, int pageSize);

    CategoryDto createCategory(CategoryDto category);

    CategoryDto updateCategory(Long id, CategoryDto updatedCategory);

    void deleteCategory(Long id);

    public CategoryDto edit(Long id);
}
