package ro.somethinggoodtoeat.restaurantmanagementsystem.exception;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@Slf4j
@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(SomethingGoodToEatException.class)
    public String handleSomethingGoodToEatException(SomethingGoodToEatException e) {

        log.error("An error occurred", e);
        return "redirect:/api/v1/error";
    }
}
