package ro.somethinggoodtoeat.restaurantmanagementsystem.exception;

public class SomethingGoodToEatException extends RuntimeException {

    private final String errorCode;

    public SomethingGoodToEatException(String message, String errorCode) {
        super(message);
        this.errorCode = errorCode;
    }

    public SomethingGoodToEatException(String message, Throwable cause, String errorCode) {
        super(message, cause);
        this.errorCode = errorCode;
    }
}
