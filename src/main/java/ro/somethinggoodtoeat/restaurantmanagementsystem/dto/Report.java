package ro.somethinggoodtoeat.restaurantmanagementsystem.dto;

import lombok.*;

@Data
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Report {

    private String startDate;
    private String endDate;
}
