package ro.somethinggoodtoeat.restaurantmanagementsystem.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ro.somethinggoodtoeat.restaurantmanagementsystem.enums.TableStatus;

import javax.validation.constraints.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TableDto {

    private Long id;

    @NotBlank(message = "Name is required")
    @Size(max = 50, message = "Name cannot exceed 50 characters")
    @Pattern(regexp = "^Table \\d{3}$", message = "Name must be in the format 'Table 001'")
    private String name;

    @NotNull(message = "Number of seats is required")
    @Max(value = 10, message = "Number of seats cannot exceed 10")
    private Integer numberOfSeats;

    private TableStatus status;

    private String backGroundColor;

    private String statusColor;
}
