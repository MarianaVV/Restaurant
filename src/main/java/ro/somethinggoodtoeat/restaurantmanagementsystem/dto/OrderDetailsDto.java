package ro.somethinggoodtoeat.restaurantmanagementsystem.dto;

import lombok.*;
import ro.somethinggoodtoeat.restaurantmanagementsystem.enums.OrderDetailsStatus;
import ro.somethinggoodtoeat.restaurantmanagementsystem.enums.UnitOfMeasure;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class OrderDetailsDto {

    private Long id;
    private Long menuItemId;
    private Long orderId;
    private MenuDto menu;
    private Double amount;
    private Double price;
    private UnitOfMeasure unitOfMeasure;
    private OrderDetailsStatus orderDetailsStatus;
    private String colorStatus;

    private String backgroundColor;
}
