package ro.somethinggoodtoeat.restaurantmanagementsystem.dto;

import lombok.*;
import ro.somethinggoodtoeat.restaurantmanagementsystem.enums.PaymentType;
import ro.somethinggoodtoeat.restaurantmanagementsystem.enums.OrderStatus;

import javax.validation.constraints.NotBlank;
import java.time.LocalDateTime;
import java.util.List;

@Data
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class OrderDto {

    private Long id;
    @NotBlank(message = "Name is required")
    private String name;
    private PaymentType paymentType;
    private Long userId;
    private Long tableId;
    private List<OrderDetailsDto> orderDetails;
    private Double totalAmount;
    private Double receivedAmount;
    private Double amountDue;
    private Double tip;
    private Double totalAmountWithVat;
    private Double vatAmount;
    private OrderStatus orderStatus;
    private TableDto table;
    private String colorStatus;
    private LocalDateTime startTime;
}
