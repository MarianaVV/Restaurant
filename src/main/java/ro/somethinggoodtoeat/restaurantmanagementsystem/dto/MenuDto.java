package ro.somethinggoodtoeat.restaurantmanagementsystem.dto;

import lombok.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.*;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MenuDto {

    private Long id;

    private String idString;

    @NotBlank(message = "Name is required")
    @Size(max = 50, message = "Name cannot exceed 50 characters")
    private String name;

    @NotNull(message = "Price is required")
    @DecimalMin(value = "0.0", inclusive = false, message = "Price must be greater than 0")
    private Double price;

    private MultipartFile image;
    @NotBlank(message = "Description is required")
    private String description;
    private String imageContent;
    private CategoryDto category;
    private String isPopular;

    public MenuDto(String name, String isPopular) {
        this.name = name;
        this.isPopular = isPopular;
    }
}
