package ro.somethinggoodtoeat.restaurantmanagementsystem.dto.securityDto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UserDetails {

    private Long id;

    @NotBlank(message = "Name is required")
    @Size(max = 50, message = "Name cannot exceed 50 characters")
    private String lastName;
    @NotBlank(message = "Name is required")
    @Size(max = 50, message = "Name cannot exceed 50 characters")
    private String firstName;
    private String role;
    @NotBlank(message = "Email address is required")
    private String emailAddress;

    @NotBlank(message = "Username is required")
    private String username;
    private String password;
    private Set<PermissionDto> permissions;

    @Builder.Default
    private Boolean accountNotExpired = true;
    @Builder.Default
    private Boolean accountNotLocked = true;
    @Builder.Default
    private Boolean credentialsNotExpired = true;
    @Builder.Default
    private Boolean enabled = true;
}
