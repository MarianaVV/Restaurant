package ro.somethinggoodtoeat.restaurantmanagementsystem.security;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import ro.somethinggoodtoeat.restaurantmanagementsystem.domain.UserEntity;
import ro.somethinggoodtoeat.restaurantmanagementsystem.domain.UserPermissionEntity;
import ro.somethinggoodtoeat.restaurantmanagementsystem.repository.security.UserRepository;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import javax.transaction.Transactional;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
@Slf4j
public class LoginUserService implements UserDetailsService {

    private final UserRepository userRepository;

    @Transactional
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        log.debug("Getting user information.");
        UserEntity userEntity = userRepository.findByUsername(username).orElseThrow(() ->
        {
            return new UsernameNotFoundException("User name: " + username + "not found");
        });

        User user = new User(userEntity.getUsername(), userEntity.getPassword(),
                userEntity.getEnabled(), userEntity.getAccountNotExpired(), userEntity.getCredentialsNotExpired(),
                userEntity.getAccountNotLocked(), convertToSpringAuthorities(userEntity.getPermissions()));
        return user;
    }

    private Collection<? extends GrantedAuthority> convertToSpringAuthorities(Set<UserPermissionEntity> authorities) {
        if (authorities != null && authorities.size() > 0) {
            return authorities.stream()
                    .map(userPermission -> userPermission.getPermission().getRole())
                    .map(SimpleGrantedAuthority::new)
                    .collect(Collectors.toSet());
        } else {
            return new HashSet<>();
        }
    }
}
