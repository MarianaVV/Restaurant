package ro.somethinggoodtoeat.restaurantmanagementsystem.converter;

import lombok.Synchronized;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import ro.somethinggoodtoeat.restaurantmanagementsystem.dto.securityDto.UserDetails;
import ro.somethinggoodtoeat.restaurantmanagementsystem.dto.User;

@Component
public class UserToUserDtoConverter implements Converter<User, UserDetails> {

    @Override
    @Synchronized
    public UserDetails convert(User source) {

        if (source == null) {
            return null;
        }
        final UserDetails userDetails = new UserDetails();
        userDetails.setUsername(source.getUsername());
        userDetails.setPassword(source.getPassword());

        return userDetails;
    }
}
