package ro.somethinggoodtoeat.restaurantmanagementsystem.converter;

import lombok.Synchronized;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import ro.somethinggoodtoeat.restaurantmanagementsystem.domain.PermissionEntity;
import ro.somethinggoodtoeat.restaurantmanagementsystem.dto.securityDto.PermissionDto;
import ro.somethinggoodtoeat.restaurantmanagementsystem.repository.security.PermissionRepository;

@Component
public class PermissionDtoToPermissionEntityConverter implements Converter<PermissionDto, PermissionEntity> {

    private final PermissionRepository permissionRepository;

    public PermissionDtoToPermissionEntityConverter(PermissionRepository permissionRepository) {
        this.permissionRepository = permissionRepository;
    }

    @Synchronized
    public PermissionEntity convertAndSave(PermissionDto source) {

        if (source == null || permissionRepository == null) {
            return null;
        }

        final PermissionEntity permissionEntity = new PermissionEntity();
        permissionEntity.setId(source.getId());
        permissionEntity.setRole(source.getRole());

        return permissionRepository.save(permissionEntity);
    }

    @Override
    public PermissionEntity convert(PermissionDto source) {
        return null;
    }
}
