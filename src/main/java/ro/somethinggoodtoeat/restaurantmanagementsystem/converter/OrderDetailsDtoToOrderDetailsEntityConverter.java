package ro.somethinggoodtoeat.restaurantmanagementsystem.converter;

import lombok.Synchronized;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import ro.somethinggoodtoeat.restaurantmanagementsystem.domain.MenuEntity;
import ro.somethinggoodtoeat.restaurantmanagementsystem.domain.OrderDetailsEntity;
import ro.somethinggoodtoeat.restaurantmanagementsystem.domain.OrderEntity;
import ro.somethinggoodtoeat.restaurantmanagementsystem.dto.OrderDetailsDto;
import ro.somethinggoodtoeat.restaurantmanagementsystem.exception.SomethingGoodToEatException;
import ro.somethinggoodtoeat.restaurantmanagementsystem.repository.MenuRepository;
import ro.somethinggoodtoeat.restaurantmanagementsystem.repository.OrderRepository;

@Component
public class OrderDetailsDtoToOrderDetailsEntityConverter implements Converter<OrderDetailsDto, OrderDetailsEntity> {

    private final MenuDtoToMenuEntityConverter converter;
    private final MenuRepository menuRepository;
    private final OrderRepository orderRepository;

    public OrderDetailsDtoToOrderDetailsEntityConverter(MenuDtoToMenuEntityConverter converter,
                                                        MenuRepository menuRepository,
                                                        OrderRepository orderRepository) {
        this.converter = converter;
        this.menuRepository = menuRepository;
        this.orderRepository = orderRepository;
    }

    @Synchronized
    @Override
    public OrderDetailsEntity convert(OrderDetailsDto source) {

        if (source == null) {
            return null;
        }

        final OrderDetailsEntity orderDetailsEntity = new OrderDetailsEntity();

        if (source.getId() != null) {
            orderDetailsEntity.setId(source.getId());
        }
        final OrderEntity orderEntity = orderRepository.findById(source.getOrderId()).orElseThrow(()
                -> new SomethingGoodToEatException("Not found order with id:" + source.getOrderId(), "CLIENT_ERROR"));
        orderDetailsEntity.setOrder(orderEntity);
        orderDetailsEntity.setAmount(source.getAmount());
        orderDetailsEntity.setUnitOfMeasure(source.getUnitOfMeasure());
        orderDetailsEntity.setOrderDetailsStatus(source.getOrderDetailsStatus());
        if (source.getMenuItemId() != null) {
            MenuEntity menuEntity = menuRepository.findById(source.getMenuItemId()).orElseThrow(()
                    -> new SomethingGoodToEatException("Not found category with id:" + source.getMenuItemId(), "CLIENT_ERROR"));
            orderDetailsEntity.setMenu(menuEntity);
        } else {
            MenuEntity menuEntity = menuRepository.findById(source.getMenu().getId()).orElseThrow(()
                    -> new SomethingGoodToEatException("Not found menu with id:" + source.getMenuItemId(), "CLIENT_ERROR"));
            orderDetailsEntity.setMenu(menuEntity);
        }
        return orderDetailsEntity;
    }
}
