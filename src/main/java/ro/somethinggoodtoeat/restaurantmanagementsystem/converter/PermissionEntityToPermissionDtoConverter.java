package ro.somethinggoodtoeat.restaurantmanagementsystem.converter;

import org.springframework.core.convert.converter.Converter;
import ro.somethinggoodtoeat.restaurantmanagementsystem.domain.PermissionEntity;
import ro.somethinggoodtoeat.restaurantmanagementsystem.dto.securityDto.PermissionDto;

public class PermissionEntityToPermissionDtoConverter implements Converter<PermissionEntity, PermissionDto> {


    @Override
    public PermissionDto convert(PermissionEntity source) {
        if (source == null) {
            return null;
        }

        final PermissionDto permissionDto = new PermissionDto();
        permissionDto.setId(source.getId());
        permissionDto.setRole(source.getRole());

        return permissionDto;
    }
}
