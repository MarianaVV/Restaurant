package ro.somethinggoodtoeat.restaurantmanagementsystem.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;
import ro.somethinggoodtoeat.restaurantmanagementsystem.domain.MenuEntity;
import ro.somethinggoodtoeat.restaurantmanagementsystem.dto.MenuDto;

import java.util.Base64;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class MenuEntityToMenuDtoConverter implements Converter<MenuEntity, MenuDto> {

    private final CategoryEntityToCategoryDtoConverter converter;

    public MenuEntityToMenuDtoConverter(CategoryEntityToCategoryDtoConverter converter) {
        this.converter = converter;
    }


    @Override
    public MenuDto convert(MenuEntity source) {

        if (source == null) {
            return null;
        }

        final MenuDto menuDto = new MenuDto();
        menuDto.setId(source.getId());
        menuDto.setName(source.getName());
        menuDto.setPrice(source.getPrice());
        menuDto.setImageContent(convertImageToBase64(source.getImage()));
        menuDto.setDescription(source.getDescription());
        menuDto.setIsPopular(source.getIsPopular());
        if (source.getCategory() != null) {
            menuDto.setCategory(converter.convert(source.getCategory()));
        }
        return menuDto;
    }

    private String convertImageToBase64(byte[] image) {
        if (image != null && image.length > 0) {
            return Base64.getEncoder().encodeToString(image);
        }
        return null;
    }

    public List<MenuDto> toDTOList(List<MenuEntity> menuEntities) {
        return menuEntities.stream()
                .map(this::convert)
                .collect(Collectors.toList());
    }

    public List<MenuDto> toDTOList(Page<MenuEntity> menuPage) {
        List<MenuEntity> menuEntities = menuPage.getContent();
        return toDTOList(menuEntities);
    }
}
