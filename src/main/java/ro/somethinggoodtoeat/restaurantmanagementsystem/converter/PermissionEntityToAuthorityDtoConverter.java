package ro.somethinggoodtoeat.restaurantmanagementsystem.converter;

import lombok.Synchronized;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import ro.somethinggoodtoeat.restaurantmanagementsystem.domain.PermissionEntity;
import ro.somethinggoodtoeat.restaurantmanagementsystem.dto.securityDto.PermissionDto;

@Component
public class PermissionEntityToAuthorityDtoConverter implements Converter<PermissionEntity, PermissionDto> {

    @Synchronized
    @Override
    public PermissionDto convert(PermissionEntity source) {

        if (source == null) {
            return null;
        }

        final PermissionDto permissionDto = new PermissionDto();
        permissionDto.setId(source.getId());
        permissionDto.setRole(source.getRole());

        return permissionDto;
    }
}
