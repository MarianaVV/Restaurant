package ro.somethinggoodtoeat.restaurantmanagementsystem.converter;

import lombok.Synchronized;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import ro.somethinggoodtoeat.restaurantmanagementsystem.domain.MenuEntity;
import ro.somethinggoodtoeat.restaurantmanagementsystem.domain.OrderDetailsEntity;
import ro.somethinggoodtoeat.restaurantmanagementsystem.dto.OrderDetailsDto;
import ro.somethinggoodtoeat.restaurantmanagementsystem.exception.SomethingGoodToEatException;
import ro.somethinggoodtoeat.restaurantmanagementsystem.repository.MenuRepository;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class OrderDetailsEntityToOrderDetailsDtoConverter implements Converter<OrderDetailsEntity, OrderDetailsDto> {

    private final MenuRepository menuRepository;
    private final MenuEntityToMenuDtoConverter menuDtoConverter;

    public OrderDetailsEntityToOrderDetailsDtoConverter(MenuRepository menuRepository,
                                                        MenuEntityToMenuDtoConverter menuDtoConverter) {
        this.menuRepository = menuRepository;
        this.menuDtoConverter = menuDtoConverter;
    }

    @Synchronized
    @Override
    public OrderDetailsDto convert(OrderDetailsEntity source) {

        if (source == null) {
            return null;
        }

        final OrderDetailsDto orderDetailsDto = new OrderDetailsDto();
        orderDetailsDto.setId(source.getId());
        orderDetailsDto.setAmount(source.getAmount());
        orderDetailsDto.setUnitOfMeasure(source.getUnitOfMeasure());
        orderDetailsDto.setOrderDetailsStatus(source.getOrderDetailsStatus());

        if (source.getOrder() != null) {
            orderDetailsDto.setOrderId(source.getOrder().getId());
        }

        if (source.getMenu() != null) {
            Long menuId = source.getMenu().getId();
            MenuEntity menuEntity = menuRepository.findById(menuId).orElseThrow(()
                    -> new SomethingGoodToEatException("Not found menu with id:" + menuId, "CLIENT_ERROR"));
            orderDetailsDto.setMenu(menuDtoConverter.convert(menuEntity));
        }

        return orderDetailsDto;
    }

    public List<OrderDetailsDto> toDTOList(List<OrderDetailsEntity> orderDetails) {
        return orderDetails.stream()
                .map(this::convert)
                .collect(Collectors.toList());
    }
}
