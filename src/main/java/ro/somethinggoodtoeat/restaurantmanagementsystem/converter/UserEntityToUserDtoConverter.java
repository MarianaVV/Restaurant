package ro.somethinggoodtoeat.restaurantmanagementsystem.converter;

import lombok.Synchronized;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;
import org.springframework.core.convert.converter.Converter;
import ro.somethinggoodtoeat.restaurantmanagementsystem.domain.UserEntity;
import ro.somethinggoodtoeat.restaurantmanagementsystem.dto.securityDto.UserDetails;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class UserEntityToUserDtoConverter implements Converter<UserEntity, UserDetails> {

    private final PermissionEntityToAuthorityDtoConverter permissionEntityToAuthorityDtoConverter;

    public UserEntityToUserDtoConverter(PermissionEntityToAuthorityDtoConverter permissionEntityToAuthorityDtoConverter) {
        this.permissionEntityToAuthorityDtoConverter = permissionEntityToAuthorityDtoConverter;
    }

    @Synchronized
    @Override
    public UserDetails convert(UserEntity source) {

        if (source == null) {
            return null;
        }

        final UserDetails userDetails = new UserDetails();
        userDetails.setId(source.getId());
        userDetails.setLastName(source.getLastName());
        userDetails.setFirstName(source.getFirstName());
        userDetails.setEmailAddress(source.getEmailAddress());
        userDetails.setRole(source.getRole().toString());
        userDetails.setEmailAddress(source.getEmailAddress());
        userDetails.setUsername(source.getUsername());
        userDetails.setPassword(source.getPassword());
        userDetails.setAccountNotExpired(source.getAccountNotExpired());
        userDetails.setAccountNotLocked(source.getAccountNotLocked());
        userDetails.setCredentialsNotExpired(source.getCredentialsNotExpired());
        userDetails.setEnabled(source.getEnabled());

        return userDetails;
    }

    public List<UserDetails> toDTOList(List<UserEntity> userEntities) {
        return userEntities.stream()
                .map(this::convert)
                .collect(Collectors.toList());
    }

    public List<UserDetails> toDTOList(Page<UserEntity> userPage) {
        List<UserEntity> userEntities = userPage.getContent();
        return toDTOList(userEntities);
    }
}
