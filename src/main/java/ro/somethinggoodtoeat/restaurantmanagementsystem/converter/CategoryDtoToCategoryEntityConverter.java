package ro.somethinggoodtoeat.restaurantmanagementsystem.converter;

import lombok.Synchronized;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import ro.somethinggoodtoeat.restaurantmanagementsystem.domain.CategoryEntity;
import ro.somethinggoodtoeat.restaurantmanagementsystem.dto.CategoryDto;

@Component
public class CategoryDtoToCategoryEntityConverter implements Converter<CategoryDto, CategoryEntity> {

    @Synchronized
    @Override
    public CategoryEntity convert(CategoryDto source) {

        if (source == null) {
            return null;
        }

        final CategoryEntity categoryEntity = new CategoryEntity();
        categoryEntity.setId(source.getId());
        categoryEntity.setName(source.getName());
        return categoryEntity;
    }
}
