package ro.somethinggoodtoeat.restaurantmanagementsystem.converter;

import lombok.Synchronized;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import ro.somethinggoodtoeat.restaurantmanagementsystem.domain.CategoryEntity;
import ro.somethinggoodtoeat.restaurantmanagementsystem.dto.CategoryDto;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class CategoryEntityToCategoryDtoConverter implements Converter<CategoryEntity, CategoryDto> {

    @Synchronized
    @Override
    public CategoryDto convert(CategoryEntity source) {

        if (source == null) {
            return null;
        }

        final CategoryDto category = new CategoryDto();
        category.setId(source.getId());
        category.setName(source.getName());
        return category;
    }

    public List<CategoryDto> toDTOList(List<CategoryEntity> categories) {
        return categories.stream()
                .map(this::convert)
                .collect(Collectors.toList());
    }
}
