package ro.somethinggoodtoeat.restaurantmanagementsystem.converter;

import lombok.Synchronized;
import org.springframework.stereotype.Component;
import ro.somethinggoodtoeat.restaurantmanagementsystem.domain.TableEntity;
import ro.somethinggoodtoeat.restaurantmanagementsystem.dto.TableDto;
import org.springframework.core.convert.converter.Converter;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class TableEntityToTableDtoConverter implements Converter<TableEntity, TableDto> {


    @Synchronized
    @Override
    public TableDto convert(TableEntity source) {


        if (source == null) {
            return null;
        }

        final TableDto tableDto = new TableDto();
        tableDto.setId(source.getId());
        tableDto.setName(source.getName());
        tableDto.setStatus(source.getTableStatus());
        tableDto.setNumberOfSeats(source.getNumberOfSeats());

        return tableDto;
    }

    public List<TableDto> toDTOList(List<TableEntity> tables) {
        return tables.stream()
                .map(this::convert)
                .collect(Collectors.toList());
    }
}
