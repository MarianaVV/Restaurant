package ro.somethinggoodtoeat.restaurantmanagementsystem.converter;

import lombok.Synchronized;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import ro.somethinggoodtoeat.restaurantmanagementsystem.domain.UserEntity;
import ro.somethinggoodtoeat.restaurantmanagementsystem.dto.securityDto.UserDetails;
import org.springframework.core.convert.converter.Converter;
import ro.somethinggoodtoeat.restaurantmanagementsystem.enums.Role;

@Component
public class UserDtoToUserEntityConverter implements Converter<UserDetails, UserEntity> {

    private final PasswordEncoder passwordEncoder;

    public UserDtoToUserEntityConverter(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    @Synchronized
    @Override
    public UserEntity convert(UserDetails source) {

        if (source == null) {
            return null;
        }

        final UserEntity userEntity = new UserEntity();
        userEntity.setId(source.getId());
        userEntity.setLastName(source.getLastName());
        userEntity.setFirstName(source.getFirstName());
        userEntity.setRole(Role.valueOf(mapRole(source.getRole())));
        userEntity.setEmailAddress(source.getEmailAddress());
        userEntity.setUsername(source.getUsername());

        if (passwordEncoder != null) {
            userEntity.setPassword(passwordEncoder.encode(source.getPassword()));
        }
        userEntity.setAccountNotExpired(source.getAccountNotExpired());
        userEntity.setAccountNotLocked(source.getAccountNotLocked());
        userEntity.setEnabled(source.getEnabled());

        return userEntity;
    }

    public String mapRole(String inputRole) {
        if (inputRole == null || inputRole.isEmpty()) {
            return null;
        }

        String upperCaseRole = inputRole.toUpperCase();

        if (upperCaseRole.equals("MANAGER")) {
            return "ROLE_MANAGER";
        }
        if (upperCaseRole.equals("WAITER")) {
            return "ROLE_WAITER";
        }
        if (upperCaseRole.equals("CHEF")) {
            return "ROLE_CHEF";
        } else {
            return inputRole;
        }
    }
}
