package ro.somethinggoodtoeat.restaurantmanagementsystem.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import ro.somethinggoodtoeat.restaurantmanagementsystem.domain.OrderDetailsEntity;
import ro.somethinggoodtoeat.restaurantmanagementsystem.domain.OrderEntity;
import ro.somethinggoodtoeat.restaurantmanagementsystem.dto.OrderDetailsDto;
import ro.somethinggoodtoeat.restaurantmanagementsystem.dto.OrderDto;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class OrderEntityToOrderDtoConverter implements Converter<OrderEntity, OrderDto> {

    private final OrderDetailsEntityToOrderDetailsDtoConverter orderDetailsConverter;

    private final TableEntityToTableDtoConverter tableEntityToTableDtoConverter;


    public OrderEntityToOrderDtoConverter(OrderDetailsEntityToOrderDetailsDtoConverter orderDetailsConverter,
                                          TableEntityToTableDtoConverter tableEntityToTableDtoConverter) {
        this.orderDetailsConverter = orderDetailsConverter;
        this.tableEntityToTableDtoConverter = tableEntityToTableDtoConverter;
    }

    @Override
    public OrderDto convert(OrderEntity source) {
        if (source == null) {
            return null;
        }

        final OrderDto orderDto = new OrderDto();
        orderDto.setId(source.getId());
        orderDto.setName(source.getName());
        orderDto.setOrderStatus(source.getOrderStatus());
        if (source.getEatingTable() != null) {
            orderDto.setTableId(source.getEatingTable().getId());
        }
        orderDto.setTable(tableEntityToTableDtoConverter.convert(source.getEatingTable()));
        orderDto.setPaymentType(source.getPaymentType());
        orderDto.setOrderDetails(convertOrderDetails(source.getOrderDetailEntities()));
        orderDto.setTotalAmount(source.getPriceOfOrder());
        orderDto.setAmountDue(source.getAmountDue());
        orderDto.setReceivedAmount(source.getReceivedAmount());
        orderDto.setTip(source.getTip());

        return orderDto;
    }

    private List<OrderDetailsDto> convertOrderDetails(List<OrderDetailsEntity> orderDetailsEntityList) {
        if (orderDetailsEntityList == null) {
            return null;
        }

        List<OrderDetailsDto> orderDetailsDtoList = new ArrayList<>();
        for (OrderDetailsEntity orderDetailsEntity : orderDetailsEntityList) {
            OrderDetailsDto orderDetailsDto = orderDetailsConverter.convert(orderDetailsEntity);
            orderDetailsDtoList.add(orderDetailsDto);
        }
        return orderDetailsDtoList;
    }

    public List<OrderDto> toDTOList(List<OrderEntity> orderDetails) {
        return orderDetails.stream()
                .map(this::convert)
                .collect(Collectors.toList());
    }
}
