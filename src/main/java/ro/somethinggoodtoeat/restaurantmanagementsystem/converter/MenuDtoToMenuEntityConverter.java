package ro.somethinggoodtoeat.restaurantmanagementsystem.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import ro.somethinggoodtoeat.restaurantmanagementsystem.domain.MenuEntity;
import ro.somethinggoodtoeat.restaurantmanagementsystem.dto.MenuDto;

import java.io.IOException;

@Component
public class MenuDtoToMenuEntityConverter implements Converter<MenuDto, MenuEntity> {


    private final CategoryDtoToCategoryEntityConverter converter;

    public MenuDtoToMenuEntityConverter(CategoryDtoToCategoryEntityConverter converter) {
        this.converter = converter;
    }

    public MenuEntity convert(MenuDto source) {

        if (source == null) {
            return null;
        }

        final MenuEntity menuEntity = new MenuEntity();
        menuEntity.setId(source.getId());
        menuEntity.setName(source.getName());
        menuEntity.setPrice(source.getPrice());

        byte[] imageBytes = null;
        if (source.getImage() != null && !source.getImage().isEmpty()) {
            try {
                imageBytes = source.getImage().getBytes();
            } catch (IOException e) {
                throw new RuntimeException("Error converting MultipartFile to byte[]", e);
            }
        }

        menuEntity.setImage(imageBytes);
        menuEntity.setDescription(source.getDescription());
        if (source.getIsPopular() == null) {
            menuEntity.setIsPopular("N");
        } else {
            menuEntity.setIsPopular(source.getIsPopular());
        }

        return menuEntity;
    }
}
