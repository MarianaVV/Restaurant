package ro.somethinggoodtoeat.restaurantmanagementsystem.converter;

import lombok.Synchronized;
import org.springframework.stereotype.Component;
import ro.somethinggoodtoeat.restaurantmanagementsystem.domain.TableEntity;
import ro.somethinggoodtoeat.restaurantmanagementsystem.dto.TableDto;
import org.springframework.core.convert.converter.Converter;

@Component
public class TableDtoToTableEntityConverter implements Converter<TableDto, TableEntity> {

    @Synchronized
    public TableEntity convert(TableDto source) {

        if (source == null) {
            return null;
        }

        final TableEntity tableEntity = new TableEntity();
        tableEntity.setId(source.getId());
        tableEntity.setName(source.getName());
        tableEntity.setTableStatus(source.getStatus());
        tableEntity.setNumberOfSeats(source.getNumberOfSeats());

        return tableEntity;
    }
}
