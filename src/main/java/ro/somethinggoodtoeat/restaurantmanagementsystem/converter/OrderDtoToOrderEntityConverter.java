package ro.somethinggoodtoeat.restaurantmanagementsystem.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import ro.somethinggoodtoeat.restaurantmanagementsystem.domain.OrderDetailsEntity;
import ro.somethinggoodtoeat.restaurantmanagementsystem.domain.OrderEntity;
import ro.somethinggoodtoeat.restaurantmanagementsystem.domain.TableEntity;
import ro.somethinggoodtoeat.restaurantmanagementsystem.dto.OrderDetailsDto;
import ro.somethinggoodtoeat.restaurantmanagementsystem.dto.OrderDto;
import ro.somethinggoodtoeat.restaurantmanagementsystem.enums.OrderStatus;
import ro.somethinggoodtoeat.restaurantmanagementsystem.enums.PaymentType;
import ro.somethinggoodtoeat.restaurantmanagementsystem.enums.TableStatus;
import ro.somethinggoodtoeat.restaurantmanagementsystem.exception.SomethingGoodToEatException;
import ro.somethinggoodtoeat.restaurantmanagementsystem.repository.TableRepository;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class OrderDtoToOrderEntityConverter implements Converter<OrderDto, OrderEntity> {

    private final OrderDetailsDtoToOrderDetailsEntityConverter orderDetailsConverter;
    private final TableRepository tableRepository;

    private final OrderEntityToOrderDtoConverter orderEntityToOrderDtoConverter;

    public OrderDtoToOrderEntityConverter(OrderDetailsDtoToOrderDetailsEntityConverter orderDetailsConverter,
                                          TableRepository tableRepository,
                                          OrderEntityToOrderDtoConverter orderEntityToOrderDtoConverter) {
        this.orderDetailsConverter = orderDetailsConverter;
        this.tableRepository = tableRepository;
        this.orderEntityToOrderDtoConverter = orderEntityToOrderDtoConverter;
    }

    @Override
    public OrderEntity convert(OrderDto source) {
        if (source == null) {
            return null;
        }

        final OrderEntity orderEntity = new OrderEntity();
        if (source.getId() != null) {
            orderEntity.setId(source.getId());
        }
        orderEntity.setName(source.getName());
        orderEntity.setOrderDetailEntities(convertOrderDetails(source.getOrderDetails()));
        if (source.getReceivedAmount() != null) {
            orderEntity.setReceivedAmount(source.getReceivedAmount());
        }
        if (source.getTip() != null) {
            orderEntity.setTip(source.getTip());
        }
        if (source.getTotalAmount() != null) {
            orderEntity.setPriceOfOrder(source.getTotalAmount());
        } else {
            orderEntity.setPriceOfOrder(0.0);
        }
        if (orderEntity.getAmountDue() != null) {
            orderEntity.setAmountDue(source.getAmountDue());
        } else {
            orderEntity.setAmountDue(0.0);
        }
        if (source.getPaymentType() != null) {
            orderEntity.setPaymentType(source.getPaymentType());
        } else {
            orderEntity.setPaymentType(PaymentType.CASH);
        }
        if (source.getOrderStatus() != null) {
            orderEntity.setOrderStatus(source.getOrderStatus());
        } else {
            orderEntity.setOrderStatus(OrderStatus.PENDING);
        }
        orderEntity.setStartTime(convertToZoneDateTime());
        TableEntity tableEntity = tableRepository.findById(source.getTableId()).orElseThrow(()
                -> new SomethingGoodToEatException("Not found table with id:" + source.getTableId(), "CLIENT_ERROR"));
        tableEntity.setTableStatus(TableStatus.BUSY);
        tableRepository.save(tableEntity);
        orderEntity.setEatingTable(tableEntity);

        return orderEntity;
    }

    private LocalDateTime convertToZoneDateTime() {

        Instant now = Instant.now();
        ZoneId romaniaZone = ZoneId.of("Europe/Bucharest");
        ZonedDateTime zonedDateTime = now.atZone(romaniaZone);

        return zonedDateTime.toLocalDateTime();
    }

    private List<OrderDetailsEntity> convertOrderDetails(List<OrderDetailsDto> orderedDetailsItems) {

        if (orderedDetailsItems == null) {
            return null;
        }

        List<OrderDetailsEntity> orderDetailsEntityList = new ArrayList<>();
        for (OrderDetailsDto orderDetailsDto : orderedDetailsItems) {
            OrderDetailsEntity orderDetailsEntity = orderDetailsConverter.convert(orderDetailsDto);
            orderDetailsEntityList.add(orderDetailsEntity);
        }
        return orderDetailsEntityList;
    }

    public List<OrderDto> toDTOList(List<OrderEntity> orders) {
        return orders.stream()
                .map(orderEntityToOrderDtoConverter::convert)
                .collect(Collectors.toList());
    }
}
