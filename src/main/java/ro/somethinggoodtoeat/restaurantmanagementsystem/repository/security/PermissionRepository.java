package ro.somethinggoodtoeat.restaurantmanagementsystem.repository.security;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ro.somethinggoodtoeat.restaurantmanagementsystem.domain.PermissionEntity;

@Repository
public interface PermissionRepository extends CrudRepository<PermissionEntity, Long> {
}
