package ro.somethinggoodtoeat.restaurantmanagementsystem.repository.security;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ro.somethinggoodtoeat.restaurantmanagementsystem.domain.UserPermissionEntity;

@Repository
public interface PermissionUserRepository extends CrudRepository<UserPermissionEntity, Long> {


    <S extends UserPermissionEntity> S save(S entity);
}
