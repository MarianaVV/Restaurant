package ro.somethinggoodtoeat.restaurantmanagementsystem.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ro.somethinggoodtoeat.restaurantmanagementsystem.domain.OrderDetailsEntity;

import java.util.List;
import java.util.Optional;

@Repository
public interface OrderDetailsRepository extends JpaRepository<OrderDetailsEntity, Long> {

    List<OrderDetailsEntity> findAll();

    @Override
    <S extends OrderDetailsEntity> S save(S entity);

    @Override
    Optional<OrderDetailsEntity> findById(Long id);

    Page<OrderDetailsEntity> findAll(Pageable pageable);

    List<OrderDetailsEntity> getAllByOrderId(Long id);

}
