package ro.somethinggoodtoeat.restaurantmanagementsystem.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ro.somethinggoodtoeat.restaurantmanagementsystem.domain.CategoryEntity;

import java.util.List;
import java.util.Optional;

@Repository
public interface CategoryRepository extends CrudRepository<CategoryEntity, Long> {

    List<CategoryEntity> findAll();

    @Override
    <S extends CategoryEntity> S save(S entity);

    @Override
    Optional<CategoryEntity> findById(Long id);

    Optional<CategoryEntity> findByName(String name);

    Page<CategoryEntity> findAll(Pageable pageable);
}
