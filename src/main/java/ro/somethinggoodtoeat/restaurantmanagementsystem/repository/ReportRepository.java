package ro.somethinggoodtoeat.restaurantmanagementsystem.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ro.somethinggoodtoeat.restaurantmanagementsystem.domain.OrderEntity;

import java.util.List;
import java.util.Optional;

@Repository
public interface ReportRepository extends JpaRepository<OrderEntity, Long> {

    List<OrderEntity> findAll();

    @Override
    <S extends OrderEntity> S save(S entity);

    @Override
    Optional<OrderEntity> findById(Long id);

    Page<OrderEntity> findAll(Pageable pageable);
}
