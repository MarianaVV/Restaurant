package ro.somethinggoodtoeat.restaurantmanagementsystem.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ro.somethinggoodtoeat.restaurantmanagementsystem.domain.TableEntity;
import ro.somethinggoodtoeat.restaurantmanagementsystem.enums.TableStatus;

import java.util.List;
import java.util.Optional;

@Repository
public interface TableRepository extends CrudRepository<TableEntity, Long> {


    List<TableEntity> findAll();

    @Override
    <S extends TableEntity> S save(S entity);

    @Override
    Optional<TableEntity> findById(Long id);

    Page<TableEntity> findAll(Pageable pageable);

    List<TableEntity> findByTableStatus(TableStatus tableStatus);
}
