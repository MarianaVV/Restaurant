package ro.somethinggoodtoeat.restaurantmanagementsystem.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ro.somethinggoodtoeat.restaurantmanagementsystem.domain.MenuEntity;

import java.util.List;
import java.util.Optional;

@Repository
public interface MenuRepository extends JpaRepository<MenuEntity, Long> {

    List<MenuEntity> findAll();

    List<MenuEntity> findAllByIsPopular(String isPopular);

    @Override
    <S extends MenuEntity> S save(S entity);

    @Override
    Optional<MenuEntity> findById(Long id);

    @Override
    void delete(MenuEntity menuEntity);
}
