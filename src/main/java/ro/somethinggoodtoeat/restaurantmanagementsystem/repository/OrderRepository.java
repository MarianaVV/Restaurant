package ro.somethinggoodtoeat.restaurantmanagementsystem.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;
import ro.somethinggoodtoeat.restaurantmanagementsystem.domain.OrderEntity;
import org.springframework.stereotype.Repository;
import ro.somethinggoodtoeat.restaurantmanagementsystem.enums.OrderStatus;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Repository
public interface OrderRepository extends JpaRepository<OrderEntity, Long> {

    List<OrderEntity> findAll();

    @Override
    <S extends OrderEntity> S save(S entity);

    @Override
    Optional<OrderEntity> findById(Long id);

    Page<OrderEntity> findAll(Pageable pageable);

    @Modifying
    @Transactional
    @Query("UPDATE OrderEntity o SET o.orderStatus = :status WHERE o.id = :orderId")
    int updateOrderStatusById(Long orderId, OrderStatus status);

    List<OrderEntity> findByStartTimeBetween(LocalDateTime startDate, LocalDateTime endDate);

    List<OrderEntity> findByOrderStatus(OrderStatus orderStatus);
}
