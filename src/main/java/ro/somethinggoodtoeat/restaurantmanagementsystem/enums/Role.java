package ro.somethinggoodtoeat.restaurantmanagementsystem.enums;

public enum Role {

    WAITER("WAITER"),
    MANAGER("MANAGER"),
    CHEF("CHEF"),
    ROLE_WAITER("WAITER"),
    ROLE_MANAGER("MANAGER"),
    ROLE_CHEF("CHEF");

    private String shortName;

    Role(String shortName) {
        this.shortName = shortName;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getName() {
        return this.name();
    }
}
