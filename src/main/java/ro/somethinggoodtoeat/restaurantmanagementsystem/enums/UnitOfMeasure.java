package ro.somethinggoodtoeat.restaurantmanagementsystem.enums;

public enum UnitOfMeasure {

    PLATES,
    PIECES,
    CUPS;
}
