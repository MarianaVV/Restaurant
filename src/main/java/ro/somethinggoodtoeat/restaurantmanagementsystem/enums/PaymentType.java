package ro.somethinggoodtoeat.restaurantmanagementsystem.enums;

public enum PaymentType {

    CASH,
    CREDIT_CARD;
}
