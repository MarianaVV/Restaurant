package ro.somethinggoodtoeat.restaurantmanagementsystem.enums;

public enum TableStatus {

    AVAILABLE,
    BOOKED,
    BUSY;
}
