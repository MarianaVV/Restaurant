package ro.somethinggoodtoeat.restaurantmanagementsystem.enums;

public enum OrderStatus {

    PENDING,
    READY,
    DELIVERED,
    PAID;
}
