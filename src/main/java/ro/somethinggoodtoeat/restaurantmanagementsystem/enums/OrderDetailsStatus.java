package ro.somethinggoodtoeat.restaurantmanagementsystem.enums;

public enum OrderDetailsStatus {

    COOKING,
    READY;
}
