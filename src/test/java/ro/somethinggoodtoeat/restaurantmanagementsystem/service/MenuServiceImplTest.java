package ro.somethinggoodtoeat.restaurantmanagementsystem.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import ro.somethinggoodtoeat.restaurantmanagementsystem.converter.CategoryDtoToCategoryEntityConverter;
import ro.somethinggoodtoeat.restaurantmanagementsystem.converter.CategoryEntityToCategoryDtoConverter;
import ro.somethinggoodtoeat.restaurantmanagementsystem.converter.MenuDtoToMenuEntityConverter;
import ro.somethinggoodtoeat.restaurantmanagementsystem.converter.MenuEntityToMenuDtoConverter;
import ro.somethinggoodtoeat.restaurantmanagementsystem.domain.MenuEntity;
import ro.somethinggoodtoeat.restaurantmanagementsystem.dto.MenuDto;
import ro.somethinggoodtoeat.restaurantmanagementsystem.exception.SomethingGoodToEatException;
import ro.somethinggoodtoeat.restaurantmanagementsystem.repository.CategoryRepository;
import ro.somethinggoodtoeat.restaurantmanagementsystem.repository.MenuRepository;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.util.Collections;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class MenuServiceImplTest {

    @Mock
    private MenuRepository menuRepository;

    @Mock
    private MenuEntityToMenuDtoConverter menuEntityToMenuDtoConverter;

    @Mock
    private Validator validator;

    @InjectMocks
    private MenuServiceImpl menuService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void testDeleteMenu() {
        Long menuId = 1L;
        MenuEntity menuEntity = new MenuEntity();
        menuEntity.setId(menuId);
        when(menuRepository.findById(menuId)).thenReturn(Optional.of(menuEntity));

        menuService.deleteMenu(menuId);

        verify(menuRepository, times(1)).findById(menuId);
        verify(menuRepository, times(1)).delete(menuEntity);
    }

    @Test
    void testDeleteMenuMenuNotFound() {
        Long nonExistentMenuId = 999L;
        when(menuRepository.findById(nonExistentMenuId)).thenReturn(Optional.empty());

        assertThrows(SomethingGoodToEatException.class, () -> menuService.deleteMenu(nonExistentMenuId));
        verify(menuRepository, times(1)).findById(nonExistentMenuId);
        verify(menuRepository, times(0)).delete(any(MenuEntity.class));
    }

    @Test
    void testEdit() {
        Long menuId = 1L;
        MenuEntity menuEntity = new MenuEntity();
        menuEntity.setId(menuId);
        menuEntity.setName("Menu");
        when(menuRepository.findById(menuId)).thenReturn(Optional.of(menuEntity));
        when(menuEntityToMenuDtoConverter.convert(menuEntity)).thenReturn(new MenuDto());

        MenuDto result = menuService.edit(menuId);

        assertNotNull(result);
        verify(menuRepository, times(1)).findById(menuId);
        verify(menuEntityToMenuDtoConverter, times(1)).convert(menuEntity);
    }

    @Test
    void testEditMenuNotFound() {
        Long nonExistentMenuId = 999L;
        when(menuRepository.findById(nonExistentMenuId)).thenReturn(Optional.empty());

        assertThrows(SomethingGoodToEatException.class, () -> menuService.edit(nonExistentMenuId));
        verify(menuRepository, times(1)).findById(nonExistentMenuId);
        verify(menuEntityToMenuDtoConverter, times(0)).convert(any(MenuEntity.class));
    }

    @Test
    void testValidateValidMenuDto() {
        MenuDto menuDto = new MenuDto();
        menuDto.setName("Menu");

        Set<ConstraintViolation<MenuDto>> violations = Collections.emptySet();
        when(validator.validate(menuDto)).thenReturn(violations);

        assertDoesNotThrow(() -> menuService.validate(menuDto));
        verify(validator, times(1)).validate(menuDto);
    }

    @Test
    void testValidateInvalidMenuDto() {
        MenuDto menuDto = new MenuDto();

        Set<ConstraintViolation<MenuDto>> violations = new HashSet<>();
        violations.add(mock(ConstraintViolation.class));
        when(validator.validate(menuDto)).thenReturn(violations);

        assertThrows(ClassCastException.class, () -> menuService.validate(menuDto));
        verify(validator, times(1)).validate(menuDto);
    }

    @Test
    void testGetMenuById() {
        Long menuId = 1L;
        MenuEntity menuEntity = new MenuEntity();
        menuEntity.setId(menuId);
        menuEntity.setName("Menu");
        when(menuRepository.findById(menuId)).thenReturn(Optional.of(menuEntity));
        when(menuEntityToMenuDtoConverter.convert(menuEntity)).thenReturn(new MenuDto());

        MenuDto result = menuService.getMenuById(menuId);

        assertNotNull(result);
        verify(menuRepository, times(1)).findById(menuId);
        verify(menuEntityToMenuDtoConverter, times(1)).convert(menuEntity);
    }

    @Test
    void testGetMenuByIdMenuNotFound() {
        Long nonExistentMenuId = 999L;
        when(menuRepository.findById(nonExistentMenuId)).thenReturn(Optional.empty());

        assertThrows(SomethingGoodToEatException.class, () -> menuService.getMenuById(nonExistentMenuId));
        verify(menuRepository, times(1)).findById(nonExistentMenuId);
        verify(menuEntityToMenuDtoConverter, times(0)).convert(any(MenuEntity.class));
    }
}
