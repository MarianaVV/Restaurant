package ro.somethinggoodtoeat.restaurantmanagementsystem.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import ro.somethinggoodtoeat.restaurantmanagementsystem.converter.UserDtoToUserEntityConverter;
import ro.somethinggoodtoeat.restaurantmanagementsystem.converter.UserEntityToUserDtoConverter;
import ro.somethinggoodtoeat.restaurantmanagementsystem.domain.UserEntity;
import ro.somethinggoodtoeat.restaurantmanagementsystem.dto.securityDto.UserDetails;
import ro.somethinggoodtoeat.restaurantmanagementsystem.enums.Role;
import ro.somethinggoodtoeat.restaurantmanagementsystem.exception.SomethingGoodToEatException;
import ro.somethinggoodtoeat.restaurantmanagementsystem.repository.security.PermissionRepository;
import ro.somethinggoodtoeat.restaurantmanagementsystem.repository.security.PermissionUserRepository;
import ro.somethinggoodtoeat.restaurantmanagementsystem.repository.security.UserRepository;

import static org.junit.jupiter.api.Assertions.*;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import javax.validation.Validator;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class UserServiceImplTest {

    UserServiceImpl userService;

    @Mock
    UserRepository userRepository;

    @Mock
    UserDtoToUserEntityConverter userDtoToUserEntityConverter;
    @Mock
    UserEntityToUserDtoConverter userEntityToUserDtoConverter;
    @Mock
    Validator validator;

    @Mock
    PermissionRepository permissionRepository;

    @Mock
    PermissionUserRepository permissionUserRepository;

    @BeforeEach
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        userService = new UserServiceImpl(userRepository, userDtoToUserEntityConverter,
                userEntityToUserDtoConverter, validator,
                permissionRepository, permissionUserRepository);
    }

    @Test
    public void testGetAllUsers() {

        int page = 0;
        int size = 10;
        List<UserEntity> userEntityList = new ArrayList<>();
        Page<UserEntity> userPage = new PageImpl<>(userEntityList);
        when(userRepository.findAll(Mockito.any(Pageable.class))).thenReturn(userPage);

        Page<UserDetails> result = userService.getAllUsers(page, size);

        assertThat(result).isNotNull();
    }

    @Test
    public void testCreateUser() {

        UserEntity userEntity = new UserEntity();
        UserDetails userDetails = new UserDetails();
        when(userDtoToUserEntityConverter.convert(any(UserDetails.class))).thenReturn(userEntity);
        when(userRepository.save(any(UserEntity.class))).thenReturn(userEntity);
        when(userEntityToUserDtoConverter.convert(any(UserEntity.class))).thenReturn(userDetails);

        UserDetails result = userService.createUser(userDetails);

        verify(userDtoToUserEntityConverter, times(1)).convert(any(UserDetails.class));
        verify(userRepository, times(1)).save(any(UserEntity.class));
        verify(userEntityToUserDtoConverter, times(1)).convert(any(UserEntity.class));
        assertEquals(userDetails, result);
    }

    @Test
    public void testUpdateUser() {

        Long userId = 1L;
        UserDetails updateUserDetails = new UserDetails();
        updateUserDetails.setId(userId);
        updateUserDetails.setFirstName("First name");
        updateUserDetails.setLastName("Last name");
        updateUserDetails.setRole(Role.CHEF.toString());
        updateUserDetails.setEmailAddress("some email address");

        UserEntity existingUserEntity = new UserEntity();
        existingUserEntity.setId(userId);
        existingUserEntity.setFirstName("First name");
        existingUserEntity.setLastName("Last name");
        existingUserEntity.setRole(Role.CHEF);
        existingUserEntity.setEmailAddress("some email address");
        when(userRepository.findById(userId)).thenReturn(Optional.of(existingUserEntity));
        when(userRepository.save(any())).thenReturn(existingUserEntity);
        when(userEntityToUserDtoConverter.convert(any())).thenReturn(updateUserDetails);
        when(userDtoToUserEntityConverter.convert(any())).thenReturn(existingUserEntity);

        UserDetails result = userService.updateUser(userId, updateUserDetails);

        assertNotNull(result);
        assertEquals(updateUserDetails.getFirstName(), result.getFirstName());
        verify(userRepository, times(1)).save(any());
    }

    @Test
    public void testDeleteUser() {

        Long userId = 1L;
        UserEntity userEntity = new UserEntity();
        when(userRepository.findById(userId)).thenReturn(Optional.of(userEntity));

        userService.deleteUser(userId);

        verify(userRepository, times(1)).findById(userId);
        verify(userRepository, times(1)).delete(userEntity);
    }

    @Test
    public void testDeleteUserNotFound() {

        Long userId = 1L;
        when(userRepository.findById(userId)).thenReturn(Optional.empty());
        assertThrows(SomethingGoodToEatException.class, () -> {
            userService.deleteUser(userId);
        });

        verify(userRepository, times(1)).findById(userId);
        verify(userRepository, never()).delete(any(UserEntity.class));
    }
}
