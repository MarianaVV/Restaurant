package ro.somethinggoodtoeat.restaurantmanagementsystem.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import ro.somethinggoodtoeat.restaurantmanagementsystem.converter.OrderDetailsDtoToOrderDetailsEntityConverter;
import ro.somethinggoodtoeat.restaurantmanagementsystem.converter.OrderDetailsEntityToOrderDetailsDtoConverter;
import ro.somethinggoodtoeat.restaurantmanagementsystem.converter.OrderDtoToOrderEntityConverter;
import ro.somethinggoodtoeat.restaurantmanagementsystem.converter.OrderEntityToOrderDtoConverter;
import ro.somethinggoodtoeat.restaurantmanagementsystem.domain.OrderDetailsEntity;
import ro.somethinggoodtoeat.restaurantmanagementsystem.dto.OrderDetailsDto;
import ro.somethinggoodtoeat.restaurantmanagementsystem.repository.OrderDetailsRepository;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

import javax.validation.Validator;
import java.util.Optional;

import ro.somethinggoodtoeat.restaurantmanagementsystem.exception.SomethingGoodToEatException;
import ro.somethinggoodtoeat.restaurantmanagementsystem.repository.OrderRepository;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.when;

public class OrderDetailsServiceImplTest {

    OrderDetailsServiceImpl orderDetailsService;

    @Mock
    OrderDetailsRepository orderDetailsRepository;

    @Mock
    OrderDetailsEntityToOrderDetailsDtoConverter orderDetailsEntityToOrderDetailsDtoConverter;

    @Mock
    OrderDetailsDtoToOrderDetailsEntityConverter orderDetailsDtoToOrderDetailsEntityConverter;

    @Mock
    OrderDtoToOrderEntityConverter orderDtoToOrderEntityConverter;

    @Mock
    OrderEntityToOrderDtoConverter orderEntityToOrderDtoConverter;

    @Mock
    OrderRepository orderRepository;

    @Mock
    Validator validator;

    @BeforeEach
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        orderDetailsService = new OrderDetailsServiceImpl(orderDetailsRepository,
                orderDetailsEntityToOrderDetailsDtoConverter,
                orderDetailsDtoToOrderDetailsEntityConverter,
                orderDtoToOrderEntityConverter,
                orderEntityToOrderDtoConverter,
                orderRepository,
                validator
        );
    }

    @Test
    void testEdit() {
        Long orderDetailsId = 1L;
        OrderDetailsEntity orderDetailsEntity = new OrderDetailsEntity();
        orderDetailsEntity.setId(orderDetailsId);
        when(orderDetailsRepository.findById(orderDetailsId)).thenReturn(Optional.of(orderDetailsEntity));
        when(orderDetailsEntityToOrderDetailsDtoConverter.convert(orderDetailsEntity)).thenReturn(new OrderDetailsDto());

        OrderDetailsDto result = orderDetailsService.edit(orderDetailsId);

        assertNotNull(result);
        verify(orderDetailsRepository, times(1)).findById(orderDetailsId);
        verify(orderDetailsEntityToOrderDetailsDtoConverter,
                times(1)).convert(orderDetailsEntity);
    }

    @Test
    void testEditOrderDetailsNotFound() {
        Long nonExistentOrderDetailsId = 999L;
        when(orderDetailsRepository.findById(nonExistentOrderDetailsId)).thenReturn(Optional.empty());

        assertThrows(SomethingGoodToEatException.class, () -> orderDetailsService.edit(nonExistentOrderDetailsId));
        verify(orderDetailsRepository, times(1)).findById(nonExistentOrderDetailsId);
        verify(orderDetailsEntityToOrderDetailsDtoConverter, times(0)).convert(any(OrderDetailsEntity.class));
    }

    @Test
    void testUpdateOrdersDetailsOrderDetailsNotFound() {
        Long nonExistentOrderDetailsId = 999L;
        OrderDetailsDto updatedOrderDetailsDto = new OrderDetailsDto();
        updatedOrderDetailsDto.setId(nonExistentOrderDetailsId);

        when(orderDetailsRepository.findById(nonExistentOrderDetailsId)).thenReturn(Optional.empty());

        assertThrows(SomethingGoodToEatException.class, () -> orderDetailsService.updateOrdersDetails(nonExistentOrderDetailsId, updatedOrderDetailsDto));
        verify(orderDetailsRepository, times(1)).findById(nonExistentOrderDetailsId);
        verify(orderDetailsRepository, times(0)).save(any());
    }

    @Test
    void testDeleteOrderDetails() {
        Long orderDetailsId = 1L;
        OrderDetailsEntity orderDetailsEntity = new OrderDetailsEntity();
        orderDetailsEntity.setId(orderDetailsId);
        when(orderDetailsRepository.findById(orderDetailsId)).thenReturn(Optional.of(orderDetailsEntity));

        orderDetailsService.deleteOrderDetails(orderDetailsId);

        verify(orderDetailsRepository, times(1)).findById(orderDetailsId);
        verify(orderDetailsRepository, times(1)).delete(orderDetailsEntity);
    }

    @Test
    void testDeleteOrderDetailsOrderDetailsNotFound() {
        Long nonExistentOrderDetailsId = 999L;
        when(orderDetailsRepository.findById(nonExistentOrderDetailsId)).thenReturn(Optional.empty());

        assertThrows(SomethingGoodToEatException.class, () -> orderDetailsService.deleteOrderDetails(nonExistentOrderDetailsId));
        verify(orderDetailsRepository, times(1)).findById(nonExistentOrderDetailsId);
        verify(orderDetailsRepository, times(0)).delete(any(OrderDetailsEntity.class));
    }
}
