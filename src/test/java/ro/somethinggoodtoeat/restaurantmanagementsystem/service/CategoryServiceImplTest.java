package ro.somethinggoodtoeat.restaurantmanagementsystem.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import ro.somethinggoodtoeat.restaurantmanagementsystem.converter.CategoryDtoToCategoryEntityConverter;
import ro.somethinggoodtoeat.restaurantmanagementsystem.converter.CategoryEntityToCategoryDtoConverter;
import ro.somethinggoodtoeat.restaurantmanagementsystem.domain.CategoryEntity;
import ro.somethinggoodtoeat.restaurantmanagementsystem.dto.CategoryDto;
import ro.somethinggoodtoeat.restaurantmanagementsystem.exception.SomethingGoodToEatException;
import ro.somethinggoodtoeat.restaurantmanagementsystem.repository.CategoryRepository;

import javax.validation.Validator;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class CategoryServiceImplTest {

    public static final String COFFEE = "Coffee";
    public static final String CATEGORY = "Category";
    public static final String UPDATED_CATEGORY = "Updated Category";

    @Mock
    CategoryRepository categoryRepository;

    @Mock
    CategoryDtoToCategoryEntityConverter categoryDtoToCategoryEntityConverter;
    @Mock
    CategoryEntityToCategoryDtoConverter categoryEntityToCategoryDtoConverter;
    @Mock
    Validator validator;

    CategoryServiceImpl categoryService;

    @BeforeEach
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        categoryService = new CategoryServiceImpl
                (categoryRepository,
                        categoryDtoToCategoryEntityConverter,
                        categoryEntityToCategoryDtoConverter,
                        validator);
    }

    @Test
    void getAllCategoriesShouldReturnListOfCategoryDto() {

        List<CategoryEntity> categoriesList = Collections.singletonList(new CategoryEntity());
        List<CategoryDto> categoriesDtoList = Collections.singletonList(new CategoryDto());
        Page<CategoryEntity> categoryPage = mock(Page.class);

        when(categoryRepository.findAll(any(Pageable.class))).thenReturn(categoryPage);
        when(categoryPage.getContent()).thenReturn(categoriesList);
        when(categoryEntityToCategoryDtoConverter.toDTOList(categoriesList)).thenReturn(categoriesDtoList);

        Page<CategoryDto> result = categoryService.getAllCategories(0, 10);

        assertNotNull(result);
        assertEquals(categoriesDtoList.size(), result.getContent().size());
        verify(categoryRepository, times(1)).findAll(any(Pageable.class));
        verify(categoryEntityToCategoryDtoConverter, times(1)).toDTOList(categoriesList);
    }

    @Test
    public void createCategoryWithValidCategoryDtoShouldReturnCreatedCategoryDto() {

        CategoryDto categoryDto = new CategoryDto();
        categoryDto.setName(COFFEE);
        CategoryEntity categoryEntity = new CategoryEntity();
        categoryEntity.setName(COFFEE);

        when(categoryDtoToCategoryEntityConverter.convert(categoryDto)).thenReturn(categoryEntity);
        when(categoryRepository.save(categoryEntity)).thenReturn(categoryEntity);
        when(categoryEntityToCategoryDtoConverter.convert(categoryEntity)).thenReturn(categoryDto);

        CategoryDto result = categoryService.createCategory(categoryDto);

        assertNotNull(result);
        assertEquals(categoryDto.getName(), result.getName());
        verify(categoryRepository, times(1)).save(categoryEntity);

    }

    @Test
    void updateCategoryWithValidCategoryDtoShouldReturnUpdatedCategoryDto() {

        Long categoryId = 1L;
        CategoryDto updatedCategoryDto = new CategoryDto();
        updatedCategoryDto.setId(categoryId);
        updatedCategoryDto.setName(UPDATED_CATEGORY);

        CategoryEntity updatedCategoryEntity = new CategoryEntity();
        updatedCategoryEntity.setId(categoryId);
        updatedCategoryEntity.setName(UPDATED_CATEGORY);

        CategoryEntity existingCategoryEntity = new CategoryEntity();
        existingCategoryEntity.setId(categoryId);
        String originalCategory = "Original Category";
        existingCategoryEntity.setName(originalCategory);
        when(categoryRepository.findById(categoryId)).thenReturn(Optional.of(existingCategoryEntity));
        when(categoryRepository.save(any())).thenAnswer(invocation -> invocation.getArgument(0));
        when(categoryEntityToCategoryDtoConverter.convert(any())).thenReturn(updatedCategoryDto);
        when(categoryDtoToCategoryEntityConverter.convert(any())).thenReturn(updatedCategoryEntity);

        CategoryDto result = categoryService.updateCategory(categoryId, updatedCategoryDto);

        assertNotNull(result);
        assertEquals(updatedCategoryDto.getName(), result.getName());
        verify(categoryRepository, times(1)).save(any());
    }

    @Test
    void deleteCategoryWithValidCategoryIdShouldDeleteCategory() {

        Long categoryId = 1L;
        CategoryEntity categoryEntity = new CategoryEntity();
        categoryEntity.setId(categoryId);
        when(categoryRepository.findById(categoryId)).thenReturn(Optional.of(categoryEntity));

        categoryService.deleteCategory(categoryId);

        verify(categoryRepository, times(1)).findById(categoryId);
        verify(categoryRepository, times(1)).delete(categoryEntity);
    }

    @Test
    void deleteCategoryWithInvalidCategoryIdShouldThrowResourceNotFoundException() {
        Long nonExistentCategoryId = 999L;
        when(categoryRepository.findById(nonExistentCategoryId)).thenReturn(Optional.empty());

        assertThrows(SomethingGoodToEatException.class,
                () -> categoryService.deleteCategory(nonExistentCategoryId));
        verify(categoryRepository, times(1)).findById(nonExistentCategoryId);
        verify(categoryRepository, times(0)).delete(any(CategoryEntity.class));
    }

    @Test
    void editWithValidCategoryIdShouldReturnConvertedCategoryDto() {
        Long categoryId = 1L;
        CategoryEntity categoryEntity = new CategoryEntity();
        categoryEntity.setId(categoryId);
        categoryEntity.setName(CATEGORY);
        when(categoryRepository.findById(categoryId)).thenReturn(Optional.of(categoryEntity));
        when(categoryEntityToCategoryDtoConverter.convert(categoryEntity)).thenReturn(new CategoryDto());

        CategoryDto result = categoryService.edit(categoryId);

        assertNotNull(result);
        verify(categoryRepository, times(1)).findById(categoryId);
        verify(categoryEntityToCategoryDtoConverter, times(1)).convert(categoryEntity);
    }

    @Test
    void editWithNonexistentCategoryIdShouldThrowResourceNotFoundException() {

        Long nonExistentCategoryId = 999L;
        when(categoryRepository.findById(nonExistentCategoryId)).thenReturn(Optional.empty());

        assertThrows(SomethingGoodToEatException.class, () -> categoryService.edit(nonExistentCategoryId));
        verify(categoryRepository, times(1)).findById(nonExistentCategoryId);
        verify(categoryEntityToCategoryDtoConverter, times(0)).convert(any(CategoryEntity.class));
    }

    @Test
    void validateWithValidCategoryDtoShouldNotThrowException() {
        CategoryDto categoryDto = new CategoryDto();
        categoryDto.setName(CATEGORY);

        assertDoesNotThrow(() -> categoryService.validate(categoryDto));
    }
}
