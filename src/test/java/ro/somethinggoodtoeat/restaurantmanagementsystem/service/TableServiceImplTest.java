package ro.somethinggoodtoeat.restaurantmanagementsystem.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import ro.somethinggoodtoeat.restaurantmanagementsystem.converter.TableDtoToTableEntityConverter;
import ro.somethinggoodtoeat.restaurantmanagementsystem.converter.TableEntityToTableDtoConverter;
import ro.somethinggoodtoeat.restaurantmanagementsystem.domain.TableEntity;
import ro.somethinggoodtoeat.restaurantmanagementsystem.dto.TableDto;
import ro.somethinggoodtoeat.restaurantmanagementsystem.enums.TableStatus;
import ro.somethinggoodtoeat.restaurantmanagementsystem.exception.SomethingGoodToEatException;
import ro.somethinggoodtoeat.restaurantmanagementsystem.repository.TableRepository;

import javax.validation.Validator;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class TableServiceImplTest {

    TableServiceImpl tableService;

    @Mock
    TableRepository tableRepository;

    @Mock
    TableDtoToTableEntityConverter tableDtoToTableEntityConverter;

    @Mock
    TableEntityToTableDtoConverter tableEntityToTableDtoConverter;

    @Mock
    OrderServiceImpl orderService;

    @Mock
    Validator validator;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        tableService = new TableServiceImpl(
                tableRepository,
                tableDtoToTableEntityConverter,
                tableEntityToTableDtoConverter,
                orderService,
                validator
        );
    }

    @Test
    void testGetAllTablesEmptyResult() {

        int pageNumber = 0;
        int pageSize = 10;
        when(tableRepository.findAll(any())).thenReturn(new PageImpl<>(Collections.emptyList()));

        Page<TableDto> resultPage = tableService.getAllTables(pageNumber, pageSize);
        List<TableDto> resultList = resultPage.getContent();

        assertNotNull(resultPage);
        assertTrue(resultList.isEmpty());
        assertEquals(0, resultList.size());
        verify(tableRepository, times(1)).findAll(any());
        verify(tableEntityToTableDtoConverter, times(0)).convert(any(TableEntity.class));
    }

    @Test
    void testEdit() {

        Long tableId = 1L;
        TableEntity tableEntity = new TableEntity();
        tableEntity.setId(tableId);
        tableEntity.setName("Table 1");
        tableEntity.setTableStatus(TableStatus.AVAILABLE);
        tableEntity.setNumberOfSeats(4);

        when(tableRepository.findById(tableId)).thenReturn(Optional.of(tableEntity));
        when(tableEntityToTableDtoConverter.convert(tableEntity)).thenReturn(new TableDto());

        TableDto result = tableService.edit(tableId);

        assertNotNull(result);
        verify(tableRepository, times(1)).findById(tableId);
        verify(tableEntityToTableDtoConverter, times(1)).convert(tableEntity);
    }

    @Test
    void testEditTableNotFound() {

        Long nonExistentTableId = 999L;
        when(tableRepository.findById(nonExistentTableId)).thenReturn(Optional.empty());

        assertThrows(SomethingGoodToEatException.class, () -> tableService.edit(nonExistentTableId));
        verify(tableRepository, times(1)).findById(nonExistentTableId);
        verify(tableEntityToTableDtoConverter, times(0)).convert(any(TableEntity.class));
    }

    @Test
    void testUpdateTableNotFound() {

        Long nonExistentTableId = 999L;
        TableDto updatedTableDto = new TableDto();
        updatedTableDto.setId(nonExistentTableId);

        when(tableRepository.findById(nonExistentTableId)).thenReturn(Optional.empty());

        assertThrows(SomethingGoodToEatException.class, () -> tableService.updateTable(nonExistentTableId, updatedTableDto));
        verify(tableRepository, times(1)).findById(nonExistentTableId);
        verify(tableRepository, times(0)).save(any());
    }

    @Test
    void testDeleteTable() {

        Long tableId = 1L;
        TableEntity tableEntity = new TableEntity();
        tableEntity.setId(tableId);
        when(tableRepository.findById(tableId)).thenReturn(Optional.of(tableEntity));

        tableService.deleteTable(tableId);

        verify(tableRepository, times(1)).findById(tableId);
        verify(tableRepository, times(1)).delete(tableEntity);
    }

    @Test
    void testDeleteTableTableNotFound() {

        Long nonExistentTableId = 999L;
        when(tableRepository.findById(nonExistentTableId)).thenReturn(Optional.empty());


        assertThrows(SomethingGoodToEatException.class, () -> tableService.deleteTable(nonExistentTableId));
        verify(tableRepository, times(1)).findById(nonExistentTableId);
        verify(tableRepository, times(0)).delete(any(TableEntity.class));
    }
}
