package ro.somethinggoodtoeat.restaurantmanagementsystem.converter;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import ro.somethinggoodtoeat.restaurantmanagementsystem.domain.PermissionEntity;
import ro.somethinggoodtoeat.restaurantmanagementsystem.dto.securityDto.PermissionDto;

public class PermissionEntityToPermissionDtoConverterTest {

    private PermissionEntityToAuthorityDtoConverter converter;

    @BeforeEach
    public void setUp() {
        converter = new PermissionEntityToAuthorityDtoConverter();
    }

    @Test
    public void testConvert() {

        PermissionEntity entity = new PermissionEntity();
        entity.setId(1L);
        entity.setRole("ROLE_ADMIN");

        PermissionDto result = converter.convert(entity);

        Assertions.assertNotNull(result);
        Assertions.assertEquals(entity.getId(), result.getId());
        Assertions.assertEquals(entity.getRole(), result.getRole());
    }

    @Test
    public void testConvert_NullSource() {
        PermissionDto result = converter.convert(null);
        Assertions.assertNull(result);
    }
}
