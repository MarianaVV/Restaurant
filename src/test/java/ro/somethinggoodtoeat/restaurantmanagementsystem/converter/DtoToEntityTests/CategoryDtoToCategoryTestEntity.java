package ro.somethinggoodtoeat.restaurantmanagementsystem.converter.DtoToEntityTests;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import ro.somethinggoodtoeat.restaurantmanagementsystem.converter.CategoryDtoToCategoryEntityConverter;
import ro.somethinggoodtoeat.restaurantmanagementsystem.domain.CategoryEntity;
import ro.somethinggoodtoeat.restaurantmanagementsystem.dto.CategoryDto;

import static org.junit.jupiter.api.Assertions.*;

public class CategoryDtoToCategoryTestEntity {
    private CategoryDtoToCategoryEntityConverter converter;

    @BeforeEach
    public void setUp() {
        converter = new CategoryDtoToCategoryEntityConverter();
    }

    @Test
    public void testConvert() {

        CategoryDto dto = new CategoryDto();
        dto.setId(1L);
        dto.setName("Test Category");

        CategoryEntity result = converter.convert(dto);

        assertNotNull(result);
        assertEquals(dto.getId(), result.getId());
        assertEquals(dto.getName(), result.getName());
    }

    @Test
    public void testConvertNullSource() {

        CategoryEntity result = converter.convert(null);

        assertNull(result);
    }

    @Test
    public void testConvertWithEmptyName() {
        CategoryDto dto = new CategoryDto();
        dto.setId(2L);
        dto.setName("");

        CategoryEntity result = converter.convert(dto);

        assertNotNull(result);
        assertEquals(dto.getId(), result.getId());
        assertEquals(dto.getName(), result.getName());
    }

    @Test
    public void testConvertWithNullId() {
        CategoryDto dto = new CategoryDto();
        dto.setId(null);
        dto.setName("Another Test Category");

        CategoryEntity result = converter.convert(dto);

        assertNotNull(result);
        assertNull(result.getId());
        assertEquals(dto.getName(), result.getName());
    }
}
