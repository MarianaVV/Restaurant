package ro.somethinggoodtoeat.restaurantmanagementsystem.converter.DtoToEntityTests;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import ro.somethinggoodtoeat.restaurantmanagementsystem.converter.TableDtoToTableEntityConverter;
import ro.somethinggoodtoeat.restaurantmanagementsystem.domain.TableEntity;
import ro.somethinggoodtoeat.restaurantmanagementsystem.dto.TableDto;
import ro.somethinggoodtoeat.restaurantmanagementsystem.enums.TableStatus;

import static org.junit.jupiter.api.Assertions.*;

public class TableDtoToTableEntityConverterTest {

    private TableDtoToTableEntityConverter converter;

    @BeforeEach
    void setUp() {
        converter = new TableDtoToTableEntityConverter();
    }

    @Test
    void testConvert() {
        TableDto dto = new TableDto();
        long id = 1L;
        dto.setId(id);
        String testTable = "Test Table";
        dto.setName(testTable);
        dto.setStatus(TableStatus.AVAILABLE);
        int numberOfSeats = 4;
        dto.setNumberOfSeats(numberOfSeats);

        TableEntity result = converter.convert(dto);

        assertNotNull(result);
        assertEquals(dto.getId(), result.getId());
        assertEquals(dto.getName(), result.getName());
        assertEquals(dto.getStatus(), result.getTableStatus());
        assertEquals(dto.getNumberOfSeats(), result.getNumberOfSeats());
    }

    @Test
    void testConvertNullSource() {
        TableEntity result = converter.convert(null);
        assertNull(result);
    }

    @Test
    void testConvertStatusNull() {
        TableDto dto = new TableDto();
        dto.setId(1L);
        dto.setName("Test Table");
        dto.setStatus(TableStatus.AVAILABLE);
        dto.setNumberOfSeats(4);

        TableEntity result = converter.convert(dto);

        assertNotNull(result);
        assertEquals(TableStatus.AVAILABLE, result.getTableStatus());
    }

    @Test
    void testConvertNumberOfSeatsZero() {
        TableDto dto = new TableDto();
        dto.setId(1L);
        dto.setName("Test Table");
        dto.setStatus(TableStatus.AVAILABLE);
        dto.setNumberOfSeats(1);

        TableEntity result = converter.convert(dto);

        assertNotNull(result);
        assertEquals(1, result.getNumberOfSeats());
    }

    @Test
    void testConvertEmptyName() {
        TableDto dto = new TableDto();
        dto.setId(1L);
        dto.setName("");
        dto.setStatus(TableStatus.AVAILABLE);
        dto.setNumberOfSeats(4);

        TableEntity result = converter.convert(dto);

        assertNotNull(result);
        assertEquals("", result.getName());
    }
}
