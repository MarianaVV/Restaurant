package ro.somethinggoodtoeat.restaurantmanagementsystem.converter.DtoToEntityTests;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import ro.somethinggoodtoeat.restaurantmanagementsystem.converter.OrderDetailsDtoToOrderDetailsEntityConverter;
import ro.somethinggoodtoeat.restaurantmanagementsystem.converter.OrderDtoToOrderEntityConverter;
import ro.somethinggoodtoeat.restaurantmanagementsystem.domain.OrderDetailsEntity;
import ro.somethinggoodtoeat.restaurantmanagementsystem.domain.OrderEntity;
import ro.somethinggoodtoeat.restaurantmanagementsystem.domain.TableEntity;
import ro.somethinggoodtoeat.restaurantmanagementsystem.dto.OrderDetailsDto;
import ro.somethinggoodtoeat.restaurantmanagementsystem.dto.OrderDto;
import ro.somethinggoodtoeat.restaurantmanagementsystem.enums.OrderStatus;
import ro.somethinggoodtoeat.restaurantmanagementsystem.enums.PaymentType;
import ro.somethinggoodtoeat.restaurantmanagementsystem.exception.SomethingGoodToEatException;
import ro.somethinggoodtoeat.restaurantmanagementsystem.repository.TableRepository;

import java.util.Collections;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class OrderDtoToOrderEntityConverterTest {

    public static final long ID = 1L;
    public static final double AMOUNT = 100.0;
    @Mock
    private OrderDetailsDtoToOrderDetailsEntityConverter orderDetailsConverter;

    @Mock
    private TableRepository tableRepository;

    @InjectMocks
    private OrderDtoToOrderEntityConverter converter;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void testConvert() {

        OrderDto dto = new OrderDto();
        dto.setId(ID);
        String testOrder = "Test Order";
        dto.setName(testOrder);
        dto.setTableId(ID);
        dto.setTotalAmount(AMOUNT);
        dto.setPaymentType(PaymentType.CASH);
        dto.setOrderStatus(OrderStatus.PENDING);

        OrderDetailsDto orderDetailsDto = new OrderDetailsDto();
        orderDetailsDto.setId(ID);
        orderDetailsDto.setAmount(AMOUNT);

        dto.setOrderDetails(Collections.singletonList(orderDetailsDto));

        TableEntity tableEntity = new TableEntity();
        tableEntity.setId(ID);
        when(tableRepository.findById(ID)).thenReturn(Optional.of(tableEntity));

        OrderDetailsEntity orderDetailsEntity1 = new OrderDetailsEntity();
        orderDetailsEntity1.setId(ID);
        when(orderDetailsConverter.convert(any(OrderDetailsDto.class))).thenReturn(orderDetailsEntity1);

        OrderEntity result = converter.convert(dto);

        assertNotNull(result);
        assertEquals(dto.getId(), result.getId());
        assertEquals(dto.getName(), result.getName());
        assertEquals(dto.getTotalAmount(), result.getPriceOfOrder());
        assertEquals(dto.getPaymentType(), result.getPaymentType());
        assertEquals(dto.getOrderStatus(), result.getOrderStatus());
        assertEquals(tableEntity, result.getEatingTable());
        assertEquals(1, result.getOrderDetailEntities().size());
        assertEquals(orderDetailsEntity1, result.getOrderDetailEntities().get(0));

        verify(tableRepository).findById(ID);
        verify(orderDetailsConverter).convert(any(OrderDetailsDto.class));
    }

    @Test
    void testConvertNullSource() {
        OrderEntity result = converter.convert(null);
        assertNull(result);
    }

    @Test
    void testConvertNoTableFound() {

        OrderDto dto = new OrderDto();
        dto.setId(ID);
        dto.setName("Test Order");
        dto.setTableId(ID);

        when(tableRepository.findById(anyLong())).thenReturn(Optional.empty());
        assertThrows(SomethingGoodToEatException.class, () -> converter.convert(dto));
    }

    @Test
    void testConvertTotalAmountNull() {

        OrderDto dto = new OrderDto();
        dto.setId(ID);
        dto.setName("Test Order");
        dto.setTableId(ID);
        dto.setTotalAmount(null);
        TableEntity tableEntity = new TableEntity();
        tableEntity.setId(ID);
        when(tableRepository.findById(anyLong())).thenReturn(Optional.of(tableEntity));

        OrderEntity result = converter.convert(dto);

        assertNotNull(result);
        assertEquals(0.0, result.getPriceOfOrder());
    }

    @Test
    void testConvert_PaymentTypeNull() {

        OrderDto dto = new OrderDto();
        dto.setId(ID);
        dto.setName("Test Order");
        dto.setTableId(ID);
        dto.setPaymentType(null);

        TableEntity tableEntity = new TableEntity();
        tableEntity.setId(ID);
        when(tableRepository.findById(anyLong())).thenReturn(Optional.of(tableEntity));

        OrderEntity result = converter.convert(dto);

        assertNotNull(result);
        assertEquals(PaymentType.CASH, result.getPaymentType());
    }
}
