package ro.somethinggoodtoeat.restaurantmanagementsystem.converter.DtoToEntityTests;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.security.crypto.password.PasswordEncoder;
import ro.somethinggoodtoeat.restaurantmanagementsystem.converter.UserDtoToUserEntityConverter;
import ro.somethinggoodtoeat.restaurantmanagementsystem.domain.UserEntity;
import ro.somethinggoodtoeat.restaurantmanagementsystem.dto.securityDto.UserDetails;
import ro.somethinggoodtoeat.restaurantmanagementsystem.enums.Role;
import static org.junit.jupiter.api.Assertions.*;

public class UserDtoToUserEntityConverterTestEntity {

    private UserDtoToUserEntityConverter converter;

    @Mock
    private PasswordEncoder passwordEncoder;

    @BeforeEach
    public void setUp() {
        converter = new UserDtoToUserEntityConverter(passwordEncoder);
    }

    @Test
    public void testConvert() {

        UserDetails dto = new UserDetails();
        dto.setId(1L);
        String name = "Doe";
        dto.setLastName(name);
        String firstName = "John";
        dto.setFirstName(firstName);
        dto.setRole(Role.ROLE_WAITER.name());
        String emailAddress = "john.doe@example.com";
        dto.setEmailAddress(emailAddress);
        String password = "somePassword";
        dto.setPassword(password);

        UserEntity result = converter.convert(dto);

        assertNotNull(result);
        assertEquals(dto.getId(), result.getId());
        assertEquals(dto.getLastName(), result.getLastName());
        assertEquals(dto.getFirstName(), result.getFirstName());
        assertEquals(Role.ROLE_WAITER, result.getRole());
        assertEquals(dto.getEmailAddress(), result.getEmailAddress());
    }

    @Test
    public void testConvertNullSource() {

        UserEntity result = converter.convert(null);

        Assertions.assertNull(result);
    }

    @Test
    void testConvertRoleMapping() {
        UserDetails dto = new UserDetails();
        dto.setId(1L);
        dto.setLastName("Doe");
        dto.setFirstName("John");
        dto.setRole("chef");
        dto.setEmailAddress("john.doe@example.com");
        dto.setPassword("somePassword");

        UserEntity result = converter.convert(dto);

        assertNotNull(result);
        assertEquals(Role.ROLE_CHEF, result.getRole());
    }

    @Test
    void testConvertNullPasswordEncoder() {
        UserDtoToUserEntityConverter converterWithoutPasswordEncoder = new UserDtoToUserEntityConverter
                (null);

        UserDetails dto = new UserDetails();
        dto.setId(1L);
        dto.setLastName("Doe");
        dto.setFirstName("John");
        dto.setRole(Role.ROLE_WAITER.name());
        dto.setEmailAddress("john.doe@example.com");
        dto.setPassword("somePassword");

        UserEntity result = converterWithoutPasswordEncoder.convert(dto);

        assertNotNull(result);
        assertNull(result.getPassword());
    }
}
