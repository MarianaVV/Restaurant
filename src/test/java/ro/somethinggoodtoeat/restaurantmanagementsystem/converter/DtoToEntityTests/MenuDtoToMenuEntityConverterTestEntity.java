package ro.somethinggoodtoeat.restaurantmanagementsystem.converter.DtoToEntityTests;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import ro.somethinggoodtoeat.restaurantmanagementsystem.converter.CategoryDtoToCategoryEntityConverter;
import ro.somethinggoodtoeat.restaurantmanagementsystem.converter.MenuDtoToMenuEntityConverter;
import ro.somethinggoodtoeat.restaurantmanagementsystem.domain.MenuEntity;
import ro.somethinggoodtoeat.restaurantmanagementsystem.dto.MenuDto;

import java.io.IOException;

public class MenuDtoToMenuEntityConverterTestEntity {

    private MenuDtoToMenuEntityConverter converter;

    @Mock
    private CategoryDtoToCategoryEntityConverter categoryConverter;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        converter = new MenuDtoToMenuEntityConverter(categoryConverter);
    }

    @Test
    public void testConvert() throws IOException {

        MenuDto menuDto = new MenuDto();
        menuDto.setId(1L);
        menuDto.setName("Pizza");
        menuDto.setPrice(10.99);
        menuDto.setDescription("Delicious pizza");

        MenuEntity result = converter.convert(menuDto);

        Assertions.assertNotNull(result);
        Assertions.assertEquals(menuDto.getId(), result.getId());
        Assertions.assertEquals(menuDto.getName(), result.getName());
        Assertions.assertEquals(menuDto.getPrice(), result.getPrice());
        Assertions.assertEquals(menuDto.getDescription(), result.getDescription());
    }

    @Test
    public void testConvertNullSource() {

        MenuEntity result = converter.convert(null);

        Assertions.assertNull(result);
    }

    @Test
    public void testConvertWithNullIsPopular() {
        MenuDto menuDto = new MenuDto();
        menuDto.setId(3L);
        menuDto.setName("Pasta");
        menuDto.setPrice(12.99);
        menuDto.setDescription("Delicious pasta");
        menuDto.setIsPopular(null);

        MenuEntity result = converter.convert(menuDto);

        Assertions.assertNotNull(result);
        Assertions.assertEquals(menuDto.getId(), result.getId());
        Assertions.assertEquals(menuDto.getName(), result.getName());
        Assertions.assertEquals(menuDto.getPrice(), result.getPrice());
        Assertions.assertEquals(menuDto.getDescription(), result.getDescription());
        Assertions.assertEquals("N", result.getIsPopular());
    }
}
