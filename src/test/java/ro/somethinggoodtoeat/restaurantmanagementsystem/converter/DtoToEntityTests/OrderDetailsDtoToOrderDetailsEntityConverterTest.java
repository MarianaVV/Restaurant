package ro.somethinggoodtoeat.restaurantmanagementsystem.converter.DtoToEntityTests;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import ro.somethinggoodtoeat.restaurantmanagementsystem.converter.MenuDtoToMenuEntityConverter;
import ro.somethinggoodtoeat.restaurantmanagementsystem.converter.OrderDetailsDtoToOrderDetailsEntityConverter;
import ro.somethinggoodtoeat.restaurantmanagementsystem.domain.MenuEntity;
import ro.somethinggoodtoeat.restaurantmanagementsystem.domain.OrderDetailsEntity;
import ro.somethinggoodtoeat.restaurantmanagementsystem.domain.OrderEntity;
import ro.somethinggoodtoeat.restaurantmanagementsystem.dto.OrderDetailsDto;
import ro.somethinggoodtoeat.restaurantmanagementsystem.enums.OrderDetailsStatus;
import ro.somethinggoodtoeat.restaurantmanagementsystem.enums.UnitOfMeasure;
import ro.somethinggoodtoeat.restaurantmanagementsystem.exception.SomethingGoodToEatException;
import ro.somethinggoodtoeat.restaurantmanagementsystem.repository.MenuRepository;
import ro.somethinggoodtoeat.restaurantmanagementsystem.repository.OrderRepository;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

public class OrderDetailsDtoToOrderDetailsEntityConverterTest {

    @Mock
    private MenuDtoToMenuEntityConverter menuConverter;

    @Mock
    private MenuRepository menuRepository;

    @Mock
    private OrderRepository orderRepository;

    @InjectMocks
    private OrderDetailsDtoToOrderDetailsEntityConverter converter;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void testConvert() {
        Long orderId = 1L;
        Long menuItemId = 2L;
        OrderDetailsDto dto = new OrderDetailsDto();
        dto.setId(1L);
        dto.setOrderId(orderId);
        dto.setMenuItemId(menuItemId);
        dto.setAmount(0.0);
        dto.setUnitOfMeasure(UnitOfMeasure.PLATES);
        dto.setOrderDetailsStatus(OrderDetailsStatus.COOKING);

        OrderEntity orderEntity = new OrderEntity();
        orderEntity.setId(orderId);
        when(orderRepository.findById(orderId)).thenReturn(Optional.of(orderEntity));

        MenuEntity menuEntity = new MenuEntity();
        menuEntity.setId(menuItemId);
        when(menuRepository.findById(menuItemId)).thenReturn(Optional.of(menuEntity));

        OrderDetailsEntity result = converter.convert(dto);

        assertNotNull(result);
        assertEquals(dto.getId(), result.getId());
        assertEquals(dto.getAmount(), result.getAmount());
        assertEquals(dto.getUnitOfMeasure(), result.getUnitOfMeasure());
        assertEquals(dto.getOrderDetailsStatus(), result.getOrderDetailsStatus());
        assertEquals(orderEntity, result.getOrder());
        assertEquals(menuEntity, result.getMenu());
    }

    @Test
    void testConvertNullSource() {
        OrderDetailsEntity result = converter.convert(null);
        assertNull(result);
    }

    @Test
    void testConvertNoOrderFound() {
        Long orderId = 1L;
        OrderDetailsDto dto = new OrderDetailsDto();
        dto.setId(1L);
        dto.setOrderId(orderId);
        dto.setAmount(0.0);
        dto.setUnitOfMeasure(UnitOfMeasure.PLATES);
        dto.setOrderDetailsStatus(OrderDetailsStatus.COOKING);

        when(orderRepository.findById(orderId)).thenReturn(Optional.empty());

        assertThrows(SomethingGoodToEatException.class, () -> converter.convert(dto));
    }

    @Test
    void testConvertNoMenuItemFound() {
        Long orderId = 1L;
        Long menuItemId = 2L;
        OrderDetailsDto dto = new OrderDetailsDto();
        dto.setId(1L);
        dto.setOrderId(orderId);
        dto.setMenuItemId(menuItemId);
        dto.setAmount(0.0);
        dto.setUnitOfMeasure(UnitOfMeasure.PLATES);
        dto.setOrderDetailsStatus(OrderDetailsStatus.COOKING);

        OrderEntity orderEntity = new OrderEntity();
        orderEntity.setId(orderId);
        when(orderRepository.findById(orderId)).thenReturn(Optional.of(orderEntity));
        when(menuRepository.findById(menuItemId)).thenReturn(Optional.empty());

        assertThrows(SomethingGoodToEatException.class, () -> converter.convert(dto));
    }
}
