package ro.somethinggoodtoeat.restaurantmanagementsystem.converter;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.security.crypto.password.PasswordEncoder;
import ro.somethinggoodtoeat.restaurantmanagementsystem.domain.PermissionEntity;
import ro.somethinggoodtoeat.restaurantmanagementsystem.domain.UserEntity;
import ro.somethinggoodtoeat.restaurantmanagementsystem.dto.securityDto.PermissionDto;
import ro.somethinggoodtoeat.restaurantmanagementsystem.dto.securityDto.UserDetails;
import ro.somethinggoodtoeat.restaurantmanagementsystem.enums.Role;
import ro.somethinggoodtoeat.restaurantmanagementsystem.repository.security.PermissionRepository;

import java.util.Set;

public class UserDtoToUserEntityConverterTest {

    @Mock
    private PermissionRepository permissionRepository;

    @Mock
    private PasswordEncoder passwordEncoder;

    private UserDtoToUserEntityConverter converter;

    @BeforeEach
    public void setUp() {

        PermissionDtoToPermissionEntityConverter authorityDtoToEntityConverter =
                new PermissionDtoToPermissionEntityConverter(permissionRepository);
        converter = new UserDtoToUserEntityConverter(passwordEncoder);
    }

    @Test
    public void testConvert_NullSource() {
        UserEntity result = converter.convert(null);
        Assertions.assertNull(result);
    }
}
