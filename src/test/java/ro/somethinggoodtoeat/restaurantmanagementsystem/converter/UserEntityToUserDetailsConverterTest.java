package ro.somethinggoodtoeat.restaurantmanagementsystem.converter;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import ro.somethinggoodtoeat.restaurantmanagementsystem.domain.UserEntity;
import ro.somethinggoodtoeat.restaurantmanagementsystem.dto.securityDto.PermissionDto;
import ro.somethinggoodtoeat.restaurantmanagementsystem.dto.securityDto.UserDetails;
import ro.somethinggoodtoeat.restaurantmanagementsystem.enums.Role;

import java.util.Set;

public class UserEntityToUserDetailsConverterTest {

    private UserEntityToUserDtoConverter converter;

    @BeforeEach
    public void setUp() {

        PermissionEntityToAuthorityDtoConverter permissionEntityToAuthorityDtoConverter = new PermissionEntityToAuthorityDtoConverter();
        converter = new UserEntityToUserDtoConverter(permissionEntityToAuthorityDtoConverter);
    }

    @Test
    public void testConvert() {

        UserEntity entity = new UserEntity();
        entity.setId(1L);
        entity.setLastName("Doe");
        entity.setFirstName("John");
        entity.setRole(Role.CHEF);

        UserDetails result = converter.convert(entity);

        Assertions.assertNotNull(result);
        Assertions.assertEquals(entity.getId(), result.getId());
        Assertions.assertEquals(entity.getLastName(), result.getLastName());
        Assertions.assertEquals(entity.getFirstName(), result.getFirstName());
    }

    @Test
    public void testConvert_NullSource() {
        UserDetails result = converter.convert(null);
        Assertions.assertNull(result);
    }
}
