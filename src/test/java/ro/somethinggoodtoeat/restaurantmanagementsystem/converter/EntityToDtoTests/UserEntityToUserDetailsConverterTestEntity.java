package ro.somethinggoodtoeat.restaurantmanagementsystem.converter.EntityToDtoTests;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import ro.somethinggoodtoeat.restaurantmanagementsystem.converter.PermissionEntityToAuthorityDtoConverter;
import ro.somethinggoodtoeat.restaurantmanagementsystem.converter.UserEntityToUserDtoConverter;
import ro.somethinggoodtoeat.restaurantmanagementsystem.domain.UserEntity;
import ro.somethinggoodtoeat.restaurantmanagementsystem.dto.securityDto.UserDetails;
import ro.somethinggoodtoeat.restaurantmanagementsystem.enums.Role;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

public class UserEntityToUserDetailsConverterTestEntity {

    private UserEntityToUserDtoConverter converter;

    public PermissionEntityToAuthorityDtoConverter permissionEntityToAuthorityDtoConverter;

    @BeforeEach
    public void setUp() {
        PermissionEntityToAuthorityDtoConverter permissionEntityToAuthorityDtoConverter = mock(PermissionEntityToAuthorityDtoConverter.class);
        converter = new UserEntityToUserDtoConverter(permissionEntityToAuthorityDtoConverter);
    }

    @Test
    public void testConvert() {

        UserEntity userEntity = new UserEntity();
        userEntity.setId(1L);
        userEntity.setLastName("Doe");
        userEntity.setFirstName("John");
        userEntity.setRole(Role.WAITER);
        userEntity.setEmailAddress("john.doe@example.com");

        UserDetails result = converter.convert(userEntity);

        assertNotNull(result);
        assertEquals(userEntity.getId(), result.getId());
        assertEquals(userEntity.getLastName(), result.getLastName());
        assertEquals(userEntity.getFirstName(), result.getFirstName());
        assertEquals(userEntity.getRole().toString(), result.getRole());
        assertEquals(userEntity.getEmailAddress(), result.getEmailAddress());
    }

    @Test
    public void testConvert_NullSource() {

        UserDetails result = converter.convert(null);

        assertNull(result);
    }

    @Test
    public void testConvertMultipleAttributes() {
        UserEntity userEntity = new UserEntity();
        userEntity.setId(1L);
        userEntity.setLastName("Doe");
        userEntity.setFirstName("John");
        userEntity.setRole(Role.MANAGER);
        userEntity.setEmailAddress("john.doe@example.com");
        userEntity.setUsername("johndoe");
        userEntity.setPassword("password");
        userEntity.setAccountNotExpired(true);
        userEntity.setAccountNotLocked(true);
        userEntity.setCredentialsNotExpired(true);
        userEntity.setEnabled(true);

        UserDetails result = converter.convert(userEntity);

        assertNotNull(result);
        assertEquals(userEntity.getId(), result.getId());
        assertEquals(userEntity.getLastName(), result.getLastName());
        assertEquals(userEntity.getFirstName(), result.getFirstName());
        assertEquals(userEntity.getRole().toString(), result.getRole());
        assertEquals(userEntity.getEmailAddress(), result.getEmailAddress());
        assertEquals(userEntity.getUsername(), result.getUsername());
        assertEquals(userEntity.getPassword(), result.getPassword());
    }
}
