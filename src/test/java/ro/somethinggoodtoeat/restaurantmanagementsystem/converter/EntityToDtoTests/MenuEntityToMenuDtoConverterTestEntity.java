package ro.somethinggoodtoeat.restaurantmanagementsystem.converter.EntityToDtoTests;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import ro.somethinggoodtoeat.restaurantmanagementsystem.converter.CategoryEntityToCategoryDtoConverter;
import ro.somethinggoodtoeat.restaurantmanagementsystem.converter.MenuEntityToMenuDtoConverter;
import ro.somethinggoodtoeat.restaurantmanagementsystem.domain.CategoryEntity;
import ro.somethinggoodtoeat.restaurantmanagementsystem.domain.MenuEntity;
import ro.somethinggoodtoeat.restaurantmanagementsystem.dto.CategoryDto;
import ro.somethinggoodtoeat.restaurantmanagementsystem.dto.MenuDto;

import java.util.Base64;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class MenuEntityToMenuDtoConverterTestEntity {

    private MenuEntityToMenuDtoConverter converter;

    @Mock
    private CategoryEntityToCategoryDtoConverter categoryConverter;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        converter = new MenuEntityToMenuDtoConverter(categoryConverter);
    }

    @Test
    public void testConvert() {

        MenuEntity menuEntity = new MenuEntity();
        long id = 1L;
        menuEntity.setId(id);
        String pizza = "Pizza";
        menuEntity.setName(pizza);
        double price = 10.99;
        menuEntity.setPrice(price);
        byte[] image = {0x12, 0x34, 0x56, 0x78};
        menuEntity.setImage(image);
        String deliciousPizza = "Delicious pizza";
        menuEntity.setDescription(deliciousPizza);
        when(categoryConverter.convert(menuEntity.getCategory())).thenReturn(null);

        MenuDto result = converter.convert(menuEntity);

        assertNotNull(result);
        assertEquals(menuEntity.getId(), result.getId());
        assertEquals(menuEntity.getName(), result.getName());
        assertEquals(menuEntity.getPrice(), result.getPrice());
        assertEquals(menuEntity.getDescription(), result.getDescription());
        assertEquals(convertImageToBase64(menuEntity.getImage()), result.getImageContent());
        assertNull(result.getCategory());
    }

    @Test
    public void testConvertNullSource() {

        MenuDto result = converter.convert(null);

        assertNull(result);
    }

    private String convertImageToBase64(byte[] image) {
        if (image != null && image.length > 0) {
            return Base64.getEncoder().encodeToString(image);
        }
        return null;
    }

    @Test
    void testConvertWithCategory() {

        CategoryEntity categoryEntity = new CategoryEntity();
        categoryEntity.setId(1L);
        categoryEntity.setName("Pizzas");

        MenuEntity menuEntity = new MenuEntity();
        menuEntity.setId(1L);
        menuEntity.setName("Margherita");
        menuEntity.setPrice(8.99);
        menuEntity.setImage(new byte[]{0x12, 0x34, 0x56, 0x78});
        menuEntity.setDescription("Classic margherita pizza");
        menuEntity.setCategory(categoryEntity);

        when(categoryConverter.convert(menuEntity.getCategory()))
                .thenReturn(mock(CategoryDto.class));

        MenuDto result = converter.convert(menuEntity);

        assertNotNull(result);
        assertEquals(menuEntity.getId(), result.getId());
        assertEquals(menuEntity.getName(), result.getName());
        assertEquals(menuEntity.getPrice(), result.getPrice());
        assertEquals(menuEntity.getDescription(), result.getDescription());
        assertEquals(convertImageToBase64(menuEntity.getImage()), result.getImageContent());
        assertNotNull(result.getCategory());
    }

    @Test
    void testConvertWithNullImage() {
        MenuEntity menuEntity = new MenuEntity();
        menuEntity.setId(1L);
        menuEntity.setName("Pepperoni");
        menuEntity.setPrice(10.99);
        menuEntity.setDescription("Spicy pepperoni pizza");
        menuEntity.setImage(null);
        when(categoryConverter.convert(menuEntity.getCategory())).thenReturn(null);

        MenuDto result = converter.convert(menuEntity);

        assertNotNull(result);
        assertNull(result.getImageContent());
    }

    @Test
    void testConvertWithEmptyImage() {
        MenuEntity menuEntity = new MenuEntity();
        menuEntity.setId(1L);
        menuEntity.setName("Veggie");
        menuEntity.setPrice(9.99);
        menuEntity.setDescription("Delicious veggie pizza");
        menuEntity.setImage(new byte[0]);
        when(categoryConverter.convert(menuEntity.getCategory())).thenReturn(null);

        MenuDto result = converter.convert(menuEntity);

        assertNotNull(result);
        assertNull(result.getImageContent());
    }
}
