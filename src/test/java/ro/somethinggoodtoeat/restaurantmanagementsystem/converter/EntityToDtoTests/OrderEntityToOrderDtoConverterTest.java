package ro.somethinggoodtoeat.restaurantmanagementsystem.converter.EntityToDtoTests;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import ro.somethinggoodtoeat.restaurantmanagementsystem.converter.OrderDetailsEntityToOrderDetailsDtoConverter;
import ro.somethinggoodtoeat.restaurantmanagementsystem.converter.OrderEntityToOrderDtoConverter;
import ro.somethinggoodtoeat.restaurantmanagementsystem.converter.TableEntityToTableDtoConverter;
import ro.somethinggoodtoeat.restaurantmanagementsystem.domain.OrderDetailsEntity;
import ro.somethinggoodtoeat.restaurantmanagementsystem.domain.OrderEntity;
import ro.somethinggoodtoeat.restaurantmanagementsystem.domain.TableEntity;
import ro.somethinggoodtoeat.restaurantmanagementsystem.dto.OrderDetailsDto;
import ro.somethinggoodtoeat.restaurantmanagementsystem.dto.OrderDto;
import ro.somethinggoodtoeat.restaurantmanagementsystem.dto.TableDto;
import ro.somethinggoodtoeat.restaurantmanagementsystem.enums.OrderStatus;
import ro.somethinggoodtoeat.restaurantmanagementsystem.enums.PaymentType;
import ro.somethinggoodtoeat.restaurantmanagementsystem.repository.TableRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

public class OrderEntityToOrderDtoConverterTest {

    @Mock
    private OrderDetailsEntityToOrderDetailsDtoConverter orderDetailsConverter;

    @Mock
    private TableEntityToTableDtoConverter tableEntityToTableDtoConverter;

    @Mock
    private TableRepository tableRepository;

    @InjectMocks
    private OrderEntityToOrderDtoConverter converter;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void testConvert() {
        Long tableId = 1L;
        Long orderDetailsId = 2L;
        OrderEntity orderEntity = new OrderEntity();
        orderEntity.setId(1L);
        String testOrder = "Test Order";
        orderEntity.setName(testOrder);
        orderEntity.setOrderStatus(OrderStatus.PENDING);
        orderEntity.setPaymentType(PaymentType.CASH);
        double priceOfOrder = 50.0;
        orderEntity.setPriceOfOrder(priceOfOrder);

        TableEntity tableEntity = new TableEntity();
        tableEntity.setId(tableId);
        when(tableRepository.findById(tableId)).thenReturn(java.util.Optional.of(tableEntity));
        when(tableEntityToTableDtoConverter.convert(tableEntity)).thenReturn(new TableDto());

        OrderDetailsEntity orderDetailsEntity = new OrderDetailsEntity();
        orderDetailsEntity.setId(orderDetailsId);
        when(orderDetailsConverter.convert(orderDetailsEntity)).thenReturn(new OrderDetailsDto());

        List<OrderDetailsEntity> orderDetailsList = new ArrayList<>();
        orderDetailsList.add(orderDetailsEntity);
        orderEntity.setOrderDetailEntities(orderDetailsList);
        orderEntity.setEatingTable(tableEntity);

        OrderDto result = converter.convert(orderEntity);

        assertNotNull(result);
        assertEquals(orderEntity.getId(), result.getId());
        assertEquals(orderEntity.getName(), result.getName());
        assertEquals(orderEntity.getOrderStatus(), result.getOrderStatus());
        assertEquals(tableId, result.getTableId());
        assertNotNull(result.getTable());
        assertEquals(orderEntity.getPaymentType(), result.getPaymentType());
        assertNotNull(result.getOrderDetails());
        assertEquals(orderDetailsList.size(), result.getOrderDetails().size());
        assertEquals(orderEntity.getPriceOfOrder(), result.getTotalAmount());
    }

    @Test
    void testConvertNullSource() {
        OrderDto result = converter.convert(null);
        assertNull(result);
    }

    @Test
    void testConvertEmptyOrderDetailsList() {
        Long tableId = 1L;
        OrderEntity orderEntity = new OrderEntity();
        orderEntity.setId(1L);
        String testOrder = "Test Order";
        orderEntity.setName(testOrder);
        orderEntity.setOrderStatus(OrderStatus.PENDING);
        orderEntity.setPaymentType(PaymentType.CASH);
        orderEntity.setPriceOfOrder(50.0);

        TableEntity tableEntity = new TableEntity();
        tableEntity.setId(tableId);
        when(tableRepository.findById(tableId)).thenReturn(java.util.Optional.of(tableEntity));
        when(tableEntityToTableDtoConverter.convert(tableEntity)).thenReturn(new TableDto());

        List<OrderDetailsEntity> orderDetailsList = new ArrayList<>();
        orderEntity.setOrderDetailEntities(orderDetailsList);
        orderEntity.setEatingTable(tableEntity);

        OrderDto result = converter.convert(orderEntity);

        assertNotNull(result);
        assertEquals(orderEntity.getId(), result.getId());
        assertEquals(orderEntity.getName(), result.getName());
        assertEquals(orderEntity.getOrderStatus(), result.getOrderStatus());
        assertEquals(tableId, result.getTableId());
        assertNotNull(result.getTable());
        assertEquals(orderEntity.getPaymentType(), result.getPaymentType());
        assertNotNull(result.getOrderDetails());
        assertEquals(0, result.getOrderDetails().size());
        assertEquals(orderEntity.getPriceOfOrder(), result.getTotalAmount());
    }

    @Test
    void testConvertNullTable() {
        OrderEntity orderEntity = new OrderEntity();
        orderEntity.setId(1L);
        String testOrder = "Test Order";
        orderEntity.setName(testOrder);
        orderEntity.setOrderStatus(OrderStatus.PENDING);
        orderEntity.setPaymentType(PaymentType.CASH);
        orderEntity.setPriceOfOrder(50.0);

        when(tableRepository.findById(anyLong())).thenReturn(Optional.empty());

        OrderDto result = converter.convert(orderEntity);

        assertNotNull(result);
        assertEquals(orderEntity.getId(), result.getId());
        assertEquals(orderEntity.getName(), result.getName());
        assertEquals(orderEntity.getOrderStatus(), result.getOrderStatus());
        assertNull(result.getTableId());
        assertNull(result.getTable());
        assertEquals(orderEntity.getPaymentType(), result.getPaymentType());
        assertNotNull(result.getOrderDetails());
        assertEquals(0, result.getOrderDetails().size());
        assertEquals(orderEntity.getPriceOfOrder(), result.getTotalAmount());
    }

    @Test
    void testConvertNullTableAndEmptyOrderDetailsList() {
        OrderEntity orderEntity = new OrderEntity();
        orderEntity.setId(1L);
        String testOrder = "Test Order";
        orderEntity.setName(testOrder);
        orderEntity.setOrderStatus(OrderStatus.PENDING);
        orderEntity.setPaymentType(PaymentType.CASH);
        orderEntity.setPriceOfOrder(50.0);

        when(tableRepository.findById(anyLong())).thenReturn(Optional.empty()); // Simulate table not found

        List<OrderDetailsEntity> orderDetailsList = new ArrayList<>();
        orderEntity.setOrderDetailEntities(orderDetailsList);

        OrderDto result = converter.convert(orderEntity);

        assertNotNull(result);
        assertEquals(orderEntity.getId(), result.getId());
        assertEquals(orderEntity.getName(), result.getName());
        assertEquals(orderEntity.getOrderStatus(), result.getOrderStatus());
        assertNull(result.getTableId());
        assertNull(result.getTable());
        assertEquals(orderEntity.getPaymentType(), result.getPaymentType());
        assertNotNull(result.getOrderDetails());
        assertEquals(0, result.getOrderDetails().size());
        assertEquals(orderEntity.getPriceOfOrder(), result.getTotalAmount());
    }
}
