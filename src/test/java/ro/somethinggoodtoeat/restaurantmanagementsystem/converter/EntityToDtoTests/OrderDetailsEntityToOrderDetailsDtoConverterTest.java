package ro.somethinggoodtoeat.restaurantmanagementsystem.converter.EntityToDtoTests;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import ro.somethinggoodtoeat.restaurantmanagementsystem.converter.MenuEntityToMenuDtoConverter;
import ro.somethinggoodtoeat.restaurantmanagementsystem.converter.OrderDetailsEntityToOrderDetailsDtoConverter;
import ro.somethinggoodtoeat.restaurantmanagementsystem.domain.MenuEntity;
import ro.somethinggoodtoeat.restaurantmanagementsystem.domain.OrderDetailsEntity;
import ro.somethinggoodtoeat.restaurantmanagementsystem.domain.OrderEntity;
import ro.somethinggoodtoeat.restaurantmanagementsystem.dto.OrderDetailsDto;
import ro.somethinggoodtoeat.restaurantmanagementsystem.enums.OrderDetailsStatus;
import ro.somethinggoodtoeat.restaurantmanagementsystem.enums.UnitOfMeasure;
import ro.somethinggoodtoeat.restaurantmanagementsystem.repository.MenuRepository;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

public class OrderDetailsEntityToOrderDetailsDtoConverterTest {

    @Mock
    private MenuRepository menuRepository;

    @Mock
    private MenuEntityToMenuDtoConverter menuDtoConverter;

    @InjectMocks
    private OrderDetailsEntityToOrderDetailsDtoConverter converter;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void testConvert() {
        Long orderId = 1L;
        Long menuId = 2L;
        OrderDetailsEntity orderDetailsEntity = new OrderDetailsEntity();
        orderDetailsEntity.setId(1L);
        orderDetailsEntity.setAmount(5.0);
        orderDetailsEntity.setUnitOfMeasure(UnitOfMeasure.PLATES);
        orderDetailsEntity.setOrderDetailsStatus(OrderDetailsStatus.COOKING);

        OrderEntity orderEntity = new OrderEntity();
        orderEntity.setId(orderId);
        orderDetailsEntity.setOrder(orderEntity);

        MenuEntity menuEntity = new MenuEntity();
        menuEntity.setId(menuId);
        when(menuRepository.findById(menuId)).thenReturn(Optional.of(menuEntity));
        when(menuDtoConverter.convert(menuEntity)).thenReturn(null);

        OrderDetailsDto result = converter.convert(orderDetailsEntity);

        assertNotNull(result);
        assertEquals(orderDetailsEntity.getId(), result.getId());
        assertEquals(orderDetailsEntity.getAmount(), result.getAmount());
        assertEquals(orderDetailsEntity.getUnitOfMeasure(), result.getUnitOfMeasure());
        assertEquals(orderDetailsEntity.getOrderDetailsStatus(), result.getOrderDetailsStatus());
        assertEquals(orderDetailsEntity.getOrder().getId(), result.getOrderId());
        assertNull(result.getMenu());
    }

    @Test
    void testConvertNullSource() {
        OrderDetailsDto result = converter.convert(null);
        assertNull(result);
    }

    @Test
    void testToDTOList() {
        Long orderId1 = 1L;
        Long orderId2 = 2L;
        Long menuId = 3L;
        OrderDetailsEntity orderDetailsEntity1 = new OrderDetailsEntity();
        orderDetailsEntity1.setId(1L);
        orderDetailsEntity1.setAmount(5.0);
        orderDetailsEntity1.setUnitOfMeasure(UnitOfMeasure.PLATES);
        orderDetailsEntity1.setOrderDetailsStatus(OrderDetailsStatus.COOKING);

        OrderDetailsEntity orderDetailsEntity2 = new OrderDetailsEntity();
        orderDetailsEntity2.setId(2L);
        orderDetailsEntity2.setAmount(3.0);
        orderDetailsEntity2.setUnitOfMeasure(UnitOfMeasure.PIECES);
        orderDetailsEntity2.setOrderDetailsStatus(OrderDetailsStatus.COOKING);

        OrderEntity orderEntity1 = new OrderEntity();
        orderEntity1.setId(orderId1);
        orderDetailsEntity1.setOrder(orderEntity1);

        OrderEntity orderEntity2 = new OrderEntity();
        orderEntity2.setId(orderId2);
        orderDetailsEntity2.setOrder(orderEntity2);

        MenuEntity menuEntity = new MenuEntity();
        menuEntity.setId(menuId);
        when(menuRepository.findById(menuId)).thenReturn(Optional.of(menuEntity));
        when(menuDtoConverter.convert(menuEntity)).thenReturn(null);

        List<OrderDetailsEntity> orderDetailsEntities = Arrays.asList(orderDetailsEntity1, orderDetailsEntity2);

        List<OrderDetailsDto> result = converter.toDTOList(orderDetailsEntities);

        assertNotNull(result);
        assertEquals(orderDetailsEntities.size(), result.size());

        for (int i = 0; i < orderDetailsEntities.size(); i++) {
            OrderDetailsEntity orderDetailsEntity = orderDetailsEntities.get(i);
            OrderDetailsDto orderDetailsDto = result.get(i);
            assertEquals(orderDetailsEntity.getId(), orderDetailsDto.getId());
            assertEquals(orderDetailsEntity.getAmount(), orderDetailsDto.getAmount());
            assertEquals(orderDetailsEntity.getUnitOfMeasure(), orderDetailsDto.getUnitOfMeasure());
            assertEquals(orderDetailsEntity.getOrderDetailsStatus(), orderDetailsDto.getOrderDetailsStatus());
            assertEquals(orderDetailsEntity.getOrder().getId(), orderDetailsDto.getOrderId());
            assertNull(orderDetailsDto.getMenu());
        }
    }

    @Test
    void testConvertWithNullOrder() {
        OrderDetailsEntity orderDetailsEntity = new OrderDetailsEntity();
        orderDetailsEntity.setId(1L);
        orderDetailsEntity.setAmount(5.0);
        orderDetailsEntity.setUnitOfMeasure(UnitOfMeasure.PLATES);
        orderDetailsEntity.setOrderDetailsStatus(OrderDetailsStatus.COOKING);
        orderDetailsEntity.setOrder(null);

        OrderDetailsDto result = converter.convert(orderDetailsEntity);

        assertNotNull(result);
        assertNull(result.getOrderId());
    }

    @Test
    void testToDTOListWithEmptyList() {
        List<OrderDetailsEntity> orderDetailsEntities = new ArrayList<>();

        List<OrderDetailsDto> result = converter.toDTOList(orderDetailsEntities);

        assertNotNull(result);
        assertEquals(0, result.size());
    }

    @Test
    void testToDTOListWithMultipleEntities() {
        Long orderId1 = 1L;
        Long orderId2 = 2L;
        Long menuId = 3L;

        OrderDetailsEntity orderDetailsEntity1 = new OrderDetailsEntity();
        orderDetailsEntity1.setId(1L);
        orderDetailsEntity1.setAmount(5.0);
        orderDetailsEntity1.setUnitOfMeasure(UnitOfMeasure.PLATES);
        orderDetailsEntity1.setOrderDetailsStatus(OrderDetailsStatus.COOKING);

        OrderDetailsEntity orderDetailsEntity2 = new OrderDetailsEntity();
        orderDetailsEntity2.setId(2L);
        orderDetailsEntity2.setAmount(3.0);
        orderDetailsEntity2.setUnitOfMeasure(UnitOfMeasure.PIECES);
        orderDetailsEntity2.setOrderDetailsStatus(OrderDetailsStatus.COOKING);

        OrderEntity orderEntity1 = new OrderEntity();
        orderEntity1.setId(orderId1);
        orderDetailsEntity1.setOrder(orderEntity1);

        OrderEntity orderEntity2 = new OrderEntity();
        orderEntity2.setId(orderId2);
        orderDetailsEntity2.setOrder(orderEntity2);

        MenuEntity menuEntity = new MenuEntity();
        menuEntity.setId(menuId);
        when(menuRepository.findById(menuId)).thenReturn(Optional.of(menuEntity));
        when(menuDtoConverter.convert(menuEntity)).thenReturn(null);

        List<OrderDetailsEntity> orderDetailsEntities = Arrays.asList(orderDetailsEntity1, orderDetailsEntity2);

        List<OrderDetailsDto> result = converter.toDTOList(orderDetailsEntities);

        assertNotNull(result);
        assertEquals(orderDetailsEntities.size(), result.size());

        for (int i = 0; i < orderDetailsEntities.size(); i++) {
            OrderDetailsEntity orderDetailsEntity = orderDetailsEntities.get(i);
            OrderDetailsDto orderDetailsDto = result.get(i);
            assertEquals(orderDetailsEntity.getId(), orderDetailsDto.getId());
            assertEquals(orderDetailsEntity.getAmount(), orderDetailsDto.getAmount());
            assertEquals(orderDetailsEntity.getUnitOfMeasure(), orderDetailsDto.getUnitOfMeasure());
            assertEquals(orderDetailsEntity.getOrderDetailsStatus(), orderDetailsDto.getOrderDetailsStatus());
            assertEquals(orderDetailsEntity.getOrder().getId(), orderDetailsDto.getOrderId());
            assertNull(orderDetailsDto.getMenu());
        }
    }

    @Test
    void testConvertWithNullMenu() {
        Long orderId = 1L;

        OrderDetailsEntity orderDetailsEntity = new OrderDetailsEntity();
        orderDetailsEntity.setId(1L);
        orderDetailsEntity.setAmount(5.0);
        orderDetailsEntity.setUnitOfMeasure(UnitOfMeasure.PLATES);
        orderDetailsEntity.setOrderDetailsStatus(OrderDetailsStatus.COOKING);

        OrderEntity orderEntity = new OrderEntity();
        orderEntity.setId(orderId);
        orderDetailsEntity.setOrder(orderEntity);

        when(menuRepository.findById(anyLong())).thenReturn(Optional.empty());

        OrderDetailsDto result = converter.convert(orderDetailsEntity);

        assertNotNull(result);
        assertEquals(orderDetailsEntity.getId(), result.getId());
        assertEquals(orderDetailsEntity.getAmount(), result.getAmount());
        assertEquals(orderDetailsEntity.getUnitOfMeasure(), result.getUnitOfMeasure());
        assertEquals(orderDetailsEntity.getOrderDetailsStatus(), result.getOrderDetailsStatus());
        assertEquals(orderDetailsEntity.getOrder().getId(), result.getOrderId());
        assertNull(result.getMenu());
    }
}
