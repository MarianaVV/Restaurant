package ro.somethinggoodtoeat.restaurantmanagementsystem.converter.EntityToDtoTests;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import ro.somethinggoodtoeat.restaurantmanagementsystem.converter.TableEntityToTableDtoConverter;
import ro.somethinggoodtoeat.restaurantmanagementsystem.domain.TableEntity;
import ro.somethinggoodtoeat.restaurantmanagementsystem.dto.TableDto;
import ro.somethinggoodtoeat.restaurantmanagementsystem.enums.TableStatus;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TableEntityToTableDtoConverterTest {

    private TableEntityToTableDtoConverter converter;

    @BeforeEach
    void setUp() {
        converter = new TableEntityToTableDtoConverter();
    }

    @Test
    void testConvert() {
        Long tableId = 1L;
        String tableName = "Table 1";
        TableStatus tableStatus = TableStatus.AVAILABLE;
        int numberOfSeats = 4;

        TableEntity tableEntity = new TableEntity();
        tableEntity.setId(tableId);
        tableEntity.setName(tableName);
        tableEntity.setTableStatus(tableStatus);
        tableEntity.setNumberOfSeats(numberOfSeats);

        TableDto result = converter.convert(tableEntity);

        assertNotNull(result);
        assertEquals(tableEntity.getId(), result.getId());
        assertEquals(tableEntity.getName(), result.getName());
        assertEquals(tableEntity.getTableStatus(), result.getStatus());
        assertEquals(tableEntity.getNumberOfSeats(), result.getNumberOfSeats());
    }

    @Test
    void testConvert_NullSource() {
        TableDto result = converter.convert(null);
        assertNull(result);
    }

    @Test
    void testToDTOList() {
        List<TableEntity> tableEntities = new ArrayList<>();
        for (int i = 1; i <= 3; i++) {
            TableEntity tableEntity = mock(TableEntity.class);
            when(tableEntity.getId()).thenReturn((long) i);
            when(tableEntity.getName()).thenReturn("Table " + i);
            when(tableEntity.getTableStatus()).thenReturn(TableStatus.AVAILABLE);
            when(tableEntity.getNumberOfSeats()).thenReturn(4);
            tableEntities.add(tableEntity);
        }

        List<TableDto> result = converter.toDTOList(tableEntities);

        assertNotNull(result);
        assertEquals(tableEntities.size(), result.size());

        for (int i = 0; i < tableEntities.size(); i++) {
            TableEntity tableEntity = tableEntities.get(i);
            TableDto tableDto = result.get(i);
            assertEquals(tableEntity.getId(), tableDto.getId());
            assertEquals(tableEntity.getName(), tableDto.getName());
            assertEquals(tableEntity.getTableStatus(), tableDto.getStatus());
            assertEquals(tableEntity.getNumberOfSeats(), tableDto.getNumberOfSeats());
        }
    }
}
