package ro.somethinggoodtoeat.restaurantmanagementsystem.converter.EntityToDtoTests;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import ro.somethinggoodtoeat.restaurantmanagementsystem.converter.CategoryEntityToCategoryDtoConverter;
import ro.somethinggoodtoeat.restaurantmanagementsystem.domain.CategoryEntity;
import ro.somethinggoodtoeat.restaurantmanagementsystem.dto.CategoryDto;

import java.util.Arrays;
import java.util.List;

public class CategoryEntityToCategoryDtoConverterTestEntity {

    private CategoryEntityToCategoryDtoConverter converter;

    @BeforeEach
    public void setUp() {
        converter = new CategoryEntityToCategoryDtoConverter();
    }

    @Test
    public void testConvert() {

        CategoryEntity categoryEntity = new CategoryEntity();
        categoryEntity.setId(1L);
        categoryEntity.setName("Main Course");

        CategoryDto result = converter.convert(categoryEntity);

        Assertions.assertNotNull(result);
        Assertions.assertEquals(categoryEntity.getId(), result.getId());
        Assertions.assertEquals(categoryEntity.getName(), result.getName());
    }

    @Test
    public void testConvert_NullSource() {

        CategoryDto result = converter.convert(null);

        Assertions.assertNull(result);
    }

    @Test
    public void testToDTOList() {

        CategoryEntity categoryEntity1 = new CategoryEntity();
        categoryEntity1.setId(1L);
        categoryEntity1.setName("Main Course");

        CategoryEntity categoryEntity2 = new CategoryEntity();
        categoryEntity2.setId(2L);
        categoryEntity2.setName("Dessert");

        List<CategoryEntity> categoryEntityList = Arrays.asList(categoryEntity1, categoryEntity2);

        List<CategoryDto> result = converter.toDTOList(categoryEntityList);

        Assertions.assertNotNull(result);
        Assertions.assertEquals(categoryEntityList.size(), result.size());
        Assertions.assertEquals(categoryEntity1.getId(), result.get(0).getId());
        Assertions.assertEquals(categoryEntity1.getName(), result.get(0).getName());
        Assertions.assertEquals(categoryEntity2.getId(), result.get(1).getId());
        Assertions.assertEquals(categoryEntity2.getName(), result.get(1).getName());
    }
}
