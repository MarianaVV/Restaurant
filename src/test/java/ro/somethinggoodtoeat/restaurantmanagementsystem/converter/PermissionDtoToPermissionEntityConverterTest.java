package ro.somethinggoodtoeat.restaurantmanagementsystem.converter;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import ro.somethinggoodtoeat.restaurantmanagementsystem.domain.PermissionEntity;
import ro.somethinggoodtoeat.restaurantmanagementsystem.dto.securityDto.PermissionDto;
import ro.somethinggoodtoeat.restaurantmanagementsystem.repository.security.PermissionRepository;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

public class PermissionDtoToPermissionEntityConverterTest {

    @Mock
    private PermissionRepository permissionRepository;
    private PermissionDtoToPermissionEntityConverter converter;

    @BeforeEach
    public void setUp() {

        MockitoAnnotations.initMocks(this);
        converter = new PermissionDtoToPermissionEntityConverter(permissionRepository);
    }

    @Test
    public void testConvertNullSource() {
        PermissionEntity result = converter.convert(null);
        assertNull(result);
    }

    @Test
    public void testConvertAndSaveNullRepository() {

        PermissionDto dto = new PermissionDto();
        dto.setId(1L);
        dto.setRole("ROLE_ADMIN");

        PermissionEntity result = new PermissionDtoToPermissionEntityConverter(null).convertAndSave(dto);

        assertNull(result);
    }

    @Test
    public void testConvertAndSave() {
        PermissionDto dto = new PermissionDto();
        dto.setId(1L);
        dto.setRole("ROLE_ADMIN");

        PermissionEntity savedEntity = new PermissionEntity();
        savedEntity.setId(1L);
        savedEntity.setRole("ROLE_ADMIN");

        when(permissionRepository.save(any(PermissionEntity.class))).thenReturn(savedEntity);

        PermissionEntity result = converter.convertAndSave(dto);

        assertNotNull(result);
        assertEquals(dto.getId(), result.getId());
        assertEquals(dto.getRole(), result.getRole());
    }


}
