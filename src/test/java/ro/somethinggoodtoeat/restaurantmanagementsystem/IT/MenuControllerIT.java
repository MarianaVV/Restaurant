package ro.somethinggoodtoeat.restaurantmanagementsystem.IT;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;
import org.springframework.web.context.WebApplicationContext;
import ro.somethinggoodtoeat.restaurantmanagementsystem.dto.MenuDto;
import ro.somethinggoodtoeat.restaurantmanagementsystem.service.CategoryServiceImpl;
import ro.somethinggoodtoeat.restaurantmanagementsystem.service.MenuServiceImpl;

import java.util.Collections;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringJUnitConfig(ControllerTestConfig.class)
@SpringBootTest
public class MenuControllerIT extends BaseIT {


    @MockBean
    private MenuServiceImpl menuServiceImpl;

    @MockBean
    private CategoryServiceImpl categoryServiceImpl;

    public MenuControllerIT(WebApplicationContext wac) {
        super(wac);
    }

    @Test
    void getMenusWithRoleManager() throws Exception {

        Page<MenuDto> menuPage = new PageImpl<>(Collections.emptyList());
        when(menuServiceImpl.getAllMenus(anyInt(), anyInt())).thenReturn(menuPage);

        mockMvc.perform(get("/api/v1/menu/menus")
                        .with(httpBasic("manager", "testManager01234")))
                .andExpect(status().isOk())
                .andExpect(view().name("/menu/menu.html"))
                .andExpect(model().attributeExists("menus"));
    }

    @Test
    void displayNewMenuWithRoleManager() throws Exception {

        mockMvc.perform(get("/api/v1/menu/menu/new")
                        .with(httpBasic("manager", "testManager01234")))
                .andExpect(status().isOk())
                .andExpect(view().name("menu/create.html"));
    }

    @Test
    void deleteMenuWithRoleCook() throws Exception {

        mockMvc.perform(get("/api/v1/menu/menu/1/delete")
                        .with(httpBasic("cook", "testCook01234")))
                .andExpect(status().isForbidden());
    }

    @Test
    void findMenusWithRoleCook() throws Exception {

        mockMvc.perform(get("/api/v1/menu/**")
                        .with(httpBasic("cook", "testCook01234")))
                .andExpect(status().isForbidden());
    }
}
