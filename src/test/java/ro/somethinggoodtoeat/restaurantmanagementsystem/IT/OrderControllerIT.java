package ro.somethinggoodtoeat.restaurantmanagementsystem.IT;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;
import org.springframework.web.context.WebApplicationContext;
import ro.somethinggoodtoeat.restaurantmanagementsystem.dto.OrderDto;
import ro.somethinggoodtoeat.restaurantmanagementsystem.service.OrderDetailsServiceImpl;
import ro.somethinggoodtoeat.restaurantmanagementsystem.service.OrderServiceImpl;
import ro.somethinggoodtoeat.restaurantmanagementsystem.service.TableServiceImpl;

import java.util.Collections;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringJUnitConfig(ControllerTestConfig.class)
@SpringBootTest
public class OrderControllerIT extends BaseIT {

    @MockBean
    private OrderServiceImpl orderServiceImpl;

    public OrderControllerIT(WebApplicationContext wac) {
        super(wac);
    }

    @Test
    void getAllOrders() throws Exception {

        Page<OrderDto> orderPage = new PageImpl<>(Collections.emptyList());
        when(orderServiceImpl.getAllOrders(anyInt(), anyInt())).thenReturn(orderPage);

        mockMvc.perform(get("/api/v1/order/orders")
                        .with(httpBasic("manager", "testManager01234")))
                .andExpect(status().isOk())
                .andExpect(view().name("/order/order.html"))
                .andExpect(model().attributeExists("orders"));
    }

    @Test
    void newOrderWithRoleManager() throws Exception {

        mockMvc.perform(get("/api/v1/order/order/new")
                        .with(httpBasic("manager", "testManager01234")))
                .andExpect(status().isOk())
                .andExpect(view().name("order/create.html"));
    }
}
