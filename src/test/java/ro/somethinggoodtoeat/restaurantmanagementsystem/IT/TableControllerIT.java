package ro.somethinggoodtoeat.restaurantmanagementsystem.IT;

import static org.mockito.Mockito.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;
import org.springframework.web.context.WebApplicationContext;
import ro.somethinggoodtoeat.restaurantmanagementsystem.dto.TableDto;
import ro.somethinggoodtoeat.restaurantmanagementsystem.service.TableServiceImpl;

import java.util.Arrays;

import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringJUnitConfig(ControllerTestConfig.class)
@SpringBootTest
public class TableControllerIT extends BaseIT {

    @MockBean
    private TableServiceImpl tableService;

    public TableControllerIT(WebApplicationContext wac) {
        super(wac);
    }

    @BeforeEach
    public void setUp() {

        reset(tableService);
    }

    @Test
    void newTableWithRoleManager() throws Exception {

        mockMvc.perform(get("/api/v1/table/table/new")
                        .with(httpBasic("manager", "testManager01234")))
                .andExpect(status().isOk())
                .andExpect(view().name("table/create.html"))
                .andExpect(model().attributeExists("table"))
                .andExpect(model().attributeExists("statuses"));
    }

    @Test
    void newTableWithRoleWaiter() throws Exception {

        mockMvc.perform(get("/api/v1/table/table/new")
                        .with(httpBasic("waiter", "testWaiter01234")))
                .andExpect(status().isOk())
                .andExpect(view().name("table/create.html"))
                .andExpect(model().attributeExists("table"))
                .andExpect(model().attributeExists("statuses"));
    }

    @Test
    void editTableWithRoleManager() throws Exception {
        Long tableId = 1L;
        TableDto tableDto = new TableDto();
        when(tableService.edit(tableId)).thenReturn(tableDto);

        mockMvc.perform(get("/api/v1/table/table/{id}/edit", tableId)
                        .with(httpBasic("manager", "testManager01234")))
                .andExpect(status().isOk())
                .andExpect(view().name("table/edit.html"))
                .andExpect(model().attributeExists("table"))
                .andExpect(model().attributeExists("statuses"));

    }

    @Test
    void editTableWithRoleWaiter() throws Exception {
        Long tableId = 1L;
        TableDto tableDto = new TableDto();
        when(tableService.edit(tableId)).thenReturn(tableDto);

        mockMvc.perform(get("/api/v1/table/table/{id}/edit", tableId)
                        .with(httpBasic("waiter", "testWaiter01234")))
                .andExpect(status().isOk())
                .andExpect(view().name("table/edit.html"))
                .andExpect(model().attributeExists("table"))
                .andExpect(model().attributeExists("statuses"));

    }
}
