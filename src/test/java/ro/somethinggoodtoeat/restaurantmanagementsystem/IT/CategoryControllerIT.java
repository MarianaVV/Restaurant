package ro.somethinggoodtoeat.restaurantmanagementsystem.IT;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.HttpMethod;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.web.context.WebApplicationContext;
import ro.somethinggoodtoeat.restaurantmanagementsystem.dto.CategoryDto;
import ro.somethinggoodtoeat.restaurantmanagementsystem.service.CategoryServiceImpl;

import java.util.Arrays;

import static org.mockito.Mockito.*;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringJUnitConfig(ControllerTestConfig.class)
@SpringBootTest
public class CategoryControllerIT extends BaseIT {

    @MockBean
    private CategoryServiceImpl categoryService;

    public CategoryControllerIT(WebApplicationContext wac) {
        super(wac);
    }

    @BeforeEach
    public void setUp() {
        reset(categoryService);
    }

    @Test
    void getAllCategoriesWithRoleManager() throws Exception {
        Page<CategoryDto> categoryPage =
                new PageImpl<>(Arrays.asList(new CategoryDto(), new CategoryDto()));
        when(categoryService.getAllCategories(anyInt(), anyInt())).thenReturn(categoryPage);

        mockMvc.perform(get("/api/v1/category/categories")
                        .with(httpBasic("manager", "testManager01234")))
                .andExpect(status().isOk())
                .andExpect(view().name("/category/category.html"))
                .andExpect(model().attributeExists("categories"))
                .andExpect(model().attribute("categories", categoryPage.getContent()));
    }

    @Test
    void getAllCategoriesWithRoleWaiterIsForbidden() throws Exception {
        Page<CategoryDto> categoryPage =
                new PageImpl<>(Arrays.asList(new CategoryDto(), new CategoryDto()));
        when(categoryService.getAllCategories(anyInt(), anyInt())).thenReturn(categoryPage);

        mockMvc.perform(get("/api/v1/category/categories")
                        .with(httpBasic("waiter", "testWaiter01234")))
                .andExpect(status().isForbidden());
    }

    @Test
    void getAllCategoriesWithRoleCookIsForbidden() throws Exception {
        Page<CategoryDto> categoryPage =
                new PageImpl<>(Arrays.asList(new CategoryDto(), new CategoryDto()));
        when(categoryService.getAllCategories(anyInt(), anyInt())).thenReturn(categoryPage);

        mockMvc.perform(get("/api/v1/category/categories")
                        .with(httpBasic("waiter", "testWaiter01234")))
                .andExpect(status().isForbidden());
    }

    @Test
    void newCategoryWithRoleWaiterIsForbidden() throws Exception {

        mockMvc.perform(get("/api/v1/category/category/new")
                        .with(httpBasic("waiter", "testWaiter01234")))
                .andExpect(status().isForbidden());
    }

    @Test
    void newCategoryWithRoleCookIsForbidden() throws Exception {

        mockMvc.perform(get("/api/v1/category/category/new")
                        .with(httpBasic("cook", "testCook01234")))
                .andExpect(status().isForbidden());
    }

    @Test
    void editCategoryWithRoleManager() throws Exception {
        Long categoryId = 1L;
        CategoryDto categoryDto = new CategoryDto();
        when(categoryService.edit(categoryId)).thenReturn(categoryDto);

        mockMvc.perform(get("/api/v1/category/category/{id}/edit", categoryId)
                        .with(httpBasic("manager", "testManager01234")))
                .andExpect(status().isOk())
                .andExpect(view().name("category/edit.html"))
                .andExpect(model().attributeExists("category"));
    }

    @Test
    void editCategoryWithRoleWaiter() throws Exception {
        Long categoryId = 1L;
        CategoryDto categoryDto = new CategoryDto();
        when(categoryService.edit(categoryId)).thenReturn(categoryDto);

        mockMvc.perform(get("/api/v1/category/category/{id}/edit", categoryId)
                        .with(httpBasic("waiter", "testWaiter01234")))
                .andExpect(status().isForbidden());
    }

    @Test
    void editCategoryWithRoleCook() throws Exception {
        Long categoryId = 1L;
        CategoryDto categoryDto = new CategoryDto();
        when(categoryService.edit(categoryId)).thenReturn(categoryDto);

        mockMvc.perform(get("/api/v1/category/category/{id}/edit", categoryId)
                        .with(httpBasic("cook", "testCook01234")))
                .andExpect(status().isForbidden());
    }

    @Test
    void createCategoryWithRoleWaiter() throws Exception {

        CategoryDto categoryDto = new CategoryDto();
        categoryDto.setId(1L);

        mockMvc.perform(post("/api/v1/category/category")
                        .with(httpBasic("waiter", "testWaiter01234"))
                        .flashAttr("category", categoryDto))
                .andExpect(status().isForbidden());

        verifyNoInteractions(categoryService);
    }

    @Test
    void createCategoryWithRoleCook() throws Exception {

        CategoryDto categoryDto = new CategoryDto();
        categoryDto.setId(1L);

        mockMvc.perform(post("/api/v1/category/category")
                        .with(httpBasic("cook", "testCook01234"))
                        .flashAttr("category", categoryDto))
                .andExpect(status().isForbidden());

        verifyNoInteractions(categoryService);
    }

    @Test
    void updateCategoryWithRoleWaiter() throws Exception {
        Long categoryId = 1L;
        CategoryDto categoryDto = new CategoryDto();
        categoryDto.setId(categoryId);

        mockMvc.perform(request(HttpMethod.POST, "/api/v1/category/category/{id}/update", categoryId)
                        .with(httpBasic("waiter", "testWaiter01234"))
                        .flashAttr("category", categoryDto))
                .andExpect(status().isForbidden());

        verifyNoInteractions(categoryService);
    }

    @Test
    void updateCategoryWithRoleCookIsForbidden() throws Exception {
        Long categoryId = 1L;
        CategoryDto categoryDto = new CategoryDto();
        categoryDto.setId(categoryId);

        mockMvc.perform(request(HttpMethod.POST, "/api/v1/category/category/{id}/update", categoryId)
                        .with(httpBasic("cook", "testCook01234"))
                        .flashAttr("category", categoryDto))
                .andExpect(status().isForbidden());
        verifyNoInteractions(categoryService);
    }

    @Test
    void deleteCategoryWithRoleWaiterIsForbidden() throws Exception {
        Long categoryId = 1L;

        mockMvc.perform(get("/api/v1/category/category/{id}/delete", categoryId)
                        .with(httpBasic("waiter", "testWaiter01234")))
                .andExpect(status().isForbidden());
    }

    @Test
    void deleteCategoryWithRoleCookIsForbidden() throws Exception {
        Long categoryId = 1L;

        mockMvc.perform(get("/api/v1/category/category/{id}/delete", categoryId)
                        .with(httpBasic("cook", "testCook01234")))
                .andExpect(status().isForbidden());
    }
}
