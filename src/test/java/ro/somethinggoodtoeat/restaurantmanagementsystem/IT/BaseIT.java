package ro.somethinggoodtoeat.restaurantmanagementsystem.IT;

import org.springframework.web.context.WebApplicationContext;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;

public class BaseIT {

    protected WebApplicationContext wac;

    protected MockMvc mockMvc;

    public BaseIT(WebApplicationContext wac) {
        this.wac = wac;
    }


    @BeforeEach
    public void setup() {
        mockMvc = MockMvcBuilders
                .webAppContextSetup(wac)
                .apply(springSecurity())
                .build();
    }
}
