package ro.somethinggoodtoeat.restaurantmanagementsystem.IT;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.web.context.WebApplicationContext;
import ro.somethinggoodtoeat.restaurantmanagementsystem.dto.securityDto.UserDetails;
import java.util.Arrays;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import ro.somethinggoodtoeat.restaurantmanagementsystem.service.UserServiceImpl;

@SpringJUnitConfig(ControllerTestConfig.class)
@SpringBootTest
public class UserControllerIT extends BaseIT {

    @MockBean
    private UserServiceImpl userService;

    public UserControllerIT(WebApplicationContext wac) {
        super(wac);
    }

    @BeforeEach
    public void setUp() {
        reset(userService);
    }

    @Test
    void testGetAllUsersWithRoleManager() throws Exception {
        Page<UserDetails> userPage = new PageImpl<>(Arrays.asList(new UserDetails(), new UserDetails()));
        when(userService.getAllUsers(anyInt(), anyInt())).thenReturn(userPage);

        mockMvc.perform(get("/api/v1/user/users")
                        .with(httpBasic("manager", "testManager01234")))
                .andExpect(status().isOk())
                .andExpect(view().name("/user/user.html"))
                .andExpect(model().attributeExists("users"))
                .andExpect(model().attribute("users", userPage.getContent()));
    }

    @Test
    void testGetAllUsersWithRoleWaiterIsForbidden() throws Exception {
        Page<UserDetails> userPage = new PageImpl<>(Arrays.asList(new UserDetails(), new UserDetails()));
        when(userService.getAllUsers(anyInt(), anyInt())).thenReturn(userPage);

        mockMvc.perform(get("/api/v1/user/users")
                        .with(httpBasic("waiter", "testWaiter01234")))
                .andExpect(status().isForbidden());
    }

    @Test
    void testGetAllUsersWithRoleCookIsForbidden() throws Exception {
        Page<UserDetails> userPage = new PageImpl<>(Arrays.asList(new UserDetails(), new UserDetails()));
        when(userService.getAllUsers(anyInt(), anyInt())).thenReturn(userPage);

        mockMvc.perform(get("/api/v1/user/users")
                        .with(httpBasic("cook", "testCook01234")))
                .andExpect(status().isForbidden());
    }

    @Test
    void testNewUserWithRoleWaiterIsForbidden() throws Exception {

        mockMvc.perform(get("/api/v1/user/user/new")
                        .with(httpBasic("waiter", "testWaiter01234")))
                .andExpect(status().isForbidden());
    }

    @Test
    void testNewUserWithRoleCookIsForbidden() throws Exception {

        mockMvc.perform(get("/api/v1/user/user/new")
                        .with(httpBasic("cook", "testCook01234")))
                .andExpect(status().isForbidden());
    }

    @Test
    void testEditUserWithRoleWaiterIsForbidden() throws Exception {
        Long userId = 1L;
        UserDetails userDetails = new UserDetails();
        when(userService.edit(userId)).thenReturn(userDetails);

        mockMvc.perform(get("/api/v1/user/user/{id}/edit", userId)
                        .with(httpBasic("waiter", "testWaiter01234")))
                .andExpect(status().isForbidden());
    }

    @Test
    void testEditUserWithRoleCookIsForbidden() throws Exception {
        Long userId = 1L;
        UserDetails userDetails = new UserDetails();
        when(userService.edit(userId)).thenReturn(userDetails);

        mockMvc.perform(get("/api/v1/user/user/{id}/edit", userId)
                        .with(httpBasic("cook", "testCook01234")))
                .andExpect(status().isForbidden());
    }

    @Test
    void testCreateUserWithRoleWaiterIsForbidden() throws Exception {

        UserDetails userDetails = new UserDetails();
        userDetails.setId(1L);
        String selectedRole = "WAITER";

        mockMvc.perform(post("/api/v1/user/user")
                        .with(httpBasic("waiter", "testWaiter01234"))
                        .flashAttr("user", userDetails)
                        .param("role", selectedRole))
                .andExpect(status().isForbidden());

    }

    @Test
    void testDeleteUserWithRoleWaiterIsForbidden() throws Exception {
        Long userId = 1L;

        mockMvc.perform(get("/api/v1/category/category/{id}/delete", userId)
                        .with(httpBasic("waiter", "testWaiter01234")))
                .andExpect(status().isForbidden());
    }

    @Test
    void testDeleteUserWithRoleCookIsForbidden() throws Exception {
        Long userId = 1L;

        mockMvc.perform(get("/api/v1/category/category/{id}/delete", userId)
                        .with(httpBasic("cook", "testCook01234")))
                .andExpect(status().isForbidden());
    }
}
