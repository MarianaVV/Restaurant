package ro.somethinggoodtoeat.restaurantmanagementsystem.controller;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import ro.somethinggoodtoeat.restaurantmanagementsystem.dto.TableDto;
import ro.somethinggoodtoeat.restaurantmanagementsystem.enums.TableStatus;
import ro.somethinggoodtoeat.restaurantmanagementsystem.service.TableServiceImpl;
import java.util.Arrays;
import java.util.Collections;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TableControllerTest {

    public static final String TABLE_CREATE = "table/create.html";
    @Mock
    private TableServiceImpl tableService;
    @Mock
    private Model model;
    @Mock
    private BindingResult bindingResult;
    @Mock
    private RedirectAttributes redirectAttributes;

    @InjectMocks
    private TableController tableController;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void testGetAllTables() {
        int page = 0;
        int size = 10;
        Page<TableDto> tablePage = new PageImpl<>(Collections.emptyList());
        when(tableService.getAllTables(page, size)).thenReturn(tablePage);

        String viewName = tableController.getAllTables(page, size, model);

        assertEquals("/table/table.html", viewName);
        verify(model).addAttribute("tables", tablePage.getContent());
        verify(model).addAttribute("page", tablePage);
    }

    @Test
    void testNewTable() {
        String viewName = tableController.newTable(model);

        assertEquals(TABLE_CREATE, viewName);
        verify(model).addAttribute(eq("table"), any(TableDto.class));
        verify(model).addAttribute("statuses",
                Arrays.asList(TableStatus.AVAILABLE,
                        TableStatus.BOOKED,
                        TableStatus.BUSY));
    }

    @Test
    void testEditTable() {
        Long tableId = 1L;
        TableDto tableDto = new TableDto();
        tableDto.setId(tableId);

        when(tableService.edit(tableId)).thenReturn(tableDto);

        String viewName = tableController.editTable(model, tableId);

        assertEquals("table/edit.html", viewName);
        verify(model).addAttribute("table", tableDto);
        verify(model).addAttribute("statuses", Arrays.asList(TableStatus.AVAILABLE, TableStatus.BUSY, TableStatus.BOOKED));
    }

    @Test
    void testCreateTableWithBindingErrors() {
        when(bindingResult.hasErrors()).thenReturn(true);

        String viewName = tableController.createTable(new TableDto(), bindingResult, model, redirectAttributes);

        assertEquals(TABLE_CREATE, viewName);
        verify(model).addAttribute("statuses", Arrays.asList(TableStatus.AVAILABLE, TableStatus.BUSY, TableStatus.BOOKED));
    }

    @Test
    void testCreateTableWithoutBindingErrors() {
        when(bindingResult.hasErrors()).thenReturn(false);
        TableDto savedTable = new TableDto();
        when(tableService.createTable(any(TableDto.class))).thenReturn(savedTable);

        String viewName = tableController.createTable(new TableDto(), bindingResult, model, redirectAttributes);

        assertEquals("redirect:/api/v1/table/tables", viewName);
        verify(redirectAttributes).addFlashAttribute(
                "successMessage", "Table with ID " + savedTable.getId() + " was created successfully."
        );
    }

    @Test
    void testUpdateTableWithBindingErrors() {
        when(bindingResult.hasErrors()).thenReturn(true);

        String viewName = tableController.updateTable(1L, new TableDto(), bindingResult, model, redirectAttributes);

        assertEquals(TABLE_CREATE, viewName);
        verify(model).addAttribute("statuses",
                Arrays.asList(TableStatus.AVAILABLE,
                        TableStatus.BUSY,
                        TableStatus.BOOKED));
    }

    @Test
    void testDeleteTableCallsTableService() {

        Long tableId = 5L;
        String viewName = tableController.deleteTable(tableId, redirectAttributes);

        verify(tableService).deleteTable(tableId);
    }
}
