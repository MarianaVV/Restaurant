package ro.somethinggoodtoeat.restaurantmanagementsystem.controller;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.junit.jupiter.api.BeforeEach;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import ro.somethinggoodtoeat.restaurantmanagementsystem.dto.securityDto.UserDetails;
import ro.somethinggoodtoeat.restaurantmanagementsystem.enums.Role;
import ro.somethinggoodtoeat.restaurantmanagementsystem.service.UserServiceImpl;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

import java.util.*;

import static org.mockito.Mockito.when;

@SpringBootTest
public class UserControllerTest {

    @Mock
    private UserServiceImpl userService;

    @Mock
    private Model model;

    @Mock
    private BindingResult bindingResult;

    @Mock
    private RedirectAttributes redirectAttributes;

    @InjectMocks
    private UserController userController;

    @BeforeEach
    void setUp() {

        MockitoAnnotations.initMocks(this);
    }

    @Test
    void testNewUser() {
        String viewName = userController.newUser(model);

        assertEquals("user/create.html", viewName);
        verify(model).addAttribute(eq("user"), any(UserDetails.class));
        verify(model).addAttribute("roles", Arrays.asList(Role.WAITER, Role.MANAGER, Role.CHEF));
    }

    @Test
    void testCreateUserWithBindingErrors() {
        when(bindingResult.hasErrors()).thenReturn(true);

        String viewName = userController.createUser(new UserDetails(), bindingResult, model, "selectedRole", redirectAttributes);

        assertEquals("user/create.html", viewName);
        verify(model).addAttribute("roles", Arrays.asList(Role.WAITER, Role.MANAGER, Role.CHEF));
    }

    @Test
    void testUpdateUserWithBindingErrors() {
        when(bindingResult.hasErrors()).thenReturn(true);

        String viewName = userController.updateUser(1L, new UserDetails(), bindingResult, model, redirectAttributes);

        assertEquals("user/create.html", viewName);
        verify(model).addAttribute("roles", Arrays.asList(Role.WAITER, Role.MANAGER, Role.CHEF));
    }
}
