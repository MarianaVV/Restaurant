package ro.somethinggoodtoeat.restaurantmanagementsystem.controller;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;
import ro.somethinggoodtoeat.restaurantmanagementsystem.dto.MenuDto;
import ro.somethinggoodtoeat.restaurantmanagementsystem.service.MenuServiceImpl;
import ro.somethinggoodtoeat.restaurantmanagementsystem.service.OrderServiceImpl;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.Mockito.*;

public class ReportControllerTest {


    @Mock
    private MenuServiceImpl menuService;

    @Mock
    private OrderServiceImpl orderService;

    @InjectMocks
    private ReportController reportController;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        reportController = new ReportController(menuService, orderService);
    }

    @Test
    public void testGetAllPopularMenus() {
        List<MenuDto> mockMenus = new ArrayList<>();
        when(menuService.getAllMenusByIsPopular()).thenReturn(mockMenus);

        Model model = new ExtendedModelMap();
        String viewName = reportController.getAllPopularMenus(model);

        verify(menuService, times(1)).getAllMenusByIsPopular();
        assertThat(viewName).isEqualTo("report/report.html");

        List<MenuDto> retrievedMenus = (List<MenuDto>) model.asMap().get("menus");
        assertThat(retrievedMenus).isEqualTo(mockMenus);
    }
}
