package ro.somethinggoodtoeat.restaurantmanagementsystem.controller;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.ui.ConcurrentModel;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;
import org.springframework.validation.MapBindingResult;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributesModelMap;
import ro.somethinggoodtoeat.restaurantmanagementsystem.dto.MenuDto;
import ro.somethinggoodtoeat.restaurantmanagementsystem.dto.OrderDetailsDto;
import ro.somethinggoodtoeat.restaurantmanagementsystem.dto.OrderDto;
import ro.somethinggoodtoeat.restaurantmanagementsystem.dto.TableDto;
import ro.somethinggoodtoeat.restaurantmanagementsystem.enums.OrderDetailsStatus;
import ro.somethinggoodtoeat.restaurantmanagementsystem.enums.OrderStatus;
import ro.somethinggoodtoeat.restaurantmanagementsystem.enums.PaymentType;
import ro.somethinggoodtoeat.restaurantmanagementsystem.enums.UnitOfMeasure;
import ro.somethinggoodtoeat.restaurantmanagementsystem.service.MenuServiceImpl;
import ro.somethinggoodtoeat.restaurantmanagementsystem.service.OrderDetailsServiceImpl;
import ro.somethinggoodtoeat.restaurantmanagementsystem.service.OrderServiceImpl;
import ro.somethinggoodtoeat.restaurantmanagementsystem.service.TableServiceImpl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.*;

public class OrderControllerTest {
    public static final String REDIRECT_API_ORDER_ORDERS = "redirect:/api/v1/order/orders";
    public static final String REDIRECT_API_ORDER_ORDER_ORDER_ID = "redirect:/api/v1/order/order?orderId=";
    public static final String ORDER_ADD_ORDER_ITEM = "order/addOrderItem.html";
    @Mock
    private OrderServiceImpl orderService;
    @Mock
    private MenuServiceImpl menuService;
    @Mock
    private TableServiceImpl tableService;
    @Mock
    private OrderDetailsServiceImpl orderDetailsService;
    @Mock
    private Model model;
    @Mock
    private BindingResult bindingResult;
    @Mock
    private RedirectAttributes redirectAttributes;
    private OrderController orderController;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
        orderController = new OrderController(orderService,
                menuService,
                tableService,
                orderDetailsService);
    }

    @Test
    @DisplayName("Get All Orders")
    void testGetAllOrders() {
        int page = 0;
        int size = 10;
        Page<OrderDto> orderPage = new PageImpl<>(Collections.emptyList());
        when(orderService.getAllOrders(page, size)).thenReturn(orderPage);

        String viewName = orderController.getAllOrders(page, size, model);

        assertEquals("/order/order.html", viewName);
        verify(model).addAttribute("orders", orderPage.getContent());
        verify(model).addAttribute("page", orderPage);
    }

    @Test
    @DisplayName("Delete Order")
    void testDeleteOrder() {
        Long orderId = 1L;

        String viewName = orderController.deleteOrder(orderId, redirectAttributes);

        assertEquals(REDIRECT_API_ORDER_ORDERS, viewName);
        verify(orderService).deleteOrder(orderId);
        verify(redirectAttributes).addFlashAttribute(
                "successMessage", "Order with ID " + orderId + " was deleted successfully."
        );
    }

    @Test
    @DisplayName("New Order Post")
    void testNewOrderPost() {
        OrderDto orderDto = new OrderDto();
        Long orderId = 1L;
        orderDto.setId(orderId);

        when(bindingResult.hasErrors()).thenReturn(false);
        when(orderDetailsService.createOrder(orderDto)).thenReturn(orderDto);
        when(orderDetailsService.getOrderDetailsByOrderId(orderId)).thenReturn(Collections.emptyList());

        String viewName = orderController.newOrder(orderDto, orderId, bindingResult, model, redirectAttributes);

        assertEquals(REDIRECT_API_ORDER_ORDERS, viewName);
        verify(model).addAttribute("order", orderDto);
        verify(tableService).getAllTables();
        verify(redirectAttributes).addFlashAttribute(
                "successMessage", "Order with ID " + orderDto.getId() + " was created successfully."
        );
    }

    @Test
    @DisplayName("Edit Order")
    void testEditOrder() {
        Long orderId = 1L;
        OrderDto orderDto = new OrderDto();
        orderDto.setId(orderId);

        when(orderService.edit(orderId)).thenReturn(orderDto);
        when(orderDetailsService.getAllOrderDetails()).thenReturn(Collections.emptyList());

        String viewName = orderController.editOrder(model, orderId);

        assertEquals("order/editOrder.html", viewName);
        verify(model).addAttribute("order", orderDto);
        verify(tableService).getAllTables();
        verify(model).addAttribute("paymentType",
                Arrays.asList(PaymentType.CASH, PaymentType.CREDIT_CARD));
        verify(model).addAttribute("orderStatus",
                Arrays.asList(OrderStatus.PENDING, OrderStatus.PAID, OrderStatus.READY, OrderStatus.DELIVERED));
        verify(orderDetailsService).getAllOrderDetails();
    }

    @Test
    @DisplayName("Update Order Details Successful")
    void testUpdateOrderDetailsSuccessfulUpdate() {
        Long orderDetailId = 1L;
        Long orderId = 2L;

        OrderDetailsDto orderDetailsDto = new OrderDetailsDto();
        orderDetailsDto.setId(orderDetailId);
        orderDetailsDto.setOrderId(orderId);

        OrderDetailsDto updatedOrderDetails = new OrderDetailsDto();
        updatedOrderDetails.setId(orderDetailId);
        updatedOrderDetails.setOrderId(orderId);
        BindingResult bindingResult = new MapBindingResult(Collections.emptyMap(), "");
        RedirectAttributes redirectAttributes = new RedirectAttributesModelMap();
        when(orderDetailsService.updateOrdersDetails(eq(orderDetailId), any(OrderDetailsDto.class))).thenReturn(updatedOrderDetails);
        String viewName = orderController.updateOrderDetails(orderDetailId, orderDetailsDto, model, bindingResult, redirectAttributes);

        assertEquals(REDIRECT_API_ORDER_ORDER_ORDER_ID + updatedOrderDetails.getOrderId(), viewName);

        verify(orderDetailsService).updateOrdersDetails(eq(orderDetailId), eq(orderDetailsDto));
        verify(model).addAttribute(eq("orderDetails"), eq(updatedOrderDetails));
    }

    @Test
    @DisplayName("Edit Order Details")
    void testEditOrderDetails() {

        Long orderDetailId = 1L;
        List<MenuDto> menuList = Arrays.asList(new MenuDto(), new MenuDto());
        List<UnitOfMeasure> unitOfMeasuresList = Arrays.asList(
                UnitOfMeasure.PLATES,
                UnitOfMeasure.CUPS,
                UnitOfMeasure.PIECES);
        List<OrderDetailsStatus> orderDetailsStatuses = Arrays.asList(
                OrderDetailsStatus.COOKING,
                OrderDetailsStatus.READY);
        OrderDetailsDto retrievedOrderDetails = new OrderDetailsDto();
        retrievedOrderDetails.setId(orderDetailId);

        when(menuService.getAllMenus()).thenReturn(menuList);
        when(orderDetailsService.edit(orderDetailId)).thenReturn(retrievedOrderDetails);
        Model model = new ExtendedModelMap();

        String viewName = orderController.editOrderDetails(model, orderDetailId);

        assertEquals("order/editOrderDetails.html", viewName);
        assertTrue(model.containsAttribute("unitOfMeasures"));
        assertTrue(model.containsAttribute("orderDetails"));
        assertTrue(model.containsAttribute("menuList"));
        assertTrue(model.containsAttribute("orderDetailsStatuses"));

        List<UnitOfMeasure> retrievedUnitOfMeasures = (List<UnitOfMeasure>) model.getAttribute("unitOfMeasures");
        assertEquals(unitOfMeasuresList, retrievedUnitOfMeasures);
        OrderDetailsDto retrievedOrderDetailsDto = (OrderDetailsDto) model.getAttribute("orderDetails");
        assertEquals(retrievedOrderDetails, retrievedOrderDetailsDto);
        List<MenuDto> retrievedMenuList = (List<MenuDto>) model.getAttribute("menuList");
        assertEquals(menuList, retrievedMenuList);
        List<OrderDetailsStatus> retrievedOrderDetailsStatuses = (List<OrderDetailsStatus>) model.getAttribute("orderDetailsStatuses");
        assertEquals(orderDetailsStatuses, retrievedOrderDetailsStatuses);
    }

    @Test
    @DisplayName("Show Order Form")
    void testShowOrderForm() {
        Long orderId = 1L;

        List<PaymentType> paymentTypes = Arrays.asList(PaymentType.CASH, PaymentType.CREDIT_CARD);
        List<OrderStatus> orderStatuses = Arrays.asList(OrderStatus.PENDING, OrderStatus.PAID, OrderStatus.READY, OrderStatus.DELIVERED);

        OrderDto order = new OrderDto();
        order.setId(orderId);
        List<OrderDetailsDto> orderDetails = Arrays.asList(new OrderDetailsDto(), new OrderDetailsDto());
        Double totalAmount = 100.0;

        when(orderDetailsService.getOrderById(orderId)).thenReturn(order);
        when(orderService.getOrderDetailsDtos(orderId, order)).thenReturn(orderDetails);
        when(orderService.calculateTotalAmount(order)).thenReturn(totalAmount);
        List<TableDto> tableList = Arrays.asList(new TableDto(), new TableDto());
        when(tableService.getAllTables()).thenReturn(tableList);

        Model model = new ExtendedModelMap();
        String viewName = orderController.showOrderForm(orderId, model);

        assertEquals(ORDER_ADD_ORDER_ITEM, viewName);
        assertTrue(model.containsAttribute("order"));
        assertTrue(model.containsAttribute("tables"));
        assertTrue(model.containsAttribute("orderDetails"));
        assertTrue(model.containsAttribute("paymentType"));
        assertTrue(model.containsAttribute("orderStatus"));

        OrderDto retrievedOrder = (OrderDto) model.getAttribute("order");
        assertEquals(order, retrievedOrder);
        List<TableDto> retrievedTableList = (List<TableDto>) model.getAttribute("tables");
        assertEquals(tableList, retrievedTableList);
        List<OrderDetailsDto> retrievedOrderDetailsList = (List<OrderDetailsDto>) model.getAttribute("orderDetails");
        assertEquals(orderDetails, retrievedOrderDetailsList);
        List<PaymentType> retrievedPaymentTypes = (List<PaymentType>) model.getAttribute("paymentType");
        assertEquals(paymentTypes, retrievedPaymentTypes);
        List<OrderStatus> retrievedOrderStatuses = (List<OrderStatus>) model.getAttribute("orderStatus");
        assertEquals(orderStatuses, retrievedOrderStatuses);
    }

    @Test
    @DisplayName("New Order Details")
    void testNewOrderDetails() {

        OrderDetailsDto orderDetailsDto = new OrderDetailsDto();
        orderDetailsDto.setId(1L);
        orderDetailsDto.setOrderId(2L);

        BindingResult bindingResult = new BeanPropertyBindingResult(orderDetailsDto, "orderDetailsDto");
        when(orderDetailsService.createOrderDetails(orderDetailsDto)).thenReturn(orderDetailsDto);

        RedirectAttributes redirectAttributes = new RedirectAttributesModelMap();
        String viewName = orderController.newOrderDetails(orderDetailsDto, bindingResult, redirectAttributes);

        assertEquals(REDIRECT_API_ORDER_ORDER_ORDER_ID + orderDetailsDto.getOrderId(), viewName);
        assertFalse(bindingResult.hasErrors());
    }

    @Test
    @DisplayName("New Order Item")
    void testNewOrderItem() {
        Long orderId = 1L;
        OrderDto orderDto = new OrderDto();
        List<MenuDto> menuList = new ArrayList<>();
        menuList.add(new MenuDto());

        when(orderService.getOrderById(orderId)).thenReturn(orderDto);
        when(menuService.getAllMenus()).thenReturn(menuList);

        Model model = new ConcurrentModel();
        String viewName = orderController.newOrderItem(orderId, model);

        assertEquals("order/orderDetails.html", viewName);
        assertTrue(model.containsAttribute("orderDetails"));
        assertTrue(model.containsAttribute("menuList"));
        assertTrue(model.containsAttribute("unitOfMeasures"));
        assertTrue(model.containsAttribute("orderDetailsStatuses"));
        assertTrue(model.containsAttribute("order"));

        assertEquals(orderDto, model.getAttribute("order"));
        assertNotNull(model.getAttribute("orderDetails"));
        assertNotNull(model.getAttribute("menuList"));
        assertNotNull(model.getAttribute("unitOfMeasures"));
        assertNotNull(model.getAttribute("orderDetailsStatuses"));
    }

    @Test
    @DisplayName("Update Order")
    void testUpdateOrder() {
        Long orderId = 1L;
        OrderDto orderDto = new OrderDto();
        orderDto.setId(orderId);
        orderDto.setOrderStatus(OrderStatus.PAID);

        when(orderService.updateOrder(eq(orderId), any(OrderDto.class))).thenReturn(orderDto);
        Model model = new ConcurrentModel();
        RedirectAttributes redirectAttributes = new RedirectAttributesModelMap();
        String viewName = orderController.updateOrder(orderId, orderDto, model, new BeanPropertyBindingResult(orderDto, "orderDto"), redirectAttributes);

        assertEquals("order/payOrder.html", viewName);
        assertTrue(model.containsAttribute("order"));
        assertEquals(orderDto, model.getAttribute("order"));
    }

    @Test
    @DisplayName("Update Order Status Not Paid")
    void testUpdateOrderStatusNotPaid() {
        Long orderId = 1L;
        OrderDto orderDto = new OrderDto();
        orderDto.setId(orderId);
        orderDto.setOrderStatus(OrderStatus.READY);
        when(orderService.updateOrder(eq(orderId), any(OrderDto.class))).thenReturn(orderDto);
        Model model = new ConcurrentModel();
        RedirectAttributes redirectAttributes = new RedirectAttributesModelMap();
        String viewName = orderController.updateOrder(orderId, orderDto, model, new BeanPropertyBindingResult(orderDto, "orderDto"), redirectAttributes);

        assertEquals(REDIRECT_API_ORDER_ORDERS, viewName);
        assertTrue(model.containsAttribute("orderDto"));
        assertEquals(orderDto, model.getAttribute("orderDto"));
    }

    @Test
    @DisplayName("New Order With Order Id And Valid Input")
    void testNewOrderWithOrderIdAndValidInput() {
        Long orderId = 1L;
        OrderDto orderDto = new OrderDto();
        orderDto.setId(orderId);

        when(orderDetailsService.createOrder(eq(orderDto))).thenReturn(orderDto);
        when(orderDetailsService.getOrderDetailsByOrderId(eq(orderId))).thenReturn(Collections.singletonList(new OrderDetailsDto()));

        Model model = new ConcurrentModel();
        RedirectAttributes redirectAttributes = new RedirectAttributesModelMap();

        String viewName = orderController.newOrder(orderDto, orderId, new BeanPropertyBindingResult(orderDto, "orderDto"), model, redirectAttributes);

        assertEquals(REDIRECT_API_ORDER_ORDERS, viewName);
        assertTrue(model.containsAttribute("order"));
        assertTrue(model.containsAttribute("tables"));
        assertTrue(model.containsAttribute("paymentType"));
        assertTrue(model.containsAttribute("orderStatus"));
        assertTrue(model.containsAttribute("orderDetails"));
    }

    @Test
    @DisplayName("New Order Without Order Id And Valid Input")
    void testNewOrderWithoutOrderIdAndValidInput() {
        OrderDto orderDto = new OrderDto();
        when(orderDetailsService.createOrder(eq(orderDto))).thenReturn(orderDto);
        when(orderDetailsService.getOrderDetailsByOrderId(anyLong())).thenReturn(Collections.singletonList(new OrderDetailsDto()));
        Model model = new ConcurrentModel();
        RedirectAttributes redirectAttributes = new RedirectAttributesModelMap();

        String viewName = orderController.newOrder(orderDto, null, new BeanPropertyBindingResult(orderDto, "orderDto"), model, redirectAttributes);

        assertEquals(ORDER_ADD_ORDER_ITEM, viewName);
        assertTrue(model.containsAttribute("order"));
        assertTrue(model.containsAttribute("tables"));
        assertTrue(model.containsAttribute("paymentType"));
        assertTrue(model.containsAttribute("orderStatus"));
        assertTrue(model.containsAttribute("orderDetails"));
    }

    @Test
    @DisplayName("New Order With Validation Errors")
    void testNewOrderWithValidationErrors() {
        OrderDto orderDto = new OrderDto();
        BindingResult bindingResult = mock(BindingResult.class);
        when(bindingResult.hasErrors()).thenReturn(true);

        Model model = new ConcurrentModel();
        String viewName = orderController.newOrder(orderDto, null, bindingResult, model, null);

        assertEquals(ORDER_ADD_ORDER_ITEM, viewName);
        assertFalse(model.containsAttribute("order"));
        assertFalse(model.containsAttribute("tables"));
        assertFalse(model.containsAttribute("paymentType"));
        assertFalse(model.containsAttribute("orderStatus"));
        assertFalse(model.containsAttribute("orderDetails"));
    }

    @Test
    @DisplayName("Get All Pending Orders")
    void testGetAllPendingOrders() {
        Long orderId = 1L;
        OrderDto orderDto = new OrderDto();
        orderDto.setId(orderId);
        when(orderService.getOrderById(eq(orderId))).thenReturn(orderDto);
        List<OrderDetailsDto> orderDetails = new ArrayList<>();
        orderDetails.add(new OrderDetailsDto());
        when(orderDetailsService.getAllOrderDetailsById(eq(orderId))).thenReturn(orderDetails);

        Model model = new ConcurrentModel();
        String viewName = orderController.getAllPendingOrders(orderId, model);

        assertEquals("order/viewOrderDetails.html", viewName);
        assertTrue(model.containsAttribute("order"));
        assertTrue(model.containsAttribute("orderDetails"));
        OrderDto savedOrder = (OrderDto) model.getAttribute("order");
        assertNotNull(savedOrder);
        assertEquals(orderId, savedOrder.getId());
        List<OrderDetailsDto> savedOrderDetails = (List<OrderDetailsDto>) model.getAttribute("orderDetails");
        assertNotNull(savedOrderDetails);
        assertEquals(orderDetails, savedOrderDetails);
    }
}
