package ro.somethinggoodtoeat.restaurantmanagementsystem.controller;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import ro.somethinggoodtoeat.restaurantmanagementsystem.dto.MenuDto;
import ro.somethinggoodtoeat.restaurantmanagementsystem.service.CategoryServiceImpl;
import ro.somethinggoodtoeat.restaurantmanagementsystem.service.MenuServiceImpl;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

public class MenuControllerTest {

    public static final String REDIRECT_API_TO_MENU_MENUS = "redirect:/api/v1/menu/menus";
    public static final String MENU_CREATE = "menu/create.html";
    @Mock
    private MenuServiceImpl menuService;
    @Mock
    private CategoryServiceImpl categoryService;
    @Mock
    private Model model;
    @Mock
    private BindingResult bindingResult;
    @Mock
    private RedirectAttributes redirectAttributes;

    @InjectMocks
    private MenuController menuController;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void testGetAllMenus() {
        int page = 0;
        int size = 10;
        Page<MenuDto> menuPage = new PageImpl<>(Collections.emptyList());
        when(menuService.getAllMenus(page, size)).thenReturn(menuPage);

        String viewName = menuController.getAllMenus(page, size, model);

        assertEquals("/menu/menu.html", viewName);
        verify(model).addAttribute("menus", menuPage.getContent());
        verify(model).addAttribute("page", menuPage);
    }

    @Test
    void testDeleteMenu() {
        Long menuId = 1L;

        String viewName = menuController.deleteMenu(menuId, redirectAttributes);

        assertEquals(REDIRECT_API_TO_MENU_MENUS, viewName);
        verify(menuService).deleteMenu(menuId);
        verify(redirectAttributes).addFlashAttribute(
                "successMessage",
                "Menu with ID " + menuId +
                        " was deleted successfully.");
    }

    @Test
    void testNewMenu() {
        String viewName = menuController.newMenu(model);

        assertEquals(MENU_CREATE, viewName);
        verify(model).addAttribute(eq("menu"), any(MenuDto.class));
        verify(categoryService).getAllCategories();
    }

    @Test
    void testEditMenu() {
        Long menuId = 1L;
        MenuDto retrievedMenu = new MenuDto();
        retrievedMenu.setId(menuId);
        when(menuService.edit(menuId)).thenReturn(retrievedMenu);

        String viewName = menuController.editMenu(model, menuId);

        assertEquals("menu/edit.html", viewName);
        verify(model).addAttribute("menu", retrievedMenu);
        verify(categoryService).getAllCategories();
    }

    @Test
    void testCreateMenuWithBindingErrors() {
        when(bindingResult.hasErrors()).thenReturn(true);

        String viewName = menuController.createMenu(
                new MenuDto(), bindingResult, redirectAttributes, model);

        assertEquals(MENU_CREATE, viewName);
        verify(categoryService).getAllCategories();
    }

    @Test
    void testCreateMenuWithoutBindingErrors() {
        when(bindingResult.hasErrors()).thenReturn(false);
        MenuDto savedMenu = new MenuDto();
        when(menuService.createMenu(any(MenuDto.class))).thenReturn(savedMenu);

        String viewName = menuController.createMenu(
                new MenuDto(), bindingResult, redirectAttributes, model);

        assertEquals(REDIRECT_API_TO_MENU_MENUS, viewName);
        verify(redirectAttributes).addFlashAttribute(
                "successMessage", "Menu with ID "
                        + savedMenu.getId() + " was created successfully.");
    }

    @Test
    void testCreateMenuPopularMenuLimitReached() {
        when(bindingResult.hasErrors()).thenReturn(false);
        List<MenuDto> popularMenus = Arrays.asList(
                new MenuDto("Popular Menu 1", "Y"),
                new MenuDto("Popular Menu 2", "Y"),
                new MenuDto("Popular Menu 3", "Y")
        );
        when(menuService.getAllMenusByIsPopular()).thenReturn(popularMenus);

        MenuDto menu = new MenuDto("New Menu", "Y");
        String viewName = menuController.createMenu(menu, bindingResult, redirectAttributes, model);

        assertEquals("menu/create.html", viewName);
        verify(model).addAttribute("errorMessage",
                "You reached the limit of adding popular menus for this month." +
                        "Unselect the checkbox in order to proceed.");
        verify(model).addAttribute("categories", categoryService.getAllCategories());
    }

    @Test
    void testUpdateMenuWithoutBindingErrors() {
        when(bindingResult.hasErrors()).thenReturn(false);
        MenuDto updatedMenu = new MenuDto();
        updatedMenu.setId(1L);
        when(menuService.updateMenu(anyLong(), any(MenuDto.class))).thenReturn(updatedMenu);

        String viewName = menuController.updateMenu(
                1L, new MenuDto(), bindingResult, model, redirectAttributes);

        assertEquals(REDIRECT_API_TO_MENU_MENUS, viewName);
        verify(redirectAttributes).addFlashAttribute(
                "successMessage",
                "Menu with ID " + updatedMenu.getId() +
                        " was updated successfully.");
    }
}
