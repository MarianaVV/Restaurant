package ro.somethinggoodtoeat.restaurantmanagementsystem.controller;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import ro.somethinggoodtoeat.restaurantmanagementsystem.dto.CategoryDto;
import ro.somethinggoodtoeat.restaurantmanagementsystem.service.CategoryServiceImpl;
import java.util.Collections;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

public class CategoryControllerTest {

    public static final String REDIRECT_API_TO_CATEGORIES = "redirect:/api/v1/category/categories";
    public static final String CATEGORY_CREATE_HTML = "category/create.html";
    @Mock
    private CategoryServiceImpl categoryService;
    @Mock
    private Model model;
    @Mock
    private BindingResult bindingResult;
    @Mock
    private RedirectAttributes redirectAttributes;

    private CategoryController categoryController;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
        categoryController = new CategoryController(categoryService);
    }

    @Test
    void testGetAllCategories() {
        int page = 0;
        int size = 10;
        Page<CategoryDto> categoryPage = new PageImpl<>(Collections.emptyList());
        when(categoryService.getAllCategories(page, size)).thenReturn(categoryPage);

        String viewName = categoryController.getAllCategories(page, size, model);

        assertEquals("/category/category.html", viewName);
        verify(model).addAttribute("categories", categoryPage.getContent());
        verify(model).addAttribute("page", categoryPage);
    }

    @Test
    void testDeleteCategory() {
        Long categoryId = 1L;

        String viewName = categoryController.deleteCategory(categoryId, redirectAttributes);

        assertEquals(REDIRECT_API_TO_CATEGORIES, viewName);
        verify(categoryService).deleteCategory(categoryId);
        verify(redirectAttributes).addFlashAttribute(
                "successMessage",
                "Category with ID " + categoryId + " was deleted successfully.");
    }

    @Test
    void testNewCategory() {
        String viewName = categoryController.newCategory(model);

        assertEquals(CATEGORY_CREATE_HTML, viewName);
        verify(model).addAttribute(eq("category"), any(CategoryDto.class));
    }

    @Test
    void testEditCategory() {
        Long categoryId = 1L;
        CategoryDto retrievedCategory = new CategoryDto();
        when(categoryService.edit(categoryId)).thenReturn(retrievedCategory);

        String viewName = categoryController.editCategory(model, categoryId);

        assertEquals("category/edit.html", viewName);
        verify(model).addAttribute("category", retrievedCategory);
    }

    @Test
    void testCreateCategoryWithBindingErrors() {
        when(bindingResult.hasErrors()).thenReturn(true);

        String viewName = categoryController.createCategory(
                new CategoryDto(), bindingResult, redirectAttributes);

        assertEquals(CATEGORY_CREATE_HTML, viewName);
        verifyZeroInteractions(categoryService);
    }

    @Test
    void testCreateCategoryWithoutBindingErrors() {
        when(bindingResult.hasErrors()).thenReturn(false);
        CategoryDto savedCategory = new CategoryDto();
        when(categoryService.createCategory(any(CategoryDto.class))).thenReturn(savedCategory);

        String viewName = categoryController.createCategory(
                new CategoryDto(), bindingResult, redirectAttributes);

        assertEquals(REDIRECT_API_TO_CATEGORIES, viewName);
        verify(redirectAttributes).addFlashAttribute(
                "successMessage", "Category with ID "
                        + savedCategory.getId() + " was created successfully.");
    }

    @Test
    void testUpdateCategoryWithBindingErrors() {
        when(bindingResult.hasErrors()).thenReturn(true);

        String viewName = categoryController.updateCategory(
                1L, new CategoryDto(), bindingResult, model, redirectAttributes);

        assertEquals(CATEGORY_CREATE_HTML, viewName);
        verifyZeroInteractions(categoryService);
    }

    @Test
    void testUpdateCategoryWithoutBindingErrors() {
        when(bindingResult.hasErrors()).thenReturn(false);
        CategoryDto updatedCategory = new CategoryDto();
        updatedCategory.setId(1L);
        when(categoryService.updateCategory(anyLong(), any(CategoryDto.class))).thenReturn(updatedCategory);

        String viewName = categoryController.updateCategory(
                1L, new CategoryDto(), bindingResult, model, redirectAttributes);

        assertEquals(REDIRECT_API_TO_CATEGORIES, viewName);
        verify(redirectAttributes).addFlashAttribute(
                "successMessage",
                "Category with ID " + updatedCategory.getId() +
                        " was updated successfully.");
    }
}
